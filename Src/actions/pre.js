import {
    AUTHOR_SEARCH_REQUEST, AUTHOR_SEARCH_SUCCESS, AUTHOR_SEARCH_FALIURE,
} from './types';

import { getApi, postAipCall, getApiforWithoutHeader } from "../utils/ApiRequest";


export function AuthorSearchRequest() {   //....Request............
    return {
        type: AUTHOR_SEARCH_REQUEST
    };
}

export function AuthorSearchSuccess(response) {   //.....Success.......
    return {
        type: AUTHOR_SEARCH_SUCCESS,
        payload: response
    };
}

export function AuthorSearchFailure(error) {    //...for Failure.....
    return {
        type: AUTHOR_SEARCH_FALIURE,
        payload: error
    };
}

//.......for forgot Password dispatch method................//
export function AuthorSearchAPI(AuthorSearch, ApplangId) {

    return async (dispatch) => {
        dispatch(AuthorSearchRequest());
        //..............Header........
        const Header = {
            information: 'localhost,' + ApplangId,
            Accept: 'application/json'
        }
        getApi('authorRoute/findallAuthor/1/0/2/?search_key=' + AuthorSearch, Header)    // ....Api Method......
            .then((response) => {
                dispatch(AuthorSearchSuccess(response));  //..... successs..........
            })
            .catch((error) => {
                dispatch(AuthorSearchFailure(error));     //....Error.......
            })

    };
}


//............New Free book user add..............//

export function FreeBookUserRequest() {   //....Request............
    return {
        type: FREEBOOK_ADD_REQUEST
    };
}

export function FreeBookUserSuccess(response) {   //.....Success.......
    // console.log("free book success===>" + JSON.stringify(response))
    return {
        type: FREEBOOK_ADD_SUCCESS,
        payload: response
    };
}

export function FreeBookUserFailure(error) {    //...for Failure.....
    return {
        type: FREEBOOK_ADD_FALIURE,
        payload: error
    };
}


export function getNewFreeBookUserApiCalling(NewObject) {

    return async (dispatch) => {
        dispatch(FreeBookUserRequest());
        //..............Header........
        const Header = {
            Accept: 'application/json',
            contenttype: 'multipart/form-data'
        }
        postAipCall('freeBookRoute/add', NewObject, Header)   // ....Api Method......
            .then((response) => {
                console.log("api call success...")
                dispatch(FreeBookUserSuccess(response));  //..... successs..........
            })
            .catch((error) => {
                dispatch(FreeBookUserFailure(error));     //....Error.......
            })

    };
}

