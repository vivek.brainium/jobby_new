import React, { Component } from 'react';
import { View, Text, ImageBackground, StyleSheet, StatusBar, Dimensions, Image, TouchableOpacity, SafeAreaView } from 'react-native';
import Colors from "../../Utils/Colors";
import FontSize from "../../Utils/fonts";
import TextInputComponent from "../../Components/Login/TextInput"
import ButtonComponent from '../../Components/Button/loginButton'
import { RadioButton } from 'react-native-paper';
import FontFamily from '../../Utils/FontFamily';
import Apis from '../../Network/apicall';
import Toast from 'react-native-simple-toast';
import FloatingLabelInput from '../../Components/FlotingTextInput';
import AsyncStorage from '@react-native-community/async-storage';

const screenHeight =Dimensions.get('window').height
const screenwidth = Dimensions.get('window').width


class ForgetPassword extends Component {


  state = {
    passIcon: true,
    email: "",
    password: "",
    setview: 1,
    otp: "",
    password: '',
    newpassword: '',
    type:"Employer",
    storage: ""
  }

  async componentDidMount(){
    const storage = await AsyncStorage.getItem('usertype');
    this.setState({
      storage: storage
    })
  }

  // <======= Password show and hide functionlity =========>
  handleIconChange() {
    if (this.state.passIcon == true) {
      this.setState({ passIcon: false })
    } else {
      this.setState({ passIcon: true })
    }
  }

  // <======= Email Validation =========>
  validate = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      this.setState({ email: "incorrect" })
      return false;
    }
    else {
      this.setState({ email: text })
    }
  }

   // <======= for radio button cicle =========>
  renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }

  // <======= Employer Forgot Password =========>
  EmployerForgotpassword = () => {
    if (this.state.email == '') {
      Toast.show('Field Cannot be blanked');
    }
    else if (this.state.email == "incorrect") {
      Toast.show('Email is incorrect');
    }
    else {
      const apidata = {
        "email": this.state.email,
      }
      Apis.employer_forgotpassword(apidata)
        .then((res) => {
          if (res.response_code == 2000) {
            console.log(res.response_data, "response data");
            Toast.show("Success  " + res.response_message);
            this.setState({
              setview: 2
            })
          }
          else {
            Toast.show("Info  " + res.response_message);
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }

// <======= Employer Reset Password =========>
  EmployerResetpassword = () => {
    if (this.state.otp == '' || this.state.password == '' || this.state.newpassword == '') {
      Toast.show('Field Cannot be blanked');
    }
    else if (this.state.password != this.state.newpassword) {
      Toast.show('Password Should be same');
    }
    else if(this.state.password.length < 5) {
      Toast.show('Password Must be 6 Character');
    }
    else {
      const apidata = {
        "email": this.state.email,
        "otp": this.state.otp,
        "password": this.state.password
      }
      Apis.employer_resetpassword(apidata)
        .then((res) => {
          if (res.response_code == 2000) {
            console.log(res.response_data, "response data");
            Toast.show("Success  " + res.response_message);
            this.props.navigation.navigate('Login')
          }
          else {
            Toast.show("Info  " + res.response_message);
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }

  // <======= Employee Forgot Password =========>
  EmployeeForgotpassword = () => {
    if (this.state.email == '') {
      Toast.show('Field Cannot be blanked');
    }
    else if (this.state.email == "incorrect") {
      Toast.show('Email is incorrect');
    }
    else {
      const apidata = {
        "email": this.state.email,
      }
      Apis.employee_forgotpassword(apidata)
        .then((res) => {
          if (res.response_code == 2000) {
            console.log(res.response_data, "response data");
            Toast.show("Success " + res.response_message);
            this.setState({
              setview: 2
            })
          }
          else {
            Toast.show("Info  " + res.response_message);
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }

  // <======= Employee Reset Password =========>
  EmployeeResetpassword = () => {
    if (this.state.otp == '' || this.state.password == '' || this.state.newpassword == '') {
      Toast.show('Field Cannot be blanked');
    }
    else if (this.state.password != this.state.newpassword) {
      Toast.show('Password Should be same');
    }
    else if(this.state.password.length < 5) {
      Toast.show('Password Must be 6 Character');
    }
    else {
      const apidata = {
        "email": this.state.email,
        "otp": this.state.otp,
        "password": this.state.password
      }
      Apis.employee_resetpassword(apidata)
        .then((res) => {
          if (res.response_code == 2000) {
            console.log(res.response_data, "response data");
            Toast.show("Success  " + res.response_message);
            this.props.navigation.navigate('Login')
          }
          else {
            Toast.show("Info  " + res.response_message);
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }

  render() {
    return (
      <ImageBackground style={styles.container}
        source={require("../../Images/Login/login_bckground.png")}
      >
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />

        <View style={styles.logo_container}>
          <Image source={require("./../../Images/Login/logo.png")}
            style={styles.logo_image}
          />
        </View>
        <View style={{ alignItems: "center", marginBottom: 20 }}>
          <Text style={{ fontSize: FontSize.large_size, fontFamily: "Poppins-Medium", color: Colors.BlackColor }}>Forgot Password</Text>
        </View>

        {
          this.state.setview == 1 ?
            <View>
              <View style={[styles.repeatContainer]}>
                <FloatingLabelInput
                  textInputHeight={35}
                  textInputWidth={"100%"}
                  label="Email"
                  // value={this.state.email}
                  onChangeText={(val) => this.validate(val)}
                  placeholder={"Enter your email"}
                  autoCapitalize="none"
                />
              </View>
              

              <View style={{ alignItems: "center", marginTop:30 }}>
                {
                  this.state.storage == "employers" ?
                  <ButtonComponent
                  backgroundColor1="#1a2246"
                  height={50}
                  width="80%"
                  buttonName="Next"
                  onClick={()=> this.EmployerForgotpassword()}
                  // onClick={() => this.setState({
                  //   setview: 2
                  // })}
                />
                :
                <ButtonComponent
                  backgroundColor1="#1a2246"
                  height={50}
                  width="80%"
                  buttonName="Next"
                  onClick={()=> this.EmployeeForgotpassword()}
                  // onClick={() => this.setState({
                  //   setview: 2
                // })}
              />
                }
                
              </View>
            </View>
            : null
        }
        {
          this.state.setview == 2 ?
            <View>
              <View style={[styles.repeatContainer]}>
                <FloatingLabelInput
                  textInputHeight={35}
                  textInputWidth={"100%"}
                  label="OTP"
                  value={this.state.otp}
                  onChangeText={(val) =>  this.setState({ otp: val })}
                  placeholder={"Enter your Otp"}
                  maxLength={4}
                  keyboardType={'numeric'}
                />
              </View>
              <View style={[styles.repeatContainer]}>
                <FloatingLabelInput
                  textInputHeight={35}
                  textInputWidth={"100%"}
                  label="Password"
                  value={this.state.password}
                  onChangeText={(val) =>  this.setState({ password: val })}
                  placeholder={"Enter your password"}
                  secureTextEntry={true}
                />
              </View>
              <View style={[styles.repeatContainer]}>
                <FloatingLabelInput
                  textInputHeight={35}
                  textInputWidth={"100%"}
                  label="Confirm New Password"
                  value={this.state.newpassword}
                  onChangeText={(val) =>  this.setState({ newpassword: val })}
                  placeholder={"Enter Confirm New Password"}
                  secureTextEntry={true}
                />
              </View>
              {/* <TextInputComponent
                titleName={"Otp"}
                iconType={"Octicons"}
                iconName={"mail"}
                iconSize={18}
                iconColor="grey"
                rightIconType={"MaterialIcons"}
                placeholder={"Enter your Otp"}
                onChangeText={(val) => this.setState({ otp: val })}
              /> */}

              <View style={{ marginTop: 30, alignItems: "center" }}>
                {
                  this.state.storage == "employers" ?
                    <ButtonComponent
                      backgroundColor1="#1a2246"
                      height={50}
                      width="80%"
                      buttonName="Next"
                      onClick={() => this.EmployerResetpassword()}
                    />
                    :
                    <ButtonComponent
                      backgroundColor1="#1a2246"
                      height={50}
                      width="80%"
                      buttonName="Next"
                      onClick={() => this.EmployeeResetpassword()}
                    />
                }
              </View>
              <View style={{ paddingTop: 25 }}> 
              <Text style={{fontSize:14, alignSelf:"center"}}>Cannot get OTP <Text style={{color:"blue"}} onPress={()=> {
                this.state.storage == "employers" ?
                this.EmployerForgotpassword()
                :
                this.EmployeeForgotpassword()
              }}>Resend</Text></Text>

              </View>
            </View>
            : null
        }
      </ImageBackground>
    );
  }
}

export default ForgetPassword;


const styles = StyleSheet.create({
  container: {
    width: screenwidth,
    height: screenHeight
  },
  logo_container: {
    alignItems: "center",
    height: screenHeight / 3,
    justifyContent: "center"
  },
  logo_image: {
    height: 39,
    width: 174
  },
  newUser_Text: {
    color: "#696969",
    fontSize: FontSize.small_size
  },
  SignUp_text: {
    color: "#1a2245",
    fontSize: FontSize.small_size
  },
  radioCircle: {
    padding: 9, 
    borderWidth: 2, 
    borderColor: Colors.Nero, 
    borderRadius: 100, 
    position: 'absolute'
  },
  repeatContainer: {
    width: "80%", alignSelf: "center", marginBottom: 15
  },
});