import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, ScrollView, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ApplicationComponent from '../../Components/Application';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontFamily from '../../Utils/FontFamily';
import Colors from '../../Utils/Colors';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;


const DATA = [
  {
    id: 1,
    title: "TCG Pvt Ltd.",
    locationname: "California",
  },
  {
    id: 2,
    title: "TCG Pvt Ltd.",
    locationname: "California",
  },
  {
    id: 3,
    title: "TCG Pvt Ltd.",
    locationname: "California",
    
  },
  {
    id: 4,
    title: "TCG Pvt Ltd.",
    locationname: "California",
  },
  {
    id: 5,
    title: "TCG Pvt Ltd.",
    locationname: "California",
  },
];


class Employers extends Component {
  state = {
    postjob: ''
  }

  hideAll = () => {
    DATA.map(d => {
      if (this.state[d.id]) {
        this.setState({ [d.id]: false })
      }
    })
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="Employers"
          // type={"textinput"}
          // onChangeText={(postjob) => this.setState({ postjob })}
          // placeholder="Search"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />
        <FlatList
          scrollEnabled={false}
          data={DATA}
          renderItem={({ item }) => (
            <ApplicationComponent
              id={item.id}
              title={item.title}
              years={item.years}
              locationname={item.locationname}
              verticalthreedot
              show={this.state[item.id]}
              gotodetails={() => this.setState({ [item.id]: !this.state[item.id] })}
              maincontainerPress={() => { this.hideAll; this.props.navigation.navigate('EmployerProfile') }}
              details={() => { this.props.navigation.navigate('ShortList'); this.setState({ [item.id]: false }) }}
              block={() => { this.setState({ [item.id]: false }) }}
            />
          )}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

export default Employers;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  },
  maincontainer: {
    backgroundColor: "#999", marginBottom: 15, elevation: 3
  },
  secondcontainer: {
    backgroundColor: "#e9e9e9", padding: 20
  },
  repeatcontainer: {
    flexDirection: "row"
  },
  postimage: {
    alignItems: "flex-start", marginRight: 10
  },
  text: {
    marginBottom: 8, fontFamily: FontFamily.Poppins_Medium, color: "#494949"
  },
  title: {
    marginBottom: 10, fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium, fontWeight: "600"
  },
})