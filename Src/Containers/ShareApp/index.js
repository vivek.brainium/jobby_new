import React, { Component } from 'react';
import {SafeAreaView, TouchableOpacity,View, Text, StyleSheet, Dimensions, FlatList,TextInput,Image } from 'react-native';
import HeaderComponent from "../../Components/Header"
import ContactList from '../../Components/ContactList';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
import Colors from '../../Utils/Colors';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const DATA = [
  {
    id: 1,
    name: "Stuart Alexandra",
    email: "stuart@gmail.com",
    image: require('../../Images/EmployerProfile/image.jpg'),
  },
  {
    id: 2,
    name: "Stuart Alexandra",
    email: "stuart@gmail.com",
    image: require('../../Images/EmployerProfile/image.jpg'),
  },
  {
    id: 3,
    name: "Stuart Alexandra",
    email: "stuart@gmail.com",
    image: require('../../Images/EmployerProfile/image.jpg'),
  },
  {
    id: 4,
    name: "Stuart Alexandra",
    email: "stuart@gmail.com",
    image: require('../../Images/EmployerProfile/image.jpg'),
  },
  {
    id: 5,
    name: "Stuart Alexandra",
    email: "stuart@gmail.com",
    image: require('../../Images/EmployerProfile/image.jpg'),
  },
  {
    id: 6,
    name: "Stuart Alexandra",
    email: "stuart@gmail.com",
    image: require('../../Images/EmployerProfile/image.jpg'),
  },
];

class Share extends Component {

  state = {
    postjob: '',
    text: ""
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          backicon
          send
          headerTitle="Share"
          onClick2={() => this.props.navigation.goBack()}
        />
        <View style={{ flex: 1, }}>
          <View style={{ width: "85%", alignSelf: "center" }}>
            <Text style={styles.message}>Message</Text>
            <View style={{width:"100%", borderBottomWidth: .55,alignSelf:"center", marginBottom:40, borderBottomColor:"#696969", paddingBottom:10}}>
              <TextInput
                style={{ fontSize: FontSize.small_size, fontWeight: "600", width: "100%", }}
                //value={props.InputValue}
                multiline={true}
                //onChangeText={(text) => props.onChangeText(text)}
                autoCapitalize='none'
                autoCorrect={false}
                returnKeyType='done'
                enablesReturnKeyAutomatically={true}
                selectTextOnFocus={true}
                spellCheck={false}
              />
            </View>
            <Text style={styles.shareText}>share with</Text>
            <View style={styles.socialMediaView}>
              <TouchableOpacity style={{ alignItems: "center" }}>
                <Image source={require('../../Images/Share/facebook.png')} style={styles.socialImage} resizeMode="contain" />
                <Text style={styles.socialIconName}>Facebook</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ alignItems: "center" }}>
                <Image source={require('../../Images/Share/twitter.png')} style={styles.socialImage} resizeMode="contain" />
                <Text>Twitter</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ alignItems: "center" }}>
                <Image source={require('../../Images/Share/linkedin.png')} style={styles.socialImage} resizeMode="contain" />
                <Text>Linkedin</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ alignItems: "center" }}>
                <Image source={require('../../Images/Share/google+.png')} style={styles.socialImage} resizeMode="contain" />
                <Text>Google+</Text>
              </TouchableOpacity>
            </View>
            </View>
            <View style={{width:"100%",borderBottomWidth:.55,marginBottom:15 ,borderBottomColor:"#9b9b9b"}}></View>
            <Text style={styles.suggestionText}>suggestions from Google</Text>
          
          
          <FlatList
            data={DATA}
            renderItem={({ item }) => (
              <ContactList
                id={item.id}
                name={item.name}
                email={item.email}
                image={item.image}
              />
            )}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}
          />

        </View>
      </View>
    );
  }
}

export default Share;

const styles = StyleSheet.create({
  MainContainer: {
    //width: screenwidth,
    height: screenHeight,
    width:"100%"
  },
  message: {
    marginTop: 15,
    fontFamily: FontFamily.Poppins_Medium,
    fontSize: FontSize.medium_size,
  },
  socialImage: {
    width:30,
    height:30
  },
  socialMediaView: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 20,
    marginBottom: 25,
  },
  socialIconName: {
    fontFamily: FontFamily.Poppins_Medium,
    fontSize: FontSize.micro_size,
    fontWeight: "600"
  },
  shareText: {
    fontFamily: FontFamily.Poppins_Medium,
   fontSize: FontSize.micro_size,
   fontWeight:"600",
   color:"#333333"
  },
  suggestionText: {
    marginBottom:15,
    fontFamily: FontFamily.Poppins_Medium,
   fontSize: FontSize.small_size,
   fontWeight:"600",
   marginLeft:"7%",
   color:"#1c1c1c"
  }
})