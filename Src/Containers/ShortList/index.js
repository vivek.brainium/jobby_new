import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, ScrollView, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from '../../Components/Button/loginButton'
import ApplicationComponent from '../../Components/Application';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontFamily from '../../Utils/FontFamily';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import Colors from '../../Utils/Colors';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const DATA = [
  {
    id: 1,
    title: "Polard Neo",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 2,
    title: "Polard Neo",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 3,
    title: "Polard Neo",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019"
  },
  {
    id: 4,
    title: "Polard Neo",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 5,
    title: "Polard Neo",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
];

const DATA1 = [
  {
    id: 1,
    title: "Brainium Information Technology",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 2,
    title: "Brainium Information Technology",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 3,
    title: "Brainium Information Technology",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019"
  },
  {
    id: 4,
    title: "Brainium Information Technology",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 5,
    title: "Brainium Information Technology",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
];


class ShortList extends Component {
  state = {
    postjob: '',
    storage: ''
  }

  hideAll = () => {
    DATA.map(d => {
      if (this.state[d.id]) {
        this.setState({ [d.id]: false })
      }
    })
  }

  async componentDidMount() {
    const storage = await AsyncStorage.getItem('usertype');
    this.setState({
      storage: storage
    })
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="ShortList"
          // type={"textinput"}
          // onChangeText={(postjob) => this.setState({ postjob })}
          // placeholder="Search"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />
        <ScrollView style={{ marginBottom: 10 }}>
          {this.state.storage == "employers" ?
            <View style={styles.maincontainer}>
              <TouchableOpacity style={styles.secondcontainer} onPress={() => this.props.navigation.navigate('EmployerProfile')}>
                <View style={styles.view1}>
                  <View style={styles.InnerView}>
                    <Text style={styles.title}>HR Manager</Text>
                  </View>
                  <View style={{ marginLeft: 2 }}>
                    <View style={styles.repeatcontainer}>
                      <View style={styles.postimage}>
                        <SimpleLineIcons
                          style={{ marginBottom: 10 }}
                          name="briefcase"
                          color="gray"
                          size={18}
                        />
                      </View>
                      <Text style={styles.text}>4-6 Years</Text>
                    </View>
                    <View style={styles.repeatcontainer}>
                      <View style={styles.postimage}>
                        <EvilIcons
                          style={{ marginBottom: 10, }}
                          name="location"
                          color="gray"
                          size={20}
                        />
                      </View>
                      <Text style={styles.text}>California</Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            : null
          }
          {this.state.storage == "employers" ?

            <FlatList
              scrollEnabled={false}
              data={DATA}
              renderItem={({ item }) => (
                <ApplicationComponent
                  id={item.id}
                  title={item.title}
                  years={item.years}
                  locationname={item.locationname}
                  designatonsection
                  desingnation={item.desingnation}
                  date
                  posteddate={item.posteddate}
                  verticalthreedot
                  shortlistdotpress={this.state[item.id]}
                  gotodetails={() => this.props.navigation.navigate('ShortLised')}
                  maincontainerPress={() => { this.hideAll; this.props.navigation.navigate('ShortLised') }}
                />
              )}
              keyExtractor={item => item.id}
            />
            :
            <FlatList
              scrollEnabled={false}
              data={DATA1}
              renderItem={({ item }) => (
                <ApplicationComponent
                  id={item.id}
                  title={item.title}
                  years={item.years}
                  locationname={item.locationname}
                  verticalthreedot
                  shortlistdotpress={this.state[item.id]}
                  gotodetails={() => this.props.navigation.navigate('EmployerProfile')}
                  maincontainerPress={() => { this.hideAll; this.props.navigation.navigate('EmployerProfile') }}
                />
              )}
              keyExtractor={item => item.id}
            />
          }
        </ScrollView>
      </View>
    );
  }
}

export default ShortList;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  },
  maincontainer: {
    backgroundColor: "#999", marginBottom: 15, elevation: 3
  },
  secondcontainer: {
    backgroundColor: "#e9e9e9", padding: 20
  },
  repeatcontainer: {
    flexDirection: "row",
    // backgroundColor:"#999"
  },
  postimage: {
    width: "10%",
    alignItems: "flex-start",
    // backgroundColor:"#000"
  },
  text: {
    flex: 0.9, marginBottom: 8, fontFamily: FontFamily.Poppins_Medium, color: "#494949"
  },
  title: {
    marginBottom: 10, fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium, fontWeight: "600"
  },
})