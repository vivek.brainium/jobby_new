import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, ImageBackground, StatusBar, Image, TouchableOpacity, Animated, ScrollView, SafeAreaView } from 'react-native';
import FontSize from "../../Utils/fonts";
import FontFamily from '../../Utils/FontFamily';
import HeaderComponent from "../../Components/Header"
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Colors from "../../Utils/Colors";
import ButtonComponent from '../../Components/Button/loginButton'
const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const anim = new Animated.Value(0);


class Profile extends Component {

  state = {
    progressStatus: 10
  }

  componentDidMount() {
    this.onAnimate()
  }

  onAnimate = () => {
    anim.addListener(({ value }) => {
      this.setState({ progressStatus: parseInt(value, 10) })
    });
    Animated.timing(anim, {
      toValue: 100,
      duration: 5000,
    }).start();
  }


  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="Profile"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />

        <ScrollView style={{ flex: 1 }}>
          <View style={styles.maincontainer}>
            <View style={styles.MainImageSection}>
              <View style={styles.ImageContainer}>
                <Image source={require("../../Images/AppliedProfile/image.jpg")}
                  resizeMode={'contain'} style={{ height: "100%", width: "80%", }}
                />
              </View>

              <View style={styles.Details}>
                <Text style={styles.Name}>Jhon Marshal</Text>
                <Text style={styles.Profetion}>HR Executive</Text>
                <Text style={styles.Company}>Paladivine Inc.</Text>
              </View>
            </View>

            <View style={styles.InformationContainer}>
              <View style={styles.SingleContainer}>
                <Fontisto
                  name='email'
                  size={20}
                  color='#747474'
                  style={{ marginRight: 20 }} />
                <Text style={{
                  color: '#747474', textAlign: 'center'
                }}>johnmarshal@gmail.com</Text>
              </View>
              <View style={styles.SingleContainer}>
                <Feather
                  name='phone-call'
                  size={20}
                  color='#747474'
                  style={{ marginRight: 20 }} />
                <Text style={{
                  color: '#747474', textAlign: 'center'
                }}>9830731745</Text>
              </View>
            </View>

            <View style={styles.ProfileSommeryContainer}>
              <View style={styles.FirstContainer}>
                <View style={{ flex: 0.40, justifyContent: 'center' }}>
                  <Text style={styles.ProfileSummery}>Company Address</Text>
                </View>
              </View>
              <View style={styles.DescriptionContainer}>
                <Text style={styles.Description}>1600 Pensalvania Ave NW, Washinton, DC 20500</Text>
              </View>
            </View>
            <View style={{ justifyContent: 'flex-end', marginTop: 15, flex: 0.50 }}>
              <View style={{ marginTop: 0, alignItems: "center", justifyContent: "center" }}>
                <ButtonComponent
                  buttonName="Edit Profile"
                  backgroundColor1="#4b849e"
                  height={50}
                  width="90%"
                  onClick={() => this.props.navigation.navigate("EditProfile")}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </View >
    );
  }
}

export default Profile;

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  maincontainer: {
    height: screenHeight,
    width: screenwidth,
    backgroundColor: '#f5f5f5',
  },
  MainImageSection: {
    flex: 0.25,
    flexDirection: 'row',
    marginTop: 10
  },
  ImageContainer: {
    flex: 0.35, alignItems: 'center',
  },
  Details: {
    flex: 0.65,
    alignItems: 'flex-start',
    paddingTop: 5,
    justifyContent: "center"
  },
  Name: {
    fontSize: FontSize.large_size,
    color: '#000',
    textAlign: 'left',
    fontFamily: FontFamily.Poppins_Semibold,
    fontWeight: 'bold'
  },
  Profetion: {
    fontSize: FontSize.small_size,
    color: '#333333',
    textAlign: 'left',
    fontFamily: FontFamily.Poppins_Medium,
    fontWeight: '600'
  },
  Company: {
    color: '#464646',
    textAlign: 'left',
    fontFamily: FontFamily.Poppins_Light,
    fontWeight: '500'
  },
  InformationContainer: {
    marginLeft: 25,
    marginTop: 25
  },
  InformationContainer2: {
    flex: 0.20,
    marginLeft: 25,
    marginTop: 25
  },
  SingleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10
  },
  MbaContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15
  },
  ProfileSommeryContainer: {
    marginTop: 20,
    paddingVertical: 20,
    backgroundColor: '#fff',
    shadowOpacity: 0.1,
    shadowRadius: 5,
    elevation: 2
  },
  FirstContainer: {
    flexDirection: 'row',
    marginLeft: 25
  },
  ProfileSummery: {
    color: '#000',
    fontSize: FontSize.medium_size,
    fontWeight: '500'
  },
  ResumeButton: {
    borderRadius: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5, height: screenHeight / 18,
    width: screenwidth / 2.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  DownloadResume: {
    fontWeight: '800',
    color: '#4b849e',
    fontFamily: FontFamily.Poppins_Medium
  },
  DescriptionContainer: {
    marginLeft: 25,
    marginTop: 15,
    paddingRight: 2,
    width: "50%"
  },
  Description: {
    color: '#747474',
    textAlign: 'left',
    fontFamily: 'Poppins-Light'
  },
  ReadMore: {
    color: '#2f2f2f',
    fontSize: FontSize.small_size,
    textAlign: 'left',
    fontFamily: 'Poppins-Light'
  },
  LastContainer: {
    paddingVertical: 10,
    justifyContent: 'center',
  },
  Shortlist: {
    color: '#2f2f2f',
    fontSize: FontSize.small_size,
    textAlign: 'center'
  },
  inner: {
    width: "100%",
    height: 2,
    borderRadius: 15,
    backgroundColor: "green",
  },
})