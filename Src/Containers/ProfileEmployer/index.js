import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from '../../Components/Button/loginButton'
import FontFamily from '../../Utils/FontFamily';
import Colors from '../../Utils/Colors';
import ProfileEmpJob from '../../Components/ProfileEmployer'


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;



class ProfileEmployer extends Component {

  state = {
    //searchjob: '',
    Headline: "Current Work Details",
    CompanyName: "Berkshire Hathaway Inc.",
    Designation: "HR Executive",
    //Location: "California",
    Experience: "4 - 6 years",
    TotalExperience: "10 years",
    Skills: 'MBA',
    Description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nostrud exercitation ullamco laborisnisi ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nostrud exercitation ullamco laborisnisi ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nostrud exercitation ullamco laborisnisi ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nostrud exercitation ullamco laborisnisi ut aliquip ex ea commodo consequat."
  }

  render() {
    const { navigation } = this.props;
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => navigation.openDrawer()}
          headerTitle={"Profile Employer"}
          //  type={"textinput"}
          //onChangeText={(searchjob)=> this.setState({searchjob})}
          // placeholder="Search Job"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />

        <ProfileEmpJob
          headerName={this.state.Headline}
          CompanyName={this.state.CompanyName}
          Designation={this.state.Designation}
          //Location= {this.state.Location}
          Experience={this.state.Experience}
          TotalExperience={this.state.TotalExperience}
          Skills={this.state.Skills}
          Description={this.state.Description}
          firstButton="Save"
          firstButtonColor="#f67665"
          secondButton="Cancel"
          secondButtonColor="#b4b4b4"
        />


      </View>
    );
  }
}

export default ProfileEmployer;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  },

})