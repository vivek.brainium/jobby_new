import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, TouchableOpacity, SafeAreaView} from 'react-native';
import HeaderComponent from "../../Components/Header"
import Employer_JobListing from '../../Components/EmployerJobListing';
import Colors from '../../Utils/Colors';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const DATA = [
  {
    id: 1,
    title: "Hr Executive",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
  {
    id: 2,
    title: "Sr Software Developer",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
  {
    id: 3,
    title: "Dotnet Developer",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
  {
    id: 4,
    title: "Dotnet Developer",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
  {
    id: 5,
    title: "Sr Software Developer",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
];

class EmployerJobListing extends Component {

  state = {
    postjob: '',
    show: false
  }

  hideAll = () => {
    DATA.map(d => {
      if(this.state[d.id]) {
        this.setState({[d.id] : false})
      }
    })
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="Job Listing"
          type={"textinput"}
          onChangeText={(postjob) => this.setState({ postjob })}
          placeholder="Search Job"
          notificationonpress={()=> this.props.navigation.navigate("Notification")}
        />
        <View style={{ flex: 1, position:"relative" }}>
          <FlatList
            data={DATA}
            renderItem={({ item }) => (
                <Employer_JobListing
                  id={item.id}
                  title={item.title}
                  years={item.years}
                  locationname={item.locationname}
                  desingnation={item.desingnation}
                  posteddate={"Posted " + item.posteddate}
                  onclick={()=> this.setState({[item.id]: !this.state[item.id] })}
                  show={this.state[item.id]}
                  details={()=> {this.props.navigation.navigate('Application'); this.setState({[item.id]: false})}}
                  block={()=> {this.props.navigation.navigate('ReportBlock'); this.setState({[item.id]: false})}}
                  maincontainerPress={() => {this.hideAll; this.props.navigation.navigate('ViewJob')}}
                />
            )}
            keyExtractor={item => item.id}
          />

        </View>
      </View>
    );
  }
}

export default EmployerJobListing;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  }
})