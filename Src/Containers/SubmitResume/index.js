import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, TouchableOpacity, TextInput, ScrollView, KeyboardAvoidingView, Platform, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from '../../Components/Button/loginButton'
import FontFamily from '../../Utils/FontFamily';
import Colors from '../../Utils/Colors';
import TextInputComponent from "./../../Components/Login/TextInput"
// import { RadioButton } from 'react-native-paper';
import TagInput from 'react-native-tag-input';
import FloatingLabelInput from '../../Components/FlotingTextInput';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const horizontalInputProps = {
  keyboardType: 'default',
  returnKeyType: 'search',
  placeholder: 'Enter Tag',
  style: {
    fontSize: 14,
    marginVertical: Platform.OS == 'ios' ? 10 : -2,
  },
};

const horizontalScrollViewProps = {
  horizontal: true,
  showsHorizontalScrollIndicator: false,
};

var radio_props = [
  {label: 'Fulltime', value: 0 },
  {label: 'Part-time', value: 1 },
  {label: 'Temporary', value: 2 },
  {label: 'Volunteer', value: 3 },
  {label: 'Internship', value: 4 },
];

var work_remote = [
  {label: 'Yes', value: 'yes' },
  {label: 'No', value: 'no' },
];

var onsite = [
  {label: 'Yes', value: 'yes' },
  {label: 'No', value: 'no' },
];

var relocate = [
  {label: 'Yes', value: 'yes' },
  {label: 'No', value: 'no' },
];


class SubmitResume extends Component {

  state = {
    name: '',
    ar_name: '',
    email: '',
    location: '',
    jobTitle: "",
    value: 'first',
    horizontalTags: [],
    horizontalText: "",
    description: '',
    companydetails: '',
    tagline: '',
    experience: '',
    stap: 1,
    re_locate: '',
    value:0
  }

  labelExtractor = (tag) => tag;

  onChangeHorizontalTags = (horizontalTags) => {
    this.setState({
      horizontalTags,
    });
  };

  onChangeHorizontalText = (horizontalText) => {
    this.setState({ horizontalText });

    const lastTyped = horizontalText.charAt(horizontalText.length - 1);
    const parseWhen = [',', ' ', ';', '\n'];

    if (parseWhen.indexOf(lastTyped) > -1) {
      this.setState({
        horizontalTags: [...this.state.horizontalTags, this.state.horizontalText],
        horizontalText: "",
      });
      this._horizontalTagInput.scrollToEnd();
    }
  }

  renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios'
    return (<View style={[{ left: radio }, styles.radioCircle]} />)
  }

  render() {
    const { navigation } = this.props;
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => navigation.openDrawer()}
          headerTitle="Resume Details"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />
        <KeyboardAvoidingView style={{ flex: 1, }} behavior="padding" >
          {this.state.stap == 1 ?
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={{ width: "100%" }}>
                <View style={{ alignItems: "center", marginBottom: 20, paddingTop: 30 }}>
                  <Text style={styles.mainjobtitle}>Your Resume details</Text>
                </View>
                <View style={[styles.repeatContainer, { flexDirection: "row", }]}>
                  <View style={{ width: "30%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium }}>Name : </Text>
                  </View>
                  <View style={{ width: "70%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Regular }}>Ritambrata Karak</Text>
                  </View>
                </View>
                <View style={[styles.repeatContainer, { flexDirection: "row", }]}>
                  <View style={{ width: "30%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium }}>Email : </Text>
                  </View>
                  <View style={{ width: "70%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Regular }}>ritambrata.brainium@gmail.com</Text>
                  </View>
                </View>
                <View style={[styles.repeatContainer, { flexDirection: "row", }]}>
                  <View style={{ width: "30%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium }}>Location : </Text>
                  </View>
                  <View style={{ width: "70%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Regular }}>Kolkata, West Bengal</Text>
                  </View>
                </View>
                <View style={[styles.repeatContainer, { flexDirection: "row", }]}>
                  <View style={{ width: "30%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium }}>Job Role : </Text>
                  </View>
                  <View style={{ width: "70%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Regular }}>Web Developer</Text>
                  </View>
                </View>
                <View style={[styles.repeatContainer, { flexDirection: "row", }]}>
                  <View style={{ width: "30%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium }}>Work Summery : </Text>
                  </View>
                  <View style={{ width: "70%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Regular }}>I am a good developer, I am a bad developer, I am a good developer, I am a good developer</Text>
                  </View>
                </View>
                <View style={[styles.repeatContainer, { flexDirection: "row", }]}>
                  <View style={{ width: "30%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium }}>Skill : </Text>
                  </View>
                  <View style={{ width: "70%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Regular }}>React Native, Web Design</Text>
                  </View>
                </View>
                <View style={[styles.repeatContainer, { flexDirection: "row", }]}>
                  <View style={{ width: "30%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium }}>Job Type : </Text>
                  </View>
                  <View style={{ width: "70%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Regular }}>Full Time</Text>
                  </View>
                </View>
                <View style={[styles.repeatContainer, { flexDirection: "row", }]}>
                  <View style={{ width: "30%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium }}>Edutation : </Text>
                  </View>
                  <View style={{ width: "70%" }}>
                    <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Regular }}>BCA, From IIT</Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row", justifyContent: 'space-around', marginBottom: 40 }}>
                  <ButtonComponent
                    buttonName="Add More Resume"
                    backgroundColor1="#73e470"
                    height={50}
                    width="90%"
                    onClick={() => this.setState({
                      stap: 2
                    })}
                  />
                </View>
              </View>
            </ScrollView>
            :
            <ScrollView
              contentContainerStyle={{ width: "100%", }}>
              <View style={{ alignItems: "center", marginBottom: 20, paddingTop: 30 }}>
                <Text style={styles.mainjobtitle}>Your Resume details</Text>
              </View>

              <View style={[styles.repeatContainer2]}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={screenwidth / 1.25}
                  label="Name"
                  value={this.state.name}
                  onChangeText={(val) => this.setState({ name: val })}
                  placeholder="Enter Your Name"
                />
              </View>
              {/* <TextInputComponent
                titleName={"Name"}
                iconType={"MaterialIcons"}
                iconName={"tag-faces"}
                iconSize={20}
                iconColor="grey"
                placeholder={"Enter your name"}
                onChangeText={(val) => this.setState({ name: val })}
              /> */}
              <View style={[styles.repeatContainer2]}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={screenwidth / 1.25}
                  label="أدخل الاسم"
                  value={this.state.ar_name}
                  onChangeText={(val) => this.setState({ ar_name: val })}
                  placeholder="أدخل الاسم باللغة العربية"
                />
              </View>
              {/* <TextInputComponent
                titleName={"أدخل الاسم"}
                iconType={"MaterialIcons"}
                iconName={"tag-faces"}
                iconSize={20}
                iconColor="grey"
                placeholder={"أدخل الاسم باللغة العربية"}
                onChangeText={(val) => this.setState({ ar_name: val })}
              /> */}
              <View style={[styles.repeatContainer2]}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={screenwidth / 1.25}
                  label="Email"
                  value={this.state.email}
                  onChangeText={(val) => this.setState({ email: val })}
                  placeholder="Enter your email"
                />
              </View>
              {/* <TextInputComponent
                titleName={"Email"}
                iconType={"MaterialIcons"}
                iconName={"email"}
                iconSize={20}
                iconColor="grey"
                placeholder={"Enter your email"}
                onChangeText={(val) => this.setState({ email: val })}
              /> */}
              <View style={[styles.repeatContainer2]}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={screenwidth / 1.25}
                  label="Location"
                  value={this.state.location}
                  onChangeText={(val) => this.setState({ location: val })}
                  placeholder="Enter your Location"
                />
              </View>

              {/* <TextInputComponent
                titleName={"Location"}
                iconType={"Octicons"}
                iconName={"location"}
                iconSize={18}
                width={16}
                height={20}
                iconColor="grey"
                placeholder={"Enter Your City name"}
                onChangeText={(val) => this.setState({ location: val })}
              /> */}

              {/* <TextInputComponent
                titleName={"Job title"}
                iconType={"MaterialIcons"}
                iconName={"title"}
                iconSize={20}
                iconColor="grey"
                placeholder={"Enter your Job title"}
                onChangeText={(val) => this.setState({ jobTitle: val })}
              /> */}
              <View style={[styles.repeatContainer2]}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={screenwidth / 1.25}
                  label="Job Title"
                  value={this.state.jobTitle}
                  onChangeText={(val) => this.setState({ jobTitle: val })}
                  placeholder="Enter your Job Title"
                />
              </View>

              <View style={styles.repeatContainer2}>
                <Text style={[styles.title, { marginBottom: 10, }]}>Upload Resume image</Text>
                <ButtonComponent
                  buttonName="Upload Image"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="50%"
                // onClick={() => this.props.navigation.navigate("PostJob")}
                />
              </View>
              <View style={styles.repeatContainer2}>
                <Text style={styles.title}>Work Summary</Text>
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Work Summary"
                  placeholderTextColor="#999"
                  autoCapitalize="none"
                  onChangeText={(val) => this.setState({ description: val })}
                  multiline={true}
                  value={this.state.description}
                  style={styles.mytextinput}
                />
              </View>
              <View style={styles.repeatContainer2}>
                <Text style={styles.title}>Additional Skills</Text>
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Additional Skills"
                  placeholderTextColor="#999"
                  autoCapitalize="none"
                  onChangeText={(val) => this.setState({ companydetails: val })}
                  multiline={true}
                  value={this.state.companydetails}
                  style={styles.mytextinput}
                />
              </View>

              <View style={styles.repeatContainer2}>
                <Text style={styles.title}>Add Education Details</Text>
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Add Education Details"
                  placeholderTextColor="#999"
                  autoCapitalize="none"
                  onChangeText={(val) => this.setState({ companydetails: val })}
                  multiline={true}
                  value={this.state.companydetails}
                  style={styles.mytextinput}
                />
              </View>

              <View style={styles.repeatContainer2}>
                <Text style={styles.title}>Add Experience Details</Text>
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Add Experience Details"
                  placeholderTextColor="#999"
                  autoCapitalize="none"
                  onChangeText={(val) => this.setState({ companydetails: val })}
                  multiline={true}
                  value={this.state.companydetails}
                  style={styles.mytextinput}
                />
              </View>

              <View style={[styles.repeatContainer2, {marginBottom: 0,}]}>
                <Text style={styles.title}>Application Job Type</Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap", marginVertical: 5, }}>
                  <RadioForm
                    radio_props={radio_props}
                    initial={0}
                    formHorizontal={true}
                    labelHorizontal={true}
                    buttonColor={'#45cfd2'}
                    selectedButtonColor={'#45cfd2'}
                    animation={true}
                    onPress={(value) => {this.setState({value:value})}}
                    labelStyle={{marginRight: 10, fontFamily:FontFamily.Poppins_Regular }}
                    buttonSize={10}
                    style={{flexWrap:"wrap"}}
                  />
                  {/* <RadioButton.Group
                    onValueChange={value => this.setState({ value })}
                    value={this.state.value}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                      <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Fulltime</Text>
                      {this.renderRadioCircleIos('64%')}
                      <RadioButton value="first" />
                    </View>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                      <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Parttime</Text>
                      {this.renderRadioCircleIos('67%')}
                      <RadioButton value="second" />
                    </View>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                      <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Temporary</Text>
                      {this.renderRadioCircleIos('70%')}
                      <RadioButton value="third" />
                    </View>
                  </RadioButton.Group> */}
                </View>
              </View>

              <View style={[styles.repeatContainer2, {marginBottom: 0,}]}>
                <Text style={styles.title}>Willing to work remote?</Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap", marginVertical: 5, }}>
                  <RadioForm
                    radio_props={work_remote}
                    initial={0}
                    formHorizontal={true}
                    labelHorizontal={true}
                    buttonColor={'#45cfd2'}
                    selectedButtonColor={'#45cfd2'}
                    animation={true}
                    onPress={(value) => {this.setState({remote:value})}}
                    labelStyle={{marginRight: 10, fontFamily:FontFamily.Poppins_Regular }}
                    buttonSize={10}
                    style={{flexWrap:"wrap"}}
                  />
                  {/* <RadioButton.Group
                    onValueChange={value => this.setState({ remote: value })}
                    value={this.state.remote}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                      <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Yes</Text>
                      {this.renderRadioCircleIos('48%')}
                      <RadioButton value="yes" />
                    </View>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                      <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>No</Text>
                      {this.renderRadioCircleIos('45%')}
                      <RadioButton value="no" />
                    </View>
                  </RadioButton.Group> */}
                </View>
              </View>


              <View style={[styles.repeatContainer2, {marginBottom: 0,}]}>
                <Text style={styles.title}>Willing to work onsite?</Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap", marginVertical: 5, }}>
                  <RadioForm
                    radio_props={onsite}
                    initial={0}
                    formHorizontal={true}
                    labelHorizontal={true}
                    buttonColor={'#45cfd2'}
                    selectedButtonColor={'#45cfd2'}
                    animation={true}
                    onPress={(value) => {this.setState({onsite:value})}}
                    labelStyle={{marginRight: 10, fontFamily:FontFamily.Poppins_Regular }}
                    buttonSize={10}
                    style={{flexWrap:"wrap"}}
                  />
                  {/* <RadioButton.Group
                    onValueChange={value => this.setState({ onsite: value })}
                    value={this.state.onsite}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                      <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Yes</Text>
                      {this.renderRadioCircleIos('48%')}
                      <RadioButton value="yes" />
                    </View>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                      <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>No</Text>
                      {this.renderRadioCircleIos('45%')}
                      <RadioButton value="no" />
                    </View>
                  </RadioButton.Group> */}
                </View>
              </View>


              <View style={[styles.repeatContainer2, {marginBottom: 0,}]}>
                <Text style={styles.title}>Willing to relocate?</Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap", marginVertical: 5, }}>
                  <RadioForm
                    radio_props={relocate}
                    initial={0}
                    formHorizontal={true}
                    labelHorizontal={true}
                    buttonColor={'#45cfd2'}
                    selectedButtonColor={'#45cfd2'}
                    animation={true}
                    onPress={(value) => {this.setState({re_locate:value})}}
                    labelStyle={{marginRight: 10, fontFamily:FontFamily.Poppins_Regular }}
                    buttonSize={10}
                    style={{flexWrap:"wrap"}}
                  />
                  {/* <RadioButton.Group
                    onValueChange={value => this.setState({ re_locate: value })}
                    value={this.state.re_locate}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                      <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Yes</Text>
                      {this.renderRadioCircleIos('48%')}
                      <RadioButton value="yes" />
                    </View>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                      <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>No</Text>
                      {this.renderRadioCircleIos('45%')}
                      <RadioButton value="no" />
                    </View>
                  </RadioButton.Group> */}
                </View>
              </View>

              <View style={styles.repeatContainer2}>
                <Text style={styles.title}>Upload resume file</Text>
                <View style={styles.uploadView}>
                  <Text style={styles.uploadFile}>Upload files</Text>
                </View>
              </View>
              {/* <TextInputComponent
              titleName={"Experience"}
              iconType={"MaterialIcons"}
              iconName={"description"}
              iconSize={18}
              width={16}
              iconColor="grey"
              placeholder={"Years of Experience"}
              onChangeText={(val) => this.setState({ experience: val })}
              keyboardType="numeric"
            /> */}

              <View style={{ flexDirection: "row", justifyContent: 'space-around', marginBottom: 40 }}>
                <ButtonComponent
                  buttonName="Submit Resume Details"
                  backgroundColor1="#73e470"
                  height={50}
                  width="90%"
                  onClick={() => this.props.navigation.navigate("EmployeeHomescreen")}
                />
              </View>
            </ScrollView>
          }
        </KeyboardAvoidingView>
      </View>
    );
  }
}

export default SubmitResume;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
    backgroundColor: "#fff"
  },
  repeatContainer: {
    width: "90%",
    alignItems: "flex-start",
    alignSelf: "center",
    marginBottom: 10,
    backgroundColor: "#fff",
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 10,
    shadowOpacity: 0.1,
    elevation: 3,
    shadowOffset: {
      width: 0,
      height: 2
    },
  },
  repeatContainer2: {
    width: "80%",
    alignItems: "flex-start",
    alignSelf: "center",
    marginBottom: 10,
  },

  mytextinput: {
    backgroundColor: "#fff",
    borderColor: "#999",
    borderWidth: 1,
    borderRadius: 0,
    height: 80,
    width: "100%",
    paddingHorizontal: 10,
    textAlignVertical: 'top'
  },
  title: {
    fontSize: FontSize.small_size,
    fontFamily: FontFamily.Poppins_Medium,
  },
  mainjobtitle: {
    fontSize: FontSize.medium_size,
    fontFamily: "Poppins-Medium",
    color: Colors.BlackColor,
  },
  uploadView: {
    width: "50%",
    height: 40,
    backgroundColor: "#1a2246",
    borderRadius: 50,
    justifyContent: "center",
    marginBottom: 10,
  },
  uploadFile: {
    color: "#fff",
    fontSize: FontSize.small_size,
    fontFamily: "Poppins-Medium",
    fontWeight: "800",
    alignSelf: "center",

  },
  radioCircle: {
    padding: 9,
    borderWidth: 2,
    borderColor: Colors.Nero,
    borderRadius: 100,
    position: 'absolute'
  }
})




