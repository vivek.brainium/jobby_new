import React, { Component } from 'react';
import {TouchableOpacity, View, Text, StyleSheet, Dimensions, FlatList, TextInput, Image, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import ContactList from '../../Components/ContactList';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
import CompanyProfile from '../../Components/CompanyProfile'
import MyStatusBar from '../../Utils/MyStatusBar';
import Colors from '../../Utils/Colors';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;



const MessageData = [
    {
        id: 1,
        name: "Stuart Alexandra",
        jobDescription: "Hiring for React Native Developer",
        image: require('../../Images/EmployerProfile/image.jpg'),
        companyName: "Brainium Information Technologies Pvt. Ltd."
    },
    {
        id: 2,
        name: "Stuart Alexandra",
        jobDescription: "Hiring for React Native Developer",
        image: require('../../Images/EmployerProfile/image.jpg'),
        companyName: "Brainium Information Technologies Pvt. Ltd."
    },
    {
        id: 3,
        name: "Stuart Alexandra",
        jobDescription: "Hiring for React Native Developer",
        image: require('../../Images/EmployerProfile/image.jpg'),
        companyName: "Brainium Information Technologies Pvt. Ltd."
    },
    {
        id: 4,
        name: "Stuart Alexandra",
        jobDescription: "Hiring for React Native Developer",
        image: require('../../Images/EmployerProfile/image.jpg'),
        companyName: "Brainium Information Technologies Pvt. Ltd."
    },
    {
        id: 5,
        name: "Stuart Alexandra",
        jobDescription: "Hiring for React Native Developer",
        image: require('../../Images/EmployerProfile/image.jpg'),
        companyName: "Brainium Information Technologies Pvt. Ltd."
    },
    {
        id: 6,
        name: "Stuart Alexandra",
        jobDescription: "Hiring for React Native Developer",
        image: require('../../Images/EmployerProfile/image.jpg'),
        companyName: "Brainium Information Technologies Pvt. Ltd."
    },
];

class CompanyDetails extends Component {

    state = {
    }

    render() {
        return (
            //Main Container
            <View style={styles.MainContainer}>
                <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
                <HeaderComponent
                    backicon
                    dot
                    noNotification
                    onClick={() => this.props.navigation.goBack()}
                    headerTitle="Message List"
                    type={""}
                    onChangeText={(postjob) => this.setState({ postjob })}
                    placeholder=""
                    notificationonpress={() => this.props.navigation.navigate.goBack()}
                />
                <View style={{ flex: 1, marginTop: 40 }}>

                    <FlatList
                        data={MessageData}
                        renderItem={({ item }) => (
                            <CompanyProfile
                                id={item.id}
                                name={item.name}
                                jobDescription={item.jobDescription}
                                image={item.image}
                                companyName={item.companyName}
                            />
                        )}
                        keyExtractor={item => item.id}
                        showsVerticalScrollIndicator={false}
                    />

                </View>
            </View>
        );
    }
}

export default CompanyDetails;

const styles = StyleSheet.create({
    MainContainer: {
        //width: screenwidth,
        //height: screenHeight,
        flex: 1,
        marginBottom: 40
    },


})