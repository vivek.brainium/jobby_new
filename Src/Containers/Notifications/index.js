import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import NotificationComponent from "../../Components/Notifications"
import Colors from '../../Utils/Colors';


const DATA = [
  {
    id: 1,
    question: "Where does it come from",
    description: "Lorem ipsum is simlpy dumm text of the printing",
    date: "a week ago"
  },
  {
    id: 2,
    question: "Where does it come from",
    description: "Lorem ipsum is simlpy dumm text of the printing",
    date: "a week ago"
  },
  {
    id: 3,
    question: "Where does it come from",
    description: "Lorem ipsum is simlpy dumm text of the printing",
    date: "a week ago"
  },
  {
    id: 4,
    question: "Where does it come from",
    description: "Lorem ipsum is simlpy dumm text of the printing",
    date: "a week ago"
  },
  {
    id: 5,
    question: "Where does it come from",
    description: "Lorem ipsum is simlpy dumm text of the printing",
    date: "a week ago"
  },
  {
    id: 6,
    question: "Where does it come from",
    description: "Lorem ipsum is simlpy dumm text of the printing",
    date: "a week ago"
  }
  ,
  {
    id: 7,
    question: "Where does it come from",
    description: "Lorem ipsum is simlpy dumm text of the printing",
    date: "a week ago"
  }
];

class Notifications extends Component {

  state = {

  }
  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="Notification"
          //notificationonpress={()=> this.props.navigation.navigate("Notification")}
        />

        <FlatList
          data={DATA}
          renderItem={({ item }) => (
            <NotificationComponent
              id={item.id}
              Question={item.question}
              Description={item.description}
              Date={item.date}
            />
          )}
          keyExtractor={item => item.id}
        />

      </View >
    );
  }
}

export default Notifications;

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1
  }
})