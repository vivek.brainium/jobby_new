import React, { Component } from 'react';
import { View, Text, ImageBackground, StyleSheet, StatusBar, Dimensions, Image, TouchableOpacity, Platform, SafeAreaView } from 'react-native';
import Colors from "./../../Utils/Colors";
import FontSize from "./../../Utils/fonts";
import TextInputComponent from "./../../Components/Login/TextInput"
import ButtonComponent from './../../Components/Button/loginButton'
import AsyncStorage from '@react-native-community/async-storage';
import { RadioButton } from 'react-native-paper';
import FontFamily from '../../Utils/FontFamily';
import Apis from '../../Network/apicall';
import Toast from 'react-native-simple-toast';
import FloatingLabelInput from '../../Components/FlotingTextInput';
import { save_user } from '../../actions/userAction';
import { connect } from 'react-redux';
import MyStatusBar from '../../Utils/MyStatusBar';
const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

import LinkedInModal from 'react-native-linkedin';
import { CheckBox } from 'react-native-elements';
import Entypo from 'react-native-vector-icons/Entypo';


class Login extends Component {


  state = {
    passIcon: true,
    email: "",
    password: "",
    storage: "",
    type: "Employer",
    employer: true,
    employee: false
  }

  async componentDidMount() {
    const storage = await AsyncStorage.getItem('usertype');
    this.setState({
      storage: storage
    })
  }

  // <======= Password show and hide functionlity =========>
  handleIconChange() {
    if (this.state.passIcon == true) {
      this.setState({ passIcon: false })
    } else {
      this.setState({ passIcon: true })
    }
  }

  // <======= for radio button cicle =========>
  renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios'
    return (<View style={[{ left: radio }, styles.radioCircle]} />)
  }

  // <======= Email Validation =========>
  validate = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      // Toast.show('Email is not Correct');
      this.setState({ email: "incorrect" })
      return false;
    }
    else {
      this.setState({ email: text })
      // Toast.show('Email is Correct');
    }
  }

  // <======= Employer Login api all =========>
  employer_login = () => {
    if (this.state.email == '' || this.state.password == '') {
      Toast.show('Field Cannot be blanked');
    }
    else if (this.state.email == "incorrect") {
      Toast.show('Email is incorrect');
    }
    else {
      const apidata = {
        "email": this.state.email,
        "password": this.state.password,
        "devicetoken": 123,
        "apptype": Platform.OS === 'android' ? "ANDROID" : 'IOS'
      }
      Apis.employer_login(apidata)
        .then((res) => {
          if (res.response_code == 2000) {
            this.setdata(res.response_data)
            console.log(res.response_data, "response data");
            Toast.show("Success  " + res.response_message);
            //this.props.navigation.navigate('EmployeeHomescreen');
          }
          else {
            Toast.show(" " + res.response_message);
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }

  // <======= Employee Login api all =========>
  employee_login = () => {
    if (this.state.email == '' || this.state.password == '') {
      Toast.show('Field Cannot be blanked');
    }
    else if (this.state.email == "incorrect") {
      Toast.show('Email is incorrect');
    }
    else {
      const apidata = {
        "email": this.state.email,
        "password": this.state.password,
        "devicetoken": 123,
        "apptype": Platform.OS === 'android' ? "ANDROID" : 'IOS'
      }
      Apis.employee_login(apidata)
        .then((res) => {
          if (res.response_code == 2000) {
            this.setdata(res.response_data)
            console.log(res.response_data, "response data");
            Toast.show("Success  " + res.response_message);
            //this.props.navigation.navigate('EmployeeHomescreen');
          }
          else {
            Toast.show(" " + res.response_message);
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }

  setdata = async (res) => {
    await AsyncStorage.setItem("authtocken", res.authtoken);
    this.props.save_user(res.authtoken)
    await AsyncStorage.setItem("usertype", res.user_type);
  }

  getdata = async (token) => {
    const access_token = token.access_token;
    const response = await fetch(
      `https://api.linkedin.com/v2/me`,
      {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + access_token
        }
      }
    )
    const payload = await response.json()
    console.log(payload, "payload");
    await AsyncStorage.setItem("authtocken", access_token);
    this.props.save_user(access_token)
  }

  // linkedRef = React.createRef()


  LinkedLogin = () => {
    this.modal.open()
  }

  test() {
    return (
      <LinkedInModal
        ref={ref => { this.modal = ref; }}
        clientID="8180byd83wdrxd"
        clientSecret="DuO4AN9zSVrbp0gz"
        redirectUri="https://www.linkedin.com/developer/apps"
        onSuccess={(token) => this.getdata(token)}
        permissions={['r_emailaddress', 'r_liteprofile', 'w_member_social',]}
        renderButton={() => {
          return (
            <View style={{ marginBottom: 20, alignItems: "center" }}>
            <Entypo
                style={{ position:"absolute", zIndex:99999, left:"22%", top:"20%" }}
                name="linkedin"
                color="#fff"
                size={25}
              />
            <ButtonComponent
              backgroundColor1="#0e76a8"
              height={50}
              width="80%"
              buttonName="Sign In With Linkedin"
              onClick={() => this.LinkedLogin()}
              align="flex-end"
              right={"14%"}
            
            />
          </View>
            // <TouchableOpacity style={{ alignItems: "center", position: "relative" }} onPress={}>
            //   <Image source={require('../../Images/Share/images.png')} style={{ width: "60%", height: 40, borderRadius: 20 }} />
            // </TouchableOpacity>
          )
        }}
      />
    )
  }

  // for checkbok true and false

  changeRadio_1 = () => {
    if (this.state.employer) {
      this.setState({ employee: false, employer: true })
    } else {
      this.setState({ employee: false, employer: true })
    }
  }

  changeRadio_2 = () => {
    if (this.state.employee) {
      this.setState({ employee: true, employer: false })
    } else {
      this.setState({ employee: true, employer: false })
    }
  }

  render() {
    return (
      <ImageBackground style={styles.container}
        source={require("../../Images/Login/login_bckground.png")}
      >
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <View style={styles.logo_container}>
          <Image source={require("./../../Images/Login/logo.png")}
            style={styles.logo_image}
          />
        </View>
        {
          this.state.storage == "employers" ?
            <View style={{ alignItems: "center", justifyContent: "center", marginBottom: 25 }}>
              <Text style={{ fontSize: FontSize.extra_large, fontFamily: "Poppins-Medium", color: Colors.BlackColor }}>Employer Login</Text>
            </View>
            :
            <View style={{ alignItems: "center", justifyContent: "center", marginBottom: 25 }}>
              <Text style={{ fontSize: FontSize.extra_large, fontFamily: "Poppins-Medium", color: Colors.BlackColor }}>Employee Login</Text>

            </View>
        }
        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputWidth={"100%"}
            label="Email"
            textInputHeight={40}
            onChangeText={(val) => this.validate(val)}
            placeholder={"Enter your email"}
            autoCapitalize="none"
          />
        </View>

        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputWidth={"100%"}
            textInputHeight={40}
            label="Password"
            value={this.state.password}
            onChangeText={(val) => this.setState({ password: val })}
            placeholder={"Enter your Password"}
            secureTextEntry={true}
          />
        </View>

        <TouchableOpacity
          style={{ marginBottom: 25 }}
          onPress={() => this.props.navigation.navigate("ForgetPassword")}>
          <Text style={{ textAlign: "center", fontSize: FontSize.small_size }}>Forget pasword</Text>
        </TouchableOpacity>

        {this.state.storage == "employers" ?
          <View style={{  alignItems: "center" }}>
            <ButtonComponent
              backgroundColor1="#1a2246"
              height={50}
              width="80%"
              buttonName="Sign In"
              onClick={() => this.employer_login()}
            // onClick={() => {this.props.navigation.navigate("DrawerComponent"), AsyncStorage.setItem("usertype", "employers")}}
            />
          </View>
          :
          <View style={{  alignItems: "center" }}>
            <ButtonComponent
              buttonName="Sign In"
              backgroundColor1="#1a2246"
              height={50}
              width="80%"
              onClick={() => this.employee_login()}
            // onClick={() => {this.props.navigation.navigate("DrawerComponent"),
            // AsyncStorage.setItem("usertype", "employees")}}
            />
          </View>
        }

          <Text style={{ textAlign: "center", paddingVertical: 25, fontSize:FontSize.small_size }}>OR</Text>

          {this.test()}
         
        <View
          style={{ alignItems: "center", justifyContent: "center", flexDirection: "row", paddingVertical:5, }}>
          <Text style={styles.newUser_Text}>New User? </Text>
          {this.state.storage == "employers" ?
          <TouchableOpacity onPress={()=> this.props.navigation.navigate('EmployerRegistration')}>
            <Text style={{ color: "#1a2246", fontSize: FontSize.medium_size }}> Sign Up</Text>
          </TouchableOpacity>
          :
          <TouchableOpacity onPress={()=> this.props.navigation.navigate('EmployeeRegistration')}>
            <Text style={{ color: "#1a2246", fontSize: FontSize.medium_size }}> Sign Up</Text>
          </TouchableOpacity>
          }
        </View>
      </ImageBackground>
    );
  }
}



export default connect(null, { save_user })(Login)

const styles = StyleSheet.create({
  container: {
    width: screenwidth,
    height: screenHeight
  },
  logo_container: {
    alignItems: "center",
    height: screenHeight / 4,
    justifyContent: "center",
  },
  logo_image: {
    height: 39,
    width: 174
  },
  newUser_Text: {
    color: "#696969",
    fontSize: FontSize.small_size
  },
  SignUp_text: {
    color: "#1a2245",
    fontSize: FontSize.small_size
  },
  repeatContainer: {
    width: "80%", alignSelf: "center", marginBottom: 15
  },
  radioCircle: {
    padding: 9,
    borderWidth: 2,
    borderColor: Colors.Nero,
    borderRadius: 100,
    position: 'absolute'
  },
});