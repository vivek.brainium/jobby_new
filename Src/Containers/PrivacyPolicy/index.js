import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, ScrollView, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontFamily from '../../Utils/FontFamily';
import Font from '../../Utils/fonts';
import Colors from '../../Utils/Colors';


class PrivacyPolicy extends Component {
  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="Privacy Policy"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />
        <ScrollView>
          <View style={styles.secondcontainer}>
            <View style={styles.repeatcontainer}>
              <Text style={styles.title}>Where does it come from?</Text>
              <Text style={styles.description}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</Text>
            </View>
            <View style={styles.repeatcontainer}>
              <Text style={styles.title}>Why do we use it?</Text>
              <Text style={styles.description}>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default PrivacyPolicy;

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    backgroundColor: '#e9e9e9',
  },
  secondcontainer:{
    padding:25,
  },
  repeatcontainer:{
    marginBottom:20
  },
  title:{
    fontSize:Font.medium_size, fontFamily:FontFamily.Poppins_Semibold, marginBottom:10
  },
  description:{
    lineHeight:25, color:Colors.lightgray, fontFamily:FontFamily.Poppins_Light
  }
})