import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FAQComponent from "../../Components/FAQ"
import Colors from '../../Utils/Colors';


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const DATA = [
  {
    id: 1,
    title: "Where can i find the save job ?",
    description: "It is a long estiblished fact that a reader will be distracted by readable content of page when looking as its layout. ",
  },
  {
    id: 2,
    title: "The Recommended Jobs are not getting update?",
    description: "It is long estiblished fact that a reader will be distracted.",
  },
  {
    id: 3,
    title: "The Recommended Jobs are not relevant?",
    description: "It is long estiblished fact that a reader will be distracted.",
  },
  {
    id: 4,
    title: "How many Jobs can I apply to in a day?",
    description: "It is long estiblished fact that a reader will be distracted.",
  },
  {
    id: 5,
    title: "How many Jobs can I apply to in a month?",
    description: "It is long estiblished fact that a reader will be distracted.",
  },
];

class FAQ extends Component {

  state = {
    postjob:''
  }
  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <View>
          <HeaderComponent
            onClick={() => this.props.navigation.openDrawer()}
            headerTitle="FAQ"
            onChangeText={(postjob) => this.setState({ postjob })}
            placeholder="Search"
            notificationonpress={()=> this.props.navigation.navigate("Notification")}
          />
        </View>

        <FlatList
          data={DATA}
          renderItem={({ item }) => (
            <FAQComponent
              id={item.id}
              title={item.title}
              description={item.description}
            />
          )}
          keyExtractor={item => item.id}
        />

      </View >
    );
  }
}

export default FAQ;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  }
})