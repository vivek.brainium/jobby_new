import React, { Component } from 'react';
import { View, Text, ImageBackground, StyleSheet, StatusBar, Dimensions, Image, TouchableOpacity, SafeAreaView } from 'react-native';
import Colors from "../../Utils/Colors";
import FontSize from "../../Utils/fonts";
import FontFamily from "../../Utils/FontFamily";
import ButtonComponent from '../../Components/Button/loginButton'
import AsyncStorage from '@react-native-community/async-storage';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;


class Landing extends Component {

  // componentDidMount(){
  //   console.log(this.props.route.params.signin, "signin");
  //   console.log(this.props.route.params.registration, "registration");
  // }

  employer = async () =>{
    await AsyncStorage.setItem("usertype", "employers")
    { this.props.route.params.signin ?
      this.props.navigation.navigate("Login")
      :
      this.props.navigation.navigate("EmployerRegistration")
    }
  }

  application = async () =>{
    await AsyncStorage.setItem("usertype", "employees")
    { this.props.route.params.signin ?
      this.props.navigation.navigate("Login")
      :
      this.props.navigation.navigate("EmployeeRegistration")
    }
    
  }

  render() {
    return (
      <ImageBackground style={styles.container}
        source={require("./../../Images/home/bg.jpg")}
      >
       <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />

        <View style={styles.logo_container}>
          <Image source={require("./../../Images/Login/logo.png")}
            style={styles.logo_image}
          />
        </View>

        <View style={{ flexDirection: "row", justifyContent: 'space-around', alignItems: "center", paddingHorizontal: 20 }}>
          <ButtonComponent
            buttonName="Employer"
            backgroundColor1={Colors.Nero}
            height={40}
            width="41%"
            onClick={() => this.employer()}
          />

          <View style={{ alignItems: "center" }}>
            <Text style={styles.text_container}>or</Text>
          </View>

          <ButtonComponent
            buttonName="Applicant"
            backgroundColor1={Colors.Nero}
            height={40}
            width="41%"
            onClick={() => this.application()}
          />
        </View>

      </ImageBackground>
    );
  }
}

export default Landing;


const styles = StyleSheet.create({
  container: {
    width: screenwidth,
    height: screenHeight
  },
  logo_container: {
    alignItems: "center",
    height: screenHeight / 3,
    justifyContent: "center",
    flex: 0.8
  },
  logo_image: {
    height: 39,
    width: 174
  },
  text_container: {
    fontSize: FontSize.small_size, 
    color: Colors.Nero, 
    fontFamily: FontFamily.Poppins_Medium
  }
});
