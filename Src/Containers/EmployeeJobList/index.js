import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import Employee_JobList from '../../Components/EmployeeJoblist';
import Colors from '../../Utils/Colors';



const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const DATA = [
  {
    id: 1,
    CompanyName: "Brainium Information Technologies pvt ltd.",
    title: "Hr Executive",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
  {
    id: 2,
    CompanyName: "Brainium Information Technologies pvt ltd.",
    title: "Sr Software Developer",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
  {
    id: 3,
    CompanyName: "Brainium Information Technologies pvt ltd.",
    title: "Dotnet Developer",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
  {
    id: 4,
    CompanyName: "Brainium Information Technologies pvt ltd.",
    title: "Dotnet Developer",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
  {
    id: 5,
    CompanyName: "Brainium Information Technologies pvt ltd.",
    title: "Sr Software Developer",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
];

class EmployeeJobList extends Component {

  state = {
    postjob: ''
  }

  handlerDetails = (props) => {
    console.log("View Job ", props);
    this.props.navigation.navigate('ApplyJob');
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex: 0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="Job List"
          type={"textinput"}
          onChangeText={(postjob) => this.setState({ postjob })}
          placeholder="Search Job"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />
        <View style={{ flex: 1 }}>
          <FlatList
            data={DATA}
            renderItem={({ item }) => (
              <Employee_JobList
                id={item.id}
                CompanyName={item.CompanyName}
                title={item.title}
                years={item.years}
                locationname={item.locationname}
                desingnation={item.desingnation}
                posteddate={"Posted " + item.posteddate}
                onClick={() => this.handlerDetails}
                show={this.state[item.id]}
                gotodetails={() => this.setState({ [item.id]: !this.state[item.id] })}
                apply={() => { this.props.navigation.navigate('ApplyJob'); this.setState({ [item.id]: false }) }}
                savenow={() => { this.props.navigation.navigate('SaveJob'); this.setState({ [item.id]: false }) }}
              />
            )}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    );
  }
}

export default EmployeeJobList;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  }
})