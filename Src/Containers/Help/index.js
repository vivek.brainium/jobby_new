import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import Colors from '../../Utils/Colors';
import Font from '../../Utils/fonts';
import { TouchableOpacity } from 'react-native-gesture-handler';


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const DATA = [
  {
    id: 1,
    title: "About Job Finder Setting",
  },
  {
    id: 2,
    title: "Cann't Find Previous Location Setting",
  },
  {
    id: 3,
    title: "Protect against harmful apps",
  },

];

class Help extends Component {

  state = {
    postjob: ''
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="Help"
          notificationonpress={()=> this.props.navigation.navigate("Notification")}
        />

        <View style={styles.bodyContainer}>
          <View style={styles.SuggestionContainer}>
            <Text style={styles.Suggestion}>Suggestions</Text>
          </View>

          <FlatList
            data={DATA}
            renderItem={({ item }) => (
              <View style={styles.arraybody}>
                <TouchableOpacity style={styles.arraycontainer}>
                  <Text style={styles.title}>{item.title}</Text>
                </TouchableOpacity >
              </View>
            )}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    );
  }
}

export default Help;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
    backgroundColor:"#fff"
  },
  title: {
    fontWeight: '600',
    fontFamily: 'Poppins-Medium',
    fontSize: Font.small_size,
    color: "#5b5b5b"
  },
  Suggestion: {
    fontSize: Font.large_size,
    color: "#000",
    fontWeight: '600',
    fontFamily: 'Poppins-Medium',
  },
  SuggestionContainer: {
    paddingTop: 25,
    paddingBottom:15
  },
  bodyContainer:{
    paddingHorizontal: 25,
  },
  arraycontainer:{
    backgroundColor: "#ffffff", paddingVertical: 10,
  },
  arraybody:{
    backgroundColor: "#fff", marginBottom: 2, borderBottomColor:"#d0d0d0", borderBottomWidth:1  
  }
})