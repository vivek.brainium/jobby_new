import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from '../../Components/Button/loginButton'
import FontFamily from '../../Utils/FontFamily';
import Colors from '../../Utils/Colors';
import Employee_JobList from '../../Components/EmployeeJoblist'

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const state = {
  //searchjob: '',

}
const DATA = [
  {
    id: 1,
    CompanyName: "Berkshire Hathaway Inc.",
    Designation: "HR Executive",
    Location: "California",
    Experience: "4 - 6 years",
    Skills: 'MBA',
  },
  {
    id: 2,
    CompanyName: "Berkshire Hathaway Inc.",
    Designation: "HR Executive",
    Location: "California",
    Experience: "4 - 6 years",
    Skills: 'MBA',
  },
  {
    id: 3,
    CompanyName: "Berkshire Hathaway Inc.",
    Designation: "HR Executive",
    Location: "California",
    Experience: "4 - 6 years",
    Skills: 'MBA',
  },
  {
    id: 4,
    CompanyName: "Berkshire Hathaway Inc.",
    Designation: "HR Executive",
    Location: "California",
    Experience: "4 - 6 years",
    Skills: 'MBA',
  },
  {
    id: 5,
    CompanyName: "Berkshire Hathaway Inc.",
    Designation: "HR Executive",
    Location: "California",
    Experience: "4 - 6 years",
    Skills: 'MBA',
  },
];

class SaveJobListing extends Component {

  state={
    postjob: ''
  }

  navigationHandler = () => {
    this.props.navigation.navigate('SaveJob');
  }

  render() {
    const { navigation } = this.props;
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => navigation.openDrawer()}
          headerTitle={"Save Job List"}
          type={"textinput"}
          //onChangeText={(searchjob)=> this.setState({searchjob})}
          placeholder="Search Job"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />
        <FlatList
          data={DATA}
          renderItem={({ item }) => (
            <Employee_JobList
              CompanyName={item.CompanyName}
              title={item.Designation}
              locationname={item.Location}
              years={item.Experience}
              desingnation={item.Skills}
              // firstButton= "Apply"
              // firstButtonColor= "#f67665"
              // secondButton= "Remove"
              // secondButtonColor= "#b4b4b4"
              onClick={() => this.navigationHandler}
              savejoblist={this.state[item.id]}
              gotodetails={() => this.setState({ [item.id]: !this.state[item.id] })}
              apply={()=> {this.props.navigation.navigate('ApplyJob'); this.setState({ [item.id]: false })}}
              
            />
          )}
          keyExtractor={item => item.id}
        />



      </View>
    );
  }
}

export default SaveJobListing;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  },

})