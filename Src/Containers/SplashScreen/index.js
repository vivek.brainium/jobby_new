import React, { Component } from 'react';
import {TouchableOpacity, View, Text, StyleSheet, Dimensions, FlatList,TextInput,Image ,ImageBackground} from 'react-native';
import HeaderComponent from "../../Components/Header"
import ContactList from '../../Components/ContactList';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
// import {  } from 'react-native-gesture-handler';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import Colors from '../../Utils/Colors';


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;


class Splash extends Component {

  state = {
    postjob: '',
    text:""
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
       <ImageBackground source={require('../../Images/splash/bg.png')} style={{width: '100%', height: '100%'}}>
          <View style={{}}>
          <Image source={require('../../Images/splash/Logo.png')} style={{width:"60%",height:100,alignSelf:"center",marginTop:"50%"}} resizeMode="contain"></Image>
          <Text style={{fontSize:FontSize.large_size,alignSelf:'center',marginTop:'45%'}}>L O A D I N G . . .</Text>
          </View>
       </ImageBackground>
      </View>
    );
  }
}

export default Splash;

const styles = StyleSheet.create({
  MainContainer: {
    // width: screenwidth,
    // height: screenHeight,
    flex:1,
  },
  
})