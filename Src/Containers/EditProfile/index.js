import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, SafeAreaView, StatusBar, Image, TouchableOpacity, ScrollView } from 'react-native';
import Colors from "../../Utils/Colors";
import FontSize from "../../Utils/fonts";
import Ionicons from 'react-native-vector-icons/Ionicons';
import TextInputComponent from "../../Components/Login/TextInput"
import ButtonComponent from '../../Components/Button/loginButton'
import AsyncStorage from '@react-native-community/async-storage';
import ContactInfo from '../../Components/MainFom/ContactInfo';
import FontFamily from '../../Utils/FontFamily';
import HeaderComponent from '../../Components/Header';
import FloatingLabelInput from '../../Components/FlotingTextInput';


const screenHeight = Dimensions.get('window').height
const screenwidth = Dimensions.get('window').width


class EditProfile extends Component {

  state = {
    passIcon: true,
    email: "",
    password: "",
    confirmPassword: "",
    fName: "",
    lName: "",
    cName: "",
    storage: '',
    profileName: "John Carry",
    profileEducation: "Oxford University,  London",
    profileAge: 30,
    profilePhone: "23785745",
    profileEmail: "john@gmail.com",
    profileSkills: "C, JAVA, REACT.JS, REACT NATIVE,NODE.JS",
    employmentCompany: "Brainium Information Technologies Pvt. Ltd.",
    employmentDuration: "January 2014 to till now",
    employmentDescription: "I have working as a fullstack developer ",
    educationCourse: "B.Tech",
    universityName: "Oxford University",
    universityLocation: "London",
    BOD: "02/02/1990",
    Gender: "Male",
    Category: "General",
    hometown: "Kolkata",
    PinCode: "721423",
    Status: "Single",
    Address: "Kolkata, West Bengal, India",
    Lenguage: "English, Hindi",
  }

  async componentDidMount() {
    const storage = await AsyncStorage.getItem('usertype');
    this.setState({
      storage: storage
    })
    console.log(this.state.storage, "storage");
  }

  handleIconChange() {
    if (this.state.passIcon == true) {
      this.setState({ passIcon: false })
    } else {
      this.setState({ passIcon: true })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          backicon
          Notification
          onClick2={() => this.props.navigation.goBack()}
          headerTitle="Edit Profile"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
          headingLeft={10}
        />


        {this.state.storage == "employers" ?
          <View style={{
            width: screenwidth,
            height: screenHeight / 1.14,
            // justifyContent: "center",
            paddingTop: 30
          }}>
            <View style={styles.InputTypeContainer}>
              <FloatingLabelInput
                textInputHeight={35}
                textInputWidth={screenwidth / 1.18}
                label="First Name"
                value={this.state.fName}
                onChangeText={(val) => this.setState({ fName: val })}
                placeholder="First Name"
              />
            </View>
           
            <View style={styles.InputTypeContainer}>
              <FloatingLabelInput
                textInputHeight={35}
                textInputWidth={screenwidth / 1.18}
                label="Last Name"
                value={this.state.lName}
                onChangeText={(val) => this.setState({ lName: val })}
                placeholder="Last Name"
              />
            </View>
            
            <View style={styles.InputTypeContainer}>
              <FloatingLabelInput
                textInputHeight={35}
                textInputWidth={screenwidth / 1.18}
                label="Phone Number"
                value={this.state.phone}
                onChangeText={(val) => this.setState({ phone: val })}
                placeholder="Phone Number"
              />
            </View>
            

            <View style={{ marginTop: 30, alignItems: "center",marginBottom:20 }}>
              <ButtonComponent
                buttonName="Submit"
                backgroundColor1="#73e470"
                height={50}
                width="80%"
              // onClick={() => this.props.navigation.navigate("CompanyDetails")}
              />
            </View>
            <View style={{ alignItems: "center", flexDirection: "row", alignSelf: "center" }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("ChangePassword")} activeOpacity={0.8}>
                <Text style={{ color: "#1a2246", fontSize: FontSize.small_size }}> Change Password</Text>
              </TouchableOpacity>
              <Text> |</Text>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("CompanyEdit")} activeOpacity={0.8}>
                <Text style={{ color: "#1a2246", fontSize: FontSize.small_size }}> Edit Company Details</Text>
              </TouchableOpacity>
            </View>
          </View>
          :
          <View style={{ flex: 1 }}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={{ width: "90%", alignSelf: "center", marginBottom: 30 }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('ContactInfo')}>
                </TouchableOpacity>
                <View style={{ flexDirection: "row", marginTop: 20, }}>
                  <View style={{ width: 80, height: 90, marginRight: 30 }}>
                    <Image resizeMode="contain" style={styles.HeaderImage} source={require('../../Images/EmployerProfile/image.jpg')} />
                  </View>
                  <View style={{}}>
                    <Text style={styles.nameHeader}>{this.state.profileName}</Text>
                    <Text style={styles.EducationHeader}>{this.state.profileEducation}</Text>
                    <Text style={styles.YearHeader}>{this.state.profileAge}</Text>
                  </View>
                </View>

                <View style={styles.repeatContainer}>
                  <View style={styles.ProfileTextContainer}>
                    <Text style={styles.contact}>Contact</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("MainFrom", { step: 2, singleButton: "singleButton" })}>
                      <Text style={styles.add}>Add</Text>
                    </TouchableOpacity>
                  </View>
                  <View>
                    <Text style={styles.text}>{this.state.profileEmail}</Text>
                    <Text style={styles.text}>{this.state.profilePhone}</Text>
                  </View>
                </View>

                <View style={styles.repeatContainer}>
                  <View style={styles.ProfileTextContainer}>
                    <Text style={styles.contact}>Skills</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("MainFrom", { step: 8, singleButton: "singleButton" })}>
                      <Text style={styles.add}>Add</Text>
                    </TouchableOpacity>
                  </View>
                  <View>
                    <Text style={styles.text}>{this.state.profileSkills}</Text>
                  </View>
                </View>

                <View style={styles.repeatContainer}>
                  <View style={styles.ProfileTextContainer}>
                    <Text style={styles.contact}>Employment</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("MainFrom", { step: 4, singleButton: "singleButton" })}>
                      <Text style={styles.add}>Add</Text>
                    </TouchableOpacity>
                  </View>
                  <View>
                    <Text style={styles.text}>{this.state.employmentCompany}</Text>
                    <Text style={styles.text}>{this.state.employmentDuration}</Text>
                    <Text style={styles.text}>{this.state.employmentDescription}</Text>
                  </View>
                </View>

                <View style={styles.repeatContainer}>
                  <View style={styles.ProfileTextContainer}>
                    <Text style={styles.contact}>Education</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("MainFrom", { step: 6, singleButton: "singleButton" })}>
                      <Text style={styles.add}>Add</Text>
                    </TouchableOpacity>
                  </View>
                  <View>
                    <Text style={styles.text}>{this.state.educationCourse}</Text>
                    <Text style={styles.text}>{this.state.universityName},  {this.state.universityLocation}</Text>
                    <Text style={styles.text}></Text>
                  </View>
                </View>

                <View style={styles.repeatContainer}>
                  <View style={styles.ProfileTextContainer}>
                    <Text style={styles.contact}>Other Details</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("MainFrom", { step: 1, singleButton: "singleButton" })}>
                      <Text style={styles.add}>Add</Text>
                    </TouchableOpacity>
                  </View>
                  <View>
                    <Text style={styles.text}>Date of Birth - {this.state.BOD}</Text>
                    <Text style={styles.text}>Gender - {this.state.gender}</Text>
                    <Text style={styles.text}>Category - {this.state.Category}</Text>
                    <Text style={styles.text}>Hometown - {this.state.hometown}</Text>
                    <Text style={styles.text}>Area Pin Code - {this.state.PinCode}</Text>
                    <Text style={styles.text}>Marital Status - {this.state.Status}</Text>
                    <Text style={styles.text}>Permanent Address - {this.state.Address}</Text>
                  </View>
                </View>

                <View style={styles.repeatContainer}>
                  <View style={styles.ProfileTextContainer}>
                    <Text style={styles.contact}>Languages Known</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("MainFrom", { step: 7, singleButton: "singleButton" })}>
                      <Text style={styles.add}>Add</Text>
                    </TouchableOpacity>
                  </View>
                  <View>
                    <Text style={styles.text}>{this.state.Lenguage}</Text>
                  </View>
                </View>

              </View>
            </ScrollView>
          </View>
        }
      </View>
    );
  }
}

export default EditProfile;

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    flex: 1
  },
  newUser_Text: {
    color: "#696969",
    fontSize: FontSize.small_size
  },
  SignUp_text: {
    color: "#1a2245",
    fontSize: FontSize.small_size
  },
  nameHeader: {
    fontSize: FontSize.medium_size,
    //fontFamily: "Poppins-Regular",
    fontWeight: '700'
  },
  EducationHeader: {
    fontSize: FontSize.small_size,
    fontFamily: "Poppins-Regular"
  },
  YearHeader: {
    fontSize: FontSize.small_size,
    fontFamily: "Poppins-Regular"
  },
  HeaderImage: {
    height: "100%",
    width: "100%",
    resizeMode: "cover"
  },
  contact: {
    fontSize: FontSize.medium_size,
    fontFamily: FontFamily.Poppins_Medium
  },
  add: {
    fontSize: FontSize.medium_size,
    fontFamily: FontFamily.Poppins_Medium,
    color: "#09c"
  },
  text: {
    fontSize: FontSize.small_size,
    fontFamily: FontFamily.Poppins_Medium,
    color: "#666"
  },
  ProfileTextContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingRight: 5,
    marginBottom: 10
  },
  repeatContainer: {
    marginTop: 15, backgroundColor: "#fff", paddingVertical: 10, paddingHorizontal: 10, borderRadius: 5, shadowOpacity: 0.1, elevation: 3, shadowOffset: { width: 0, height: 2 }, shadowColor: "#000"
  },
  InputTypeContainer: {
    width: "85%", alignSelf: "center", marginBottom: 15
  }
})