import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, ScrollView, SafeAreaView, TouchableOpacity, TextInput } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from '../../Components/Button/loginButton'
//import {  } from 'react-native-gesture-handler';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from "./../../Utils/Colors";
import FontFamily from '../../Utils/FontFamily';
import AsyncStorage from '@react-native-community/async-storage';
const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;
import LinearGradient from 'react-native-linear-gradient';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as Progress from 'react-native-progress';
const DATA = [
  {
    id: 1,
    name: "Stuart Alexandra",
    desingnation: "Project Manager",
    companyname: "Brainium Information Technology",
    updated: "Last updated 2 days ago.",
    profilePercentage: 89,
    image: require('../../Images/EmployerProfile/image.jpg')
  },
];


class EmployeeHomescreen extends Component {
  state = {
    postjob: '',
    storage: '',
    search: ""
  }

  async componentDidMount() {
    const storage = await AsyncStorage.getItem('usertype');
    this.setState({
      storage: storage
    })
    console.log(this.state.storage, "storage");
  }


  render() {
    return (
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex: 0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          message
          dot
          onClick={() => this.props.navigation.openDrawer()}
          notificationonpress={() => this.props.navigation.navigate("Notification")}
          messageiconpress={() => this.props.navigation.navigate("MessageScreen")}
          drawerLeft={7}
        />

        <View>
          <LinearGradient
            start={{ x: 0, y: 1 }}
            end={{ x: 1, y: 1 }}
            colors={['#45cfd2', '#57c998',]} style={styles.linearGradient}>
            {
              this.state.storage == "employees" ?

                <View style={{ flexDirection: "row", alignItems: "center", elevation: 2, backgroundColor: "#fff", marginHorizontal: 20, height: 50, borderRadius: 5, }}>
                  <View style={{ flex: 0.85 }}>
                    <TextInput
                      style={{ paddingHorizontal: 10, fontSize: FontSize.small_size, fontWeight: "600" }}
                      value={this.state.search}
                      placeholder="Search Job"
                      placeholderTextColor="grey"
                      onChangeText={(text) => this.setState({ search: text })}
                      autoCapitalize='none'
                      autoCorrect={false}
                      textColor="#999"
                      returnKeyType='done'
                      enablesReturnKeyAutomatically={true}
                      selectTextOnFocus={true}
                      spellCheck={false}
                    />
                  </View>
                  <TouchableOpacity
                    style={{ flex: 0.15, alignItems: "center" }}
                    activeOpacity={0.7}>
                    <Ionicons
                      name="ios-search"
                      size={20}
                      color="gray"
                    />
                  </TouchableOpacity>
                </View>
                : null
            }

            <View style={{ width: "85%", alignSelf: "center" }}>
              <View style={styles.profileImageView}>
                <Image source={require('../../Images/EmployerProfile/image.jpg')} style={styles.profileImage} />
                <TouchableOpacity style={{ position: "absolute", bottom: 0, right: 0, backgroundColor: "#fff", borderRadius: 50, paddingVertical: 5, paddingHorizontal: 3 }} activeOpacity={0.8}>
                  <EvilIcons
                    name="pencil"
                    size={20}
                    color="gray"
                  />
                </TouchableOpacity>
              </View>
              <View style={{ alignSelf: "center", }}>
                <Text style={styles.profileName}>Stuart Alexandra</Text>
                <Text style={styles.profileDesignation}>Project Manager - Brainium Information Technology</Text>
              </View>
              <View style={styles.percentageContainer}>
                <Text style={styles.profilePercentage}>69% Completed</Text>
                <Text style={styles.profleUpdate}>Last updated 2 days ago.</Text>
              </View>

              <View style={{ paddingTop: 3 }}>
                <Progress.Bar
                  progress={0.69}
                  width={screenwidth / 1.18}
                  color={"#fdad4e"}
                  unfilledColor={"#fff"}
                  borderWidth={0}
                  height={4}
                />
              </View>
              <TouchableOpacity style={{ position: "absolute", top: 10, right: 0, paddingVertical: 5, paddingHorizontal: 3 }} onPress={() => this.props.navigation.navigate('EditProfile')}>
                <EvilIcons
                  name="pencil"
                  size={30}
                  color="#000"
                />
              </TouchableOpacity>
            </View>
          </LinearGradient>
        </View>

        <ScrollView style={{ height: screenHeight / 1 }}
          showsVerticalScrollIndicator={false}
        >
          <View style={{ marginBottom: 25 }}>
            <View style={styles.firstBodycontainer}>
              <View style={styles.performenceContainer}>
                <Text style={styles.performance}>Your profile perfomance</Text>
                <View style={styles.apperancesContainer}>
                  <View style={{}}>
                    <Text style={styles.apperancesNumber}>1236</Text>
                    <Text style={styles.apperances}>Search Apperances</Text>
                  </View>
                  <View>
                    <Text style={styles.recuiterActionNumber}>12</Text>
                    <Text style={styles.recuiterAction}>Recruiter Actions</Text>
                  </View>
                </View>
                {
                  this.state.storage == "employers" ?
                    <TouchableOpacity style={{ marginBottom: 0, flexDirection: "row" }}>
                      <Text style={styles.missingProfileText} >1 PROFILE DETAIL IS MISSING</Text>
                      <AntDesign
                        name="right"
                        size={13}
                        color="#384476"
                        style={{ marginTop: 4, marginLeft: 5 }}
                      />
                    </TouchableOpacity>
                    :
                    <TouchableOpacity style={{ marginBottom: 0, flexDirection: "row" }} onPress={() => this.props.navigation.navigate('MainFrom', { step: 1 })}>
                      <Text style={styles.missingProfileText}>PLEASE FILL UP ALL PROFILE DETAILS</Text>
                      <AntDesign
                        name="right"
                        size={13}
                        color="#384476"
                        style={{ marginTop: 4, marginLeft: 5 }}
                      />
                    </TouchableOpacity>
                }
              </View>
            </View>

            <View style={styles.secondContainer}>
              <View style={styles.newJobContainer}>
                <View>
                  <Text style={styles.newJob}>New Recommended Jobs</Text>
                  <Text style={styles.jobHeading}>Professional, precise</Text>
                  <Text style={styles.jobaddress}>Alex Software.ltd, USA</Text>
                </View>
                <View>
                  <View style={styles.jobNumberView}>
                    <Text style={styles.jobNumber}>42</Text>
                  </View>
                </View>
              </View>
              <View style={styles.jobrecuiterView}>
                <Text style={styles.Recuiter}>New Jobs from Recruiters</Text>
                <Text style={styles.RecuiterNumber}>07</Text>
              </View>
              <View style={styles.saveJobView}>
                <Text style={styles.saveJob}>Saved Jobs</Text>
                <Text style={styles.saveJobNumber}>12</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default EmployeeHomescreen;

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1
  },
  linearGradient: {
    paddingBottom: 10,
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 1,
    shadowRadius: 4,
  },
  firstBodycontainer: {
    width: "100%",
    borderColor: "#000",
    // shadowOpacity: 0.9,
    elevation: 1,
    shadowOffset: { height: 2, width: 2 },
    shadowOpacity: 1,
    shadowRadius: 4,
    backgroundColor: "#fff",
    shadowColor: '#000',
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 0,
      height: 2
    },

  },
  performenceContainer: {
    width: "85%",
    alignSelf: "center",
    marginTop: 10,
    marginBottom: 20,
  },
  performance: {
    color: "#4a4a4a",
    fontFamily: FontFamily.Poppins_Medium,
    fontSize: 14,
    //fontWeight: "600"
  },
  apperancesContainer: {
    flexDirection: "row",
    marginTop: 15,
    marginBottom: 25,
    width: "100%",
    justifyContent: "space-between",
  },
  apperancesNumber: {
    fontWeight: "600",
    fontSize: 30,
    color: "#000000",
    fontFamily: FontFamily.Poppins_Medium,
  },
  apperances: {
    //fontWeight:"600",
    fontSize: 14,
    color: "#000000",
    fontFamily: FontFamily.Poppins_Medium,
  },
  recuiterActionNumber: {
    fontWeight: "600",
    fontSize: 30,
    color: "#000000",
    fontFamily: FontFamily.Poppins_Medium,
  },
  recuiterAction: {
    //fontWeight:"bold",
    fontSize: 14,
    color: "#000000",
    fontFamily: FontFamily.Poppins_Medium,
  },
  missingProfileText: {
    color: "#384476",
    fontSize: 14,
    fontFamily: FontFamily.Poppins_Regular
  },
  secondContainer: {
    width: "100%",
    alignItems: "center",
    paddingTop: 30,
  },
  newJobContainer: {
    flexDirection: "row",
    width: "85%",
    justifyContent: "space-between",
  },
  newJob: {
    fontSize: 15,
    color: "#484848",
    marginBottom: 30,
  },
  jobHeading: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#374271",
    marginBottom: 7
  },
  jobaddress: {
    fontSize: 11,
    color: "#4a4a4a",
    marginBottom: 40
  },
  jobNumberView: {
    backgroundColor: "#262e50",
    height: 55,
    width: screenwidth / 4,
    borderRadius: 100,
    justifyContent: "center",
    marginTop: 20
  },
  jobNumber: {
    fontSize: 30,
    alignSelf: "center",
    color: "#fff",
  },
  jobrecuiterView: {
    width: "85%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  Recuiter: {
    fontSize: 15,
    color: "#484848"
  },
  RecuiterNumber: {
    fontSize: 22,
    color: "#262e50",
    marginRight: "8%"
  },
  saveJobView: {
    width: "85%",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 15
  },
  saveJob: {
    fontSize: 15,
    color: "#484848"
  },
  saveJobNumber: {
    fontSize: 22,
    color: "#262e50",
    marginRight: "8%"
  },
  profileImageView: {
    height: 85,
    width: 85,
    borderRadius: 80,
    alignSelf: "center",
    marginTop: 13,
    // shadowColor: '#000',
    // shadowRadius: 60,
    // shadowOpacity: 0.7,
    // elevation: 18,
    // shadowOffset: {
    //   width: 100,
    //   height: 4
    // }
  },
  profileImage: {
    height: "100%",
    width: "100%",
    borderRadius: 80,
    borderWidth: 2,
    borderColor: "#fff",
  },
  profileName: {
    alignSelf: "center",
    fontSize: 25,
    color: "#fff",
    //fontWeight: "700",
    marginTop: 5,
    fontFamily: FontFamily.Poppins_Medium
  },
  profileDesignation: {
    alignSelf: "center",
    color: "#fff",
    marginBottom: 20,
    marginTop: 0,
    fontFamily: FontFamily.Poppins_Semibold,
    fontSize: 15
  },
  profilePercentage: {
    alignSelf: "center",
    color: "#fff",
    fontFamily: FontFamily.Poppins_Medium,
    fontSize: FontSize.micro_size
  },
  percentageContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignSelf: "center",
    marginTop: 3
  },
  profleUpdate: {
    alignSelf: "center",
    fontWeight: "600",
    fontFamily: FontFamily.Poppins_Medium,
    fontSize: FontSize.micro_size,
    color: "#1e2844"
  }
})