import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, TouchableOpacity, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontFamily from '../../Utils/FontFamily';
import FontSize from '../../Utils/fonts';
import { ScrollView } from 'react-native-gesture-handler';
import Font from '../../Utils/fonts';
const screenHeight = Dimensions.get('window').height
const screenwidth = Dimensions.get('window').width
import {save_user} from '../../actions/userAction';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import Colors from '../../Utils/Colors';


class Setting extends Component {

  logout = async () =>{
    this.props.save_user()
    await AsyncStorage.removeItem('authtocken');
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="Settings"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />
        <ScrollView style={{ }} showsVerticalScrollIndicator={false}>
          <TouchableOpacity style={styles.repeatcontainer}>
            <View style={styles.secondcontainer}>
              <Text style={styles.text}>Communication &amp; Privacy</Text>
              <Text style={styles.text2}>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.repeatcontainer}>
            <View style={styles.secondcontainer}>
              <Text style={styles.text}>Account</Text>
              <Text style={styles.text2}>Change you primary Email, and mobile no password </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.repeatcontainer}>
            <View style={styles.secondcontainer}>
              <Text style={styles.text}>Job Preferences</Text>
              <Text style={styles.text2}>It is a long established fact that a reader will be distracted </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.repeatcontainer}>
            <View style={styles.secondcontainer}>
              <Text style={styles.text}>Block Companies</Text>
              <Text style={styles.text2}>It is a long established fact that a reader will be distracted</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.repeatcontainer} onPress={()=> this.props.navigation.navigate('Terms')}>
            <View style={styles.secondcontainer}>
              <Text style={styles.text}>Terms &amp; Conditions</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.repeatcontainer} onPress={()=> this.props.navigation.navigate('PrivacyPolicy')}>
            <View style={styles.secondcontainer}>
              <Text style={styles.text}>Privacy Policy</Text>
            </View>
          </TouchableOpacity>

        </ScrollView>
        <TouchableOpacity style={{
          shadowOpacity: 0.1,
          elevation: 1,
          borderTopWidth: 0,
          borderTopColor: "#999",
          flexDirection: "row",
          justifyContent: "center", alignItems: "center",
          backgroundColor:"#fff"
        }} onPress={()=> this.logout()}>
          <View style={[styles.secondcontainer, { justifyContent: "center", alignItems: "center" }]}>
            <Text style={styles.text}>LOGOUT</Text>
          </View>
        </TouchableOpacity>

      </View>
    );
  }
}

export default connect(null, {save_user})(Setting)

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  repeatcontainer: {
    backgroundColor: "#e9e9e9", marginBottom: 10, elevation: 1, justifyContent: "center"
  },
  secondcontainer: {
    backgroundColor: "#ffff", paddingHorizontal: 20, paddingVertical: 15
  },
  text: {
    marginBottom: 2, fontFamily: FontFamily.Poppins_Medium, color: "#000", fontSize:Font.medium_size
  },
  text2: {
    fontFamily: FontFamily.Poppins_Medium, color: "#707070", 
  },
})