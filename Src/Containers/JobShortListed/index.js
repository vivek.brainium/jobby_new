import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from '../../Components/Button/loginButton'
import FontFamily from '../../Utils/FontFamily';
import Colors from '../../Utils/Colors';
import JobSearch from '../../Components/JobSearch'

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;



class JobShortlisted extends Component {

  state = {
    //searchjob: '',
    CompanyName: "Berkshire Hathaway Inc.",
    Designation: "HR Executive",
    Location: "California",
    Experience: "4 - 6 years",
    Skills: 'MBA',
    Description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
  }

  render() {
    const { navigation } = this.props;
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => navigation.openDrawer()}
          headerTitle={"Job Shortlisted"}
          type={"textinput"}
          //onChangeText={(searchjob)=> this.setState({searchjob})}
          placeholder="Search Job"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />

        <JobSearch
          headerName={this.state.CompanyName}
          Designation={this.state.Designation}
          Location={this.state.Location}
          Experience={this.state.Experience}
          Skills={this.state.Skills}
          Description={this.state.Description}
          thirdButton="Contact Employer"
          thirdButtonColor="#202746"
          fourthButton="Revoke Application"
          fourthButtonColor="#b4b4b4"
        />


      </View>
    );
  }
}

export default JobShortlisted;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  },

})