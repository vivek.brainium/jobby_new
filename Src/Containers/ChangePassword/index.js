import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, ImageBackground, StatusBar, Image, TouchableOpacity, SafeAreaView } from 'react-native';
import Colors from "../../Utils/Colors";
import FontSize from "../../Utils/fonts";
import Ionicons from 'react-native-vector-icons/Ionicons';
import TextInputComponent from "../../Components/Login/TextInput"
import ButtonComponent from '../../Components/Button/loginButton'
import FloatingLabelInput from '../../Components/FlotingTextInput';
import HeaderComponent from '../../Components/Header';
import MyStatusBar from '../../Utils/MyStatusBar';


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

class ChangePassword extends Component {
  state = {
    passIcon: true,
    email: "",
    password: "",
    confirmPassword: "",
  }


  handleIconChange() {
    if (this.state.passIcon == true) {
      this.setState({ passIcon: false })
    } else {
      this.setState({ passIcon: true })
    }
  }

  render() {
    return (
      <View style={styles.container}>
       <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          backicon
          noNotification
          onClick2={() => this.props.navigation.goBack()}
          headerTitle="Change Password"
        />

        <View style={{
          width: screenwidth,
          height: screenHeight /2.2,
          justifyContent: "center",
        }}>
          <View style={styles.InputTypeContainer}>
            <FloatingLabelInput
              textInputHeight={35}
              textInputWidth={screenwidth / 1.25}
              label="Password"
              value={this.state.password}
              onChangeText={(val) => this.setState({ password: val })}
              placeholder="Enter Password"
              secureTextEntry={true}
            />
          </View>
          {/* <TextInputComponent
            titleName={"Password"}
            iconType={"image"}
            width={16}
            height={20}
            image={require("../../Images/employee_Registration/man.png")}
            placeholder={"Enter Password"}
            onChangeText={(val) => this.setState({ password: val })}
          /> */}
          <View style={styles.InputTypeContainer}>
            <FloatingLabelInput
              textInputHeight={35}
              textInputWidth={screenwidth / 1.25}
              label="Confirm Password"
              value={this.state.password}
              onChangeText={(val) => this.setState({ password: val })}
              placeholder="Enter Confirm New Password"
              secureTextEntry={true}
            />
          </View>
          {/* <TextInputComponent
            titleName={"Confirm Password"}
            iconType={"image"}
            width={16}
            height={20}
            image={require("../../Images/employee_Registration/man.png")}
            placeholder={"Enter Confirm New Password"}
            onChangeText={(val) => this.setState({ confirmPassword: val })}
          /> */}

          <View style={{ marginTop: 30, alignItems: "center" }}>
            <ButtonComponent
              buttonName="Submit"
            // onClick={() => this.props.navigation.navigate("CompanyDetails")}
            />
          </View>
        </View>
      </View>
    );
  }
}
export default ChangePassword;

const styles = StyleSheet.create({
  container: {
    width: screenwidth,
    height: screenHeight,
    // justifyContent: "center"
  },
  newUser_Text: {
    color: "#696969",
    fontSize: FontSize.small_size
  },
  SignUp_text: {
    color: "#1a2245",
    fontSize: FontSize.small_size
  },
  InputTypeContainer: {
    width: "80%", alignSelf: "center", marginBottom: 15
  }
})