import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, ImageBackground, StatusBar, Image, TouchableOpacity, ScrollView, KeyboardAvoidingView, TextInput, SafeAreaView } from 'react-native';
import Colors from "./../../Utils/Colors";
import FontSize from "./../../Utils/fonts";
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import TextInputComponent from "./../../Components/Login/TextInput"
import ButtonComponent from './../../Components/Button/loginButton'
import companyPic from '../../Images/Company_Profile/company.png'
import phonecallPic from '../../Images/Company_Profile/phonecall.png'
import designationPic from '../../Images/Company_Profile/designation.png'
import HeaderComponent from '../../Components/Header';
import FloatingLabelInput from '../../Components/FlotingTextInput';
import FontFamily from '../../Utils/FontFamily';


const screenHeight = Dimensions.get('window').height
const screenwidth = Dimensions.get('window').width

class EmployerCompanyDetails extends Component {


  state = {
    companyname: "",
    address: "",
    phoneNo: 0,
    email: "",
    designation: "",
  }



  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ flex: 0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="Company Details Entry"
          // type={"textinput"}
          // onChangeText={(postjob)=> this.setState({postjob})}
          // placeholder="Search"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />

        <ScrollView showsVerticalScrollIndicator={false} style={{ paddingVertical: 25,flex:0.8, backgroundColor: "#fff"}}>
          <View>
            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="First Name"
                value={this.state.companyname}
                onChangeText={(val) => this.setState({ companyname: val })}
                placeholder="Enter your company name"
              />
            </View>

            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="Country"
                value={this.state.country}
                onChangeText={(val) => this.setState({ country: val })}
                placeholder="Enter Country Name"
              />
            </View>
            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="City"
                value={this.state.city}
                onChangeText={(val) => this.setState({ city: val })}
                placeholder="Ex. Kolkata"
              />
            </View>


            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="First Name"
                value={this.state.first}
                onChangeText={(val) => this.setState({ first: val })}
                placeholder="First Name of the job poster"
              />
            </View>
            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="Last Name"
                value={this.state.last}
                onChangeText={(val) => this.setState({ last: val })}
                placeholder="Last Name of the job poster"
              />
            </View>

            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="Email ID"
                value={this.state.email}
                onChangeText={(val) => this.setState({ email: val })}
                placeholder="Enter Your Email Id"
              />
            </View>
            <View style={styles.repeatContainer}>
              <Text style={styles.title}>Job Describtion</Text>
              <TextInput
                underlineColorAndroid="transparent"
                placeholder="Company Details"
                placeholderTextColor="#999"
                autoCapitalize="none"
                onChangeText={(val) => this.setState({ designation: val })}
                multiline={true}
                value={this.state.designation}
                style={styles.mytextinput}
              />
            </View>

            <View style={{ alignSelf: "center", alignItems: "flex-start", width: "80%", marginBottom: 10 }}>
              <Text style={[styles.title, { marginBottom: 10, }]}>Upload Company Logo</Text>
              <ButtonComponent
                buttonName="Upload Logo"
                backgroundColor1="#1a2246"
                height={50}
                width="50%"
              // onClick={() => this.props.navigation.navigate("PostJob")}
              />
            </View>
            <View style={{ marginBottom: 50, alignItems: "center", marginTop: 10 }}>
              <ButtonComponent
                buttonName="Submit &amp; View Profile"
                onClick={() => this.props.navigation.navigate('EmployerProfile')}
                backgroundColor1="#000"
                height={50}
                width="85%"
              />
            </View>
          </View>

        </ScrollView>

      </View>
    );
  }
}

export default EmployerCompanyDetails;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  newUser_Text: {
    color: "#696969",
    fontSize: FontSize.small_size
  },
  SignUp_text: {
    color: "#1a2245",
    fontSize: FontSize.small_size
  },
  repeatContainer: {
    width: "90%", alignSelf: "center", marginBottom: 10
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, borderRadius: 10, height: 80, width: "90%", paddingHorizontal: 10, textAlignVertical: 'top'
  },
  title: {
    marginBottom: 10,
    fontSize: 14,
    fontFamily: FontFamily.Poppins_Medium
  }
})