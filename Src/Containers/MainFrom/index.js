import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, SafeAreaView } from 'react-native';
import ContactInfo from '../../Components/MainFom/ContactInfo';
import Eudation from '../../Components/MainFom/Eudation';
import Experience from '../../Components/MainFom/Experience';
import Language from '../../Components/MainFom/Language';
import PersonalInfo from '../../Components/MainFom/PersonalInfo';
import PreferredJob from '../../Components/MainFom/PreferredJob';
import Salary from '../../Components/MainFom/Salary';
import Skill from '../../Components/MainFom/Skill';
import HeaderComponent from '../../Components/Header';
import ButtonComponent from '../../Components/Button/loginButton';
import Colors from '../../Utils/Colors';


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

class MainFrom extends Component {


  state = {
    passIcon: true,
    email: "",
    password: "",
    confirmPassword: "",
    fName: "",
    lName: "",
    cName: "",
    poster: "",
    step: 1,
    good: ''
  }

  componentDidMount() {
    this.lisener()
  }

  lisener = () => {
    this.willFocusListener = this.props.navigation.addListener('focus', () => {
      console.log("Params", this.props.route.params);
      if (this.props.route.params != null) {
        const step = this.props.route.params;
        this.setState({
          step: step.step,
        })
      }
      if (this.props.route.params.singleButton != null) {
        const step = this.props.route.params.singleButton;
        console.log(this.props.route.params.singleButton, "singleButton");
        this.setState({
          good: step
        })
      }
      else {
        this.setState({
          good: ''
        })
      }
    });
  }

  componentWillUnmount() {
    this.willFocusListener();
  }


  nextStep = () => {
    const { step } = this.state
    this.setState({
      step: step + 1
    })
  }

  prevStep = () => {
    const { step } = this.state
    this.setState({
      step: step - 1
    })
  }

  handleChange = input => event => {
    this.setState({ [input]: event.target.value })
  }

  render() {
    const { step } = this.state;
    const { firstName, lastName, email, age, city, country } = this.state;
    const values = { firstName, lastName, email, age, city, country };

    switch (step) {
      case 1:
        return (
          <View style={{ flex: 1, backgroundColor: "#fff" }}>
            <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
            <HeaderComponent
              backicon
              noNotification
              onClick2={() => this.props.navigation.goBack()}
              headerTitle="Personal Information"
              notificationonpress={() => this.props.navigation.navigate("Notification")}
              headingLeft={15}
            />
            <PersonalInfo
              nextStep={this.nextStep}
              handleChange={this.handleChange}
              values={values}
            />
            {
              this.state.good == "singleButton" ?
                <View style={{ alignItems: "center", marginBottom: 20 }}>
                  <ButtonComponent
                    buttonName="Save"
                    backgroundColor1="#1a2246"
                    height={40}
                    width="50%"
                    onClick={this.nextStep}
                  />
                </View>
                :
                <View style={{ alignItems: "center", marginBottom: 20 }}>
                  <ButtonComponent
                    buttonName="Save and Continue"
                    backgroundColor1="#1a2246"
                    height={40}
                    width="50%"
                    onClick={this.nextStep}
                  />
                </View>
            }
          </View>
        )
      case 2:
        return (
          <View style={{ flex: 1, backgroundColor: "#fff" }}>
            <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
            <HeaderComponent
              backicon
              noNotification
              onClick2={() => (this.state.good == "singleButton") ? this.props.navigation.goBack() : this.prevStep()}
              headerTitle="Contact Information"
              notificationonpress={() => this.props.navigation.navigate("Notification")}
              headingLeft={15}
            />
            <ContactInfo
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              handleChange={this.handleChange}
              values={values}
            />

            {this.state.good != "singleButton" ?
              <View style={{ flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "space-between", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Back"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="45%"
                  onClick={this.prevStep}
                />
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="45%"
                  onClick={this.nextStep}
                />
              </View>
              : <View style={{ alignItems: "center", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="50%"
                // onClick={this.nextStep}
                />
              </View>}
          </View>
        )
      case 3:
        return (
          <View style={{ flex: 1, backgroundColor: "#fff" }}>
            <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
            <HeaderComponent
              backicon
              noNotification
              onClick2={() => (this.state.good == "singleButton") ? this.props.navigation.goBack() : this.prevStep()}
              headerTitle="Job Information"
              notificationonpress={() => this.props.navigation.navigate("Notification")}
              headingLeft={15}
            />
            <PreferredJob
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              values={values}
            />

            {this.state.good != "singleButton" ?

              <View style={{ flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "space-between", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Back"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="45%"
                  onClick={this.prevStep}
                />
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="45%"
                  onClick={this.nextStep}
                />
              </View>
              : <View style={{ alignItems: "center", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="50%"
                // onClick={this.nextStep}
                />
              </View>}
          </View>
        )
      case 4:
        return (
          <View style={{ flex: 1, backgroundColor: "#fff" }}>
            <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
            <HeaderComponent
              backicon
              noNotification
              onClick2={() => (this.state.good == "singleButton") ? this.props.navigation.goBack() : this.prevStep()}
              headerTitle="Experience"
              notificationonpress={() => this.props.navigation.navigate("Notification")}
              headingLeft={15}
            />
            <Experience
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              values={values}
            />
            {this.state.good != "singleButton" ?
              <View style={{ flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "space-between", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Back"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="45%"
                  onClick={this.prevStep}
                />
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="45%"
                  onClick={this.nextStep}
                />
              </View>
              : <View style={{ alignItems: "center", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="50%"
                // onClick={this.nextStep}
                />
              </View>}

          </View>
        )
      case 5:
        return (
          <View style={{ flex: 1, backgroundColor: "#fff" }}>
            <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
            <HeaderComponent
              backicon
              noNotification
              onClick2={() => (this.state.good == "singleButton") ? this.props.navigation.goBack() : this.prevStep()}
              headerTitle="Salary Details"
              notificationonpress={() => this.props.navigation.navigate("Notification")}
              headingLeft={15}
            />
            <Salary
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              values={values}
            />

            {this.state.good != "singleButton" ?
              <View style={{ flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "space-between", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Back"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="45%"
                  onClick={this.prevStep}
                />
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="45%"
                  onClick={this.nextStep}
                />
              </View>
              : <View style={{ alignItems: "center", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="50%"
                // onClick={this.nextStep}
                />
              </View>}

          </View>
        )
      case 6:
        return (
          <View style={{ flex: 1, backgroundColor: "#fff" }}>
            <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
            <HeaderComponent
              backicon
              noNotification
              onClick2={() => (this.state.good == "singleButton") ? this.props.navigation.goBack() : this.prevStep()}
              headerTitle="Education Information"
              notificationonpress={() => this.props.navigation.navigate("Notification")}
              headingLeft={15}
            />
            <Eudation
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              values={values}
            />

            {this.state.good != "singleButton" ?
              <View style={{ flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "space-between", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Back"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="45%"
                  onClick={this.prevStep}
                />
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="45%"
                  onClick={this.nextStep}
                />
              </View>

              : <View style={{ alignItems: "center", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="50%"
                // onClick={this.nextStep}
                />
              </View>}


          </View>
        )
      case 7:
        return (
          <View style={{ flex: 1, backgroundColor: "#fff" }}>
            <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
            <HeaderComponent
              backicon
              noNotification
              onClick2={() => (this.state.good == "singleButton") ? this.props.navigation.goBack() : this.prevStep()}
              headerTitle="Language"
              notificationonpress={() => this.props.navigation.navigate("Notification")}
              headingLeft={15}
            />
            <Language
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              values={values}
            />

            {this.state.good != "singleButton" ?

              <View style={{ flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "space-between", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Back"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="45%"
                  onClick={this.prevStep}
                />
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="45%"
                  onClick={this.nextStep}
                />
              </View>
              :
              <View style={{ alignItems: "center", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="50%"
                // onClick={this.nextStep}
                />
              </View>}
          </View>
        )
      case 8:
        return (
          <View style={{ flex: 1, backgroundColor: "#fff" }}>
            <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
            <HeaderComponent
              backicon
              noNotification
              onClick2={() => (this.state.good == "singleButton") ? this.props.navigation.goBack() : this.prevStep()}
              headerTitle="Skill Information"
              notificationonpress={() => this.props.navigation.navigate("Notification")}
              headingLeft={15}
            />
            <Skill
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              values={values}
            />

            {this.state.good != "singleButton" ?
              <View style={{ alignItems: "center", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Finish"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="50%"
                  onClick={() => this.props.navigation.navigate("EmployeeHomescreen")}
                />
              </View>
              :
              <View style={{ alignItems: "center", marginBottom: 20 }}>
                <ButtonComponent
                  buttonName="Save"
                  backgroundColor1="#1a2246"
                  height={40}
                  width="50%"
                />
              </View>}


          </View>
        )
    }
  }
}

export default MainFrom;


const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
    backgroundColor: "#fff"
  },
})