import React, { Component } from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Dimensions, FlatList, TextInput, Image, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import MessageList from '../../Components/MessageList'
import Colors from '../../Utils/Colors';


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const MessageData = [
  {
    id: 1,
    name: "Stuart Alexandra",
    jobDescription: "Hiring for React Native Developer",
    image: require('../../Images/EmployerProfile/image.jpg'),
    companyName: "Brainium Information Technologies Pvt. Ltd."
  },
  {
    id: 2,
    name: "Stuart Alexandra",
    jobDescription: "Hiring for React Native Developer",
    image: require('../../Images/EmployerProfile/image.jpg'),
    companyName: "Brainium Information Technologies Pvt. Ltd."
  },
  {
    id: 3,
    name: "Stuart Alexandra",
    jobDescription: "Hiring for React Native Developer",
    image: require('../../Images/EmployerProfile/image.jpg'),
    companyName: "Brainium Information Technologies Pvt. Ltd."
  },
  {
    id: 4,
    name: "Stuart Alexandra",
    jobDescription: "Hiring for React Native Developer",
    image: require('../../Images/EmployerProfile/image.jpg'),
    companyName: "Brainium Information Technologies Pvt. Ltd."
  },
  {
    id: 5,
    name: "Stuart Alexandra",
    jobDescription: "Hiring for React Native Developer",
    image: require('../../Images/EmployerProfile/image.jpg'),
    companyName: "Brainium Information Technologies Pvt. Ltd."
  },
  {
    id: 6,
    name: "Stuart Alexandra",
    jobDescription: "Hiring for React Native Developer",
    image: require('../../Images/EmployerProfile/image.jpg'),
    companyName: "Brainium Information Technologies Pvt. Ltd."
  },
];

class MessageScreen extends Component {

  state = {
  }

  render() {
    return (
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          noNotification
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="Message List"
        />
        <View style={{ height: screenHeight/ 1.1, paddingVertical: 10 }}>
          <FlatList
            data={MessageData}
            renderItem={({ item }) => (
              <MessageList
                id={item.id}
                name={item.name}
                jobDescription={item.jobDescription}
                image={item.image}
                companyName={item.companyName}
              />
            )}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}
          />

        </View>
      </View>
    );
  }
}

export default MessageScreen;

const styles = StyleSheet.create({
  MainContainer: {
    //width: screenwidth,
    //height: screenHeight,
    flex: 1,
    marginBottom: 40
  },


})