import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, ImageBackground, StatusBar, Image, TouchableOpacity, Animated, ScrollView, SafeAreaView } from 'react-native';
import Colors from "../../Utils/Colors";
import FontSize from "../../Utils/fonts";
import FontFamily from '../../Utils/FontFamily';
import HeaderComponent from "../../Components/Header"
import ProfileComponent from '../../Components/Profile';


const screenHeight = Math.round(Dimensions.get('window').height);
const screenwidth = Math.round(Dimensions.get('window').width);

const anim = new Animated.Value(0);


class Employee extends Component {


    state = {
        progressStatus: 10
    }

    componentDidMount() {
        this.onAnimate()
    }

    onAnimate = () => {
        anim.addListener(({ value }) => {
            this.setState({ progressStatus: parseInt(value, 10) })
        });
        Animated.timing(anim, {
            toValue: 100,
            duration: 5000,
        }).start();
    }


    render() {
        return (

            //Main Container
            <View style={styles.MainContainer}>
                <SafeAreaView style={{ flex: 0, backgroundColor: Colors.StatusBar }} />
                <HeaderComponent
                    drawericon
                    onClick={() => this.props.navigation.openDrawer()}
                    headerTitle="Employee"
                    placeholder="Search"
                    notificationonpress={() => this.props.navigation.navigate("Notification")}
                />

                <ScrollView style={{ flex: 1 }}>
                    <ProfileComponent
                        prodileprogressbar
                        InformationContainer
                        profileyear
                        profilelocation
                        profiledegree
                        profilemail
                        profilephone
                        ProfileSummery
                        block
                        blockPress={()=> this.props.navigation.navigate("ReportBlock")}
                        // shortlist
                    />

                </ScrollView>
            </View >
        );
    }
}

export default Employee;

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: '#f5f5f5',
    },
    MainImageSection: {
        flex: 0.25,
        flexDirection: 'row',
        marginTop: 10
    },
    ImageContainer: {
        flex: 0.35, alignItems: 'center',
    },
    Details: {
        flex: 0.65,
        alignItems: 'flex-start',
        paddingTop: 5
    },
    Name: {
        fontSize: FontSize.large_size,
        color: '#000',
        textAlign: 'left',
        fontFamily: FontFamily.Poppins_Semibold,
        fontWeight: 'bold'
    },
    Profetion: {
        fontSize: FontSize.small_size,
        color: '#333333',
        textAlign: 'left',
        fontFamily: FontFamily.Poppins_Medium,
        fontWeight: '600'
    },
    Company: {
        color: '#464646',
        textAlign: 'left',
        fontFamily: 'Poppins-Light',
        fontWeight: '500'
    },
    InformationContainer: {
        flex: 0.3,
        marginLeft: 25,
        marginTop: 25
    },
    SingleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    MbaContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15
    },
    ProfileSommeryContainer: {
        flex: 0.25,
        marginTop: 20,
        paddingTop: 10,
        backgroundColor: '#fff',
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 2
    },
    FirstContainer: {
        flexDirection: 'row',
        marginLeft: 25
    },
    ProfileSummery: {
        color: '#000',
        fontSize: FontSize.medium_size,
        fontWeight: '500'
    },
    ResumeButton: {
        borderRadius: 20,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 5, height: screenHeight / 18,
        width: screenwidth / 2.5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    DownloadResume: {
        fontWeight: '800',
        color: '#4b849e',
        fontFamily: FontFamily.Poppins_Medium
    },
    DescriptionContainer: {
        marginLeft: 25,
        marginTop: 15,
        paddingRight: 2
    },
    Description: {
        color: '#747474',
        textAlign: 'left',
        fontFamily: 'Poppins-Light'
    },
    ReadMore: {
        color: '#2f2f2f',
        fontSize: FontSize.small_size,
        textAlign: 'left',
        fontFamily: 'Poppins-Light'
    },
    LastContainer: {
        flex: 0.12,
        justifyContent: 'center',
    },
    Shortlist: {
        color: '#2f2f2f',
        fontSize: FontSize.small_size,
        textAlign: 'center'
    },
})