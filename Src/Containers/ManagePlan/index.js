import React, { Component } from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Dimensions, FlatList, TextInput, Image, ImageBackground, ScrollView, SafeAreaView } from 'react-native';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import HeaderComponent from '../../Components/Header';
import ButtonComponent from '../../Components/Button/loginButton';
import Colors from '../../Utils/Colors';


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

class ManagePlan extends Component {

    state = {
        postjob: '',
        text: "",
        stap: 1
    }

    render() {
        return (
            //Main Container
            <View style={styles.MainContainer}>
                <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
                <HeaderComponent
                    drawericon
                    onClick={() => this.props.navigation.openDrawer()}
                    headerTitle="Manage Plan"
                    noNotification
                />
                {this.state.stap == 1 ?
                    <View style={{ justifyContent: "center", height: screenHeight / 1 }}>
                        <View style={{ padding: 20 }}>
                            <View style={{ alignItems: "center" }}>
                                <Text style={{ fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Regular }}>This is your Recent Plan</Text>
                            </View>
                            <View style={{ alignItems: "center" }}>
                                <View style={{ backgroundColor: "#4b849e", padding: 20, borderRadius: 70, width: 120, height: 120, alignItems: "center", justifyContent: "center", marginBottom: 20 }}>
                                    <Text style={{ fontSize: FontSize.large_size, fontFamily: FontFamily.Poppins_Regular, color: "#fff" }}>$ 5/mo</Text>
                                </View>
                                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>You can 5 job post in this package</Text>
                                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>This package valid uoto 26/04/2020</Text>
                            </View>
                        </View>
                        <View style={{ alignItems: "center" }}>
                            <ButtonComponent
                                buttonName="Update Plan"
                                backgroundColor1="#000"
                                height={50}
                                width="41%"
                                onClick={() => this.setState({ stap: 2 })}
                            />
                        </View>
                    </View>
                    : null
                }
                {
                    this.state.stap == 2 ?
                        <ScrollView contentContainerStyle={{alignItems: "center"}}>
                            <View style={{ padding: 20 }}>
                                <TouchableOpacity style={{ alignItems: "center", marginBottom: 20 }} activeOpacity={0.7}>
                                    <Text style={{ fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Medium, color: "#000" }}>Small</Text>
                                    <View style={{ backgroundColor: "#4b849e", padding: 20, borderRadius: 70, width: 130, height: 130, alignItems: "center", justifyContent: "center", }}>
                                        <Text style={{ fontSize: FontSize.large_size, fontFamily: FontFamily.Poppins_Regular, color: "#fff" }}>$ 5/mo</Text>
                                    </View>
                                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>This package valid for one month</Text>
                                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>In this package you can add 5 job post</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ alignItems: "center", marginBottom: 20 }} activeOpacity={0.7}>
                                    <Text style={{ fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Medium, color: "#000" }}>Corporate</Text>
                                    <View style={{ backgroundColor: "#4b849e", padding: 20, borderRadius: 70, width: 130, height: 130, alignItems: "center", justifyContent: "center" }}>
                                        <Text style={{ fontSize: FontSize.large_size, fontFamily: FontFamily.Poppins_Regular, color: "#fff" }}>$ 15/mo</Text>
                                    </View>
                                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>This package valid for three month</Text>
                                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>In this package you can add 15 job post</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ alignItems: "center", marginBottom: 20 }} activeOpacity={0.7}>
                                    <Text style={{ fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Medium, color: "#000" }}>Large</Text>
                                    <View style={{ backgroundColor: "#4b849e", padding: 20, borderRadius: 70, width: 130, height: 130, alignItems: "center", justifyContent: "center" }}>
                                        <Text style={{ fontSize: FontSize.large_size, fontFamily: FontFamily.Poppins_Regular, color: "#fff" }}>$ 45/mo</Text>
                                    </View>
                                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>This package valid for six month</Text>
                                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>In this package you can add 50 job post</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                        : null
                }
            </View>
        );
    }
}

export default ManagePlan;

const styles = StyleSheet.create({
    MainContainer: {
        // width: screenwidth,
        // height: screenHeight,
        flex: 1,
    },
    englishFindTextView: {
        paddingHorizontal: 27,
        justifyContent: "space-between",
        flexDirection: "row",
        marginTop: 25,
        marginBottom: 3
    },
    findText: {
        fontSize: 28,
        fontWeight: "bold",
        color: "#1a2246",
        fontFamily: FontFamily.Poppins_Medium
        //         font:poppins,Bold
        // font-size:80px
        // color:#1a2246
    },
    englishText: {

        fontSize: 13,
        color: "#202541",
        fontFamily: FontFamily.Poppins_Regular
    },
    anytime: {
        paddingHorizontal: 27,
        marginBottom: 7,
        fontSize: FontSize.micro_size,
        color: "#232445",
        fontFamily: FontFamily.Poppins_Regular
    },
    firstInputContainer: {
        flexDirection: "row",
        alignItems: "center",
        elevation: 2,
        backgroundColor: "#fff",
        marginHorizontal: 20,
        height: 50,
        borderRadius: 5,
        marginTop: 10,

    },
    firstInput: {
        paddingHorizontal: 10,
        fontSize: FontSize.small_size,
        fontWeight: "600",
        //borderWidth:1,
        color: "#565656",
        fontFamily: FontFamily.Poppins_Regular
    },
    secondInputContainer: {
        flexDirection: "row",
        alignItems: "center",
        elevation: 2,
        backgroundColor: "#fff",
        marginHorizontal: 20,
        height: 50,
        borderRadius: 5,
        marginTop: 7
    },
    secondInput: {
        paddingHorizontal: 10,
        fontSize: FontSize.small_size,
        fontWeight: "600",
        color: "#565656",
        fontFamily: FontFamily.Poppins_Regular
    },
    findJobContainer: {
        width: "40%",
        height: 55,
        alignSelf: "center",
        backgroundColor: "#fff",
        borderRadius: 40,
        marginTop: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    findJobText: {
        fontSize: 17,
        color: "#78a7f5",
        fontFamily: FontFamily.Poppins_Regular
    },
    createAccountText: {
        fontFamily: FontFamily.Poppins_Medium,
        fontSize: FontSize.small_size,
        fontWeight: "600",
        marginTop: 20,
        color: "#1a2246"
    },
    alreadyAccountText: {
        fontSize: 10,
        fontFamily: FontFamily.Poppins_Medium,
        color: "#ffffff"
    },
    signInText: {
        fontFamily: FontFamily.Poppins_Medium,
        fontSize: 14,
        fontWeight: "700",
        color: "#1f2846"
    },
    locationPic: {
        width: 23,
        height: 23
    },
    browseLocationContainer: {
        width: "85%",
        alignSelf: "center"
    },
    browseView: {
        flexDirection: "row",
        marginTop: "24%",
    },
    browse: {
        fontSize: 18,
        fontFamily: FontFamily.Poppins_Medium,
        color: "#494949",
        marginBottom: 8
    },
    india: {
        fontSize: FontSize.large_size,
        fontFamily: FontFamily.Poppins_Regular,
        fontWeight: "bold",
        color: "#494949"
    },
    Locaton: {
        fontSize: 15,
        fontFamily: FontFamily.Poppins_Medium,
        color: "#344075",
        fontWeight: "bold",
        marginBottom: "10%"
    },
    ITIndustryView: {
        width: "85%",
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 10
    },
    Industry: {
        fontSize: 16,
        fontFamily: FontFamily.Poppins_Medium,
        color: "#494949",
    },
    IndustryNumber: {
        fontSize: 19,
        fontFamily: FontFamily.Poppins_Medium,
        color: "#344075",
        width: 40
    },
    SalesView: {
        width: "85%",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    Sales: {
        fontSize: 16,
        fontFamily: FontFamily.Poppins_Medium,
        color: "#494949",
    },
    SalesNumber: {
        fontSize: 19,
        fontFamily: FontFamily.Poppins_Medium,
        color: "#344075",
        width: 40
    },
    MechanicalsView: {
        width: "85%",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    Mechanicals: {
        fontSize: 16,
        fontFamily: FontFamily.Poppins_Medium,
        color: "#494949",
    },
    MechanicalsNumber: {
        fontSize: 19,
        fontFamily: FontFamily.Poppins_Medium,
        color: "#344075",
        width: 40
    },

})