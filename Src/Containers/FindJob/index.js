import React, { Component } from 'react';
import {TouchableOpacity, View, Text,BackHandler, StyleSheet, Dimensions, FlatList, TextInput, Image, ImageBackground,ScrollView,SafeAreaView,StatusBar } from 'react-native';
//import HeaderComponent from "../../Components/Header"
//import ContactList from '../../Components/ContactList';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Colors from '../../Utils/Colors';
import MyStatusBar from '../../Utils/MyStatusBar';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

class FindJob extends Component {

    state = {
        postjob: '',
        text: ""
    }


     

    render() {
        return (
            //Main Container
            <View style={styles.MainContainer}>
                <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
                <ScrollView  scrollEnabled={false}>
                <ImageBackground source={require('../../Images/Find_Job/local.properties.png')} style={{ width: "100%", height: "100%" }}>
                    <View style={{marginBottom:20, }}>
                        <View style={styles.englishFindTextView}>
                            <Text style={styles.findText}>Find Jobs</Text>
                            <View style={{flexDirection:"row",marginTop:10}}>
                            <Text style={styles.englishText}>English </Text>
                            <View style={{marginTop:.50}}>
                            <FontAwesome
                                    name="angle-down"
                                    size={18}
                                    color="#192646"
                                />
                                </View>
                            </View>
                        </View>
                        <Text style={styles.anytime}>Anytime, Anywhere</Text>
                        
                        <View style={styles.firstInputContainer}>
                            <View style={{ flex: .95 }}>
                                <TextInput
                                    style={styles.firstInput}
                                    //value={props.InputValue}
                                    placeholder="Search by job title, skills or company"
                                    // placeholderTextColor={"grey"}
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    returnKeyType='done'
                                    enablesReturnKeyAutomatically={true}
                                    selectTextOnFocus={true}
                                    spellCheck={false}
                                />
                            </View>
                            <TouchableOpacity
                                style={{ alignItems: "center", }}
                                activeOpacity={0.7}
                            >
                                <Ionicons
                                    name="ios-search"
                                    size={22}
                                    color="gray"
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.secondInputContainer}>
                            <View style={{ flex: 0.96, }}>
                                <TextInput
                                    style={styles.secondInput}
                                    //value={props.InputValue}
                                    placeholder="All countries"
                                    // placeholderTextColor={"grey"}
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    returnKeyType='done'
                                    enablesReturnKeyAutomatically={true}
                                    selectTextOnFocus={true}
                                    spellCheck={false}
                                />
                            </View>
                            <TouchableOpacity
                                style={{ alignItems: "center" }}
                                activeOpacity={0.7}>
                                <Image source={require('../../Images/Find_Job/pin.png')} resizeMode="contain" style={styles.locationPic} />
                            </TouchableOpacity>

                        </View>

                        <TouchableOpacity style={styles.findJobContainer} onPress={()=> this.props.navigation.navigate('JobListing2')}>
                            <Text style={styles.findJobText}>Find Jobs</Text>
                        </TouchableOpacity>
                        
                        <View style={{ alignItems: "center", top: -10}}>
                            <TouchableOpacity onPress={()=> this.props.navigation.navigate('Landing', {"registration": "registration"})}>
                                <Text style={styles.createAccountText}>Create an account</Text>
                            </TouchableOpacity>
                            <View style={{ flexDirection: "row" }}>
                                <Text style={styles.alreadyAccountText}>Already have an account ? </Text>
                                <TouchableOpacity onPress={()=> this.props.navigation.navigate('Landing', {"signin":"signin"})}>
                                    <Text style={styles.signInText}>Sign in</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View >
                        <View style={styles.browseLocationContainer}>
                            <View style={styles.browseView}>
                                <Text style={styles.browse}>Browse Jobs in</Text>
                                <Text style={styles.india}> India</Text>
                            </View>
                            <View style={styles.kolkataView}>
                                <Text style={styles.browse}>Browse Jobs in</Text>
                                <Text style={styles.india}> Kolkata</Text>
                            </View>
                            <Text style={styles.Locaton}>Change Locaton</Text>
                        </View>


                        <View style={{ justifyContent: "space-between", alignItems: "center" }}>
                            <View style={styles.ITIndustryView}>
                                <Text style={styles.Industry}>IT &amp; Design Industry </Text>
                                <Text style={styles.IndustryNumber}>07</Text>
                            </View>
                            <View style={styles.SalesView}>
                                <Text style={styles.Sales}>Sales &amp; Marketing</Text>
                                <Text style={styles.SalesNumber}>12</Text>
                            </View>
                            <View style={styles.MechanicalsView}>
                                <Text style={styles.Mechanicals}>Mechanicals &amp; Electricals</Text>
                                <Text style={styles.MechanicalsNumber}>23</Text>
                            </View>
                        </View>
                        </View>
                    </View>
                    
                </ImageBackground>
                </ScrollView>
            </View>

        );
    }
}

export default FindJob;

const styles = StyleSheet.create({
    MainContainer: {
        flex:1,
        // width: screenwidth,
        // height: screenHeight,
        backgroundColor:"#fff"
    },
    englishFindTextView: {
        paddingHorizontal: 27,
        justifyContent: "space-between",
        flexDirection: "row",
        marginTop: 25,
        marginBottom: 3
    },
    findText: { 
        fontSize: 28, 
        fontWeight: "bold",
        color: "#1a2246",
        fontFamily: FontFamily.Poppins_Medium
//         font:poppins,Bold
// font-size:80px
// color:#1a2246
    },
    englishText: { 
        
        fontSize: 13,
        color:"#202541",
        fontFamily: FontFamily.Poppins_Regular
    },
    anytime: { 
        paddingHorizontal: 27, 
        marginBottom: 7, 
        fontSize: FontSize.micro_size,
        color: "#232445",
        fontFamily: FontFamily.Poppins_Regular
    },
    firstInputContainer: { 
        flexDirection: "row", 
        alignItems: "center", 
        elevation: 2, 
        backgroundColor: "#fff", 
        marginHorizontal: 20, 
        height: 50, 
        borderRadius: 5, 
        marginTop: 10 ,
        paddingTop: 7
    },
    firstInput: { 
        paddingHorizontal: 10, 
        fontSize: FontSize.small_size, 
        fontWeight: "600" ,
        color:"#565656",
        fontFamily: FontFamily.Poppins_Regular,
        
    },
    secondInputContainer: {
        flexDirection: "row",
        alignItems: "center",
        elevation: 2,
        backgroundColor: "#fff",
        marginHorizontal: 20,
        height: 50,
        borderRadius: 5,
        marginTop: 7
    },
    secondInput: { 
        paddingHorizontal: 10, 
        fontSize: FontSize.small_size, 
        fontWeight: "600" ,
        color:"#565656",
        fontFamily: FontFamily.Poppins_Regular
    },
    findJobContainer: { 
        width: "38%", 
        height: 50, 
        alignSelf: "center", 
        backgroundColor: "#fff", 
        borderRadius: 40, 
        marginTop: 24, 
        alignItems: "center", 
        justifyContent: "center" 
    },
    findJobText: { 
        fontSize: 17, 
        color: "#78a7f5",
        fontFamily: FontFamily.Poppins_Regular 
    },
    createAccountText: {
        fontFamily: FontFamily.Poppins_Medium,
        fontSize: FontSize.small_size,
        fontWeight: "600",
        marginTop: 25,
        color:"#1a2246"
    },
    alreadyAccountText: {
        fontSize: 14,
        fontFamily: FontFamily.Poppins_Medium,
        color:"#ffffff"
    },
    signInText: {
        fontFamily: FontFamily.Poppins_Medium,
        fontSize: 14,
        fontWeight: "700",
        color:"#1f2846"
    },
    locationPic: {
        width: 23,
        height: 23
    },
    browseLocationContainer: {
        width: "85%",
        alignSelf: "center",
        justifyContent:"flex-end",
        
    },
    browseView: {
        flexDirection:"row",
        marginTop:"25%",
    },
    kolkataView:{
        flexDirection:"row",
        marginBottom:8
    },
    browse:{
        fontSize: 18,
        fontFamily: FontFamily.Poppins_Medium,
        color:"#494949",
        marginBottom:0
    },
    india: {
        fontSize: FontSize.large_size,
        fontFamily: FontFamily.Poppins_Regular,
        fontWeight:"bold",
        color:"#494949"
    },
    Locaton: {
        fontSize: 14,
        fontFamily: FontFamily.Poppins_Medium,
        color:"#344075",
        fontWeight: "bold",
        marginBottom: "2%"
    },
    ITIndustryView: {
        width: "85%",
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 10
    },
    Industry: {
        fontSize: 16,
        fontFamily: FontFamily.Poppins_Medium,
        color:"#494949",
    },
    IndustryNumber: {
      fontSize: 19,
      fontFamily: FontFamily.Poppins_Medium,
      color:"#344075",
      width:40
    },
    SalesView: {
        width: "85%",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    Sales: {
        fontSize: 16,
        fontFamily: FontFamily.Poppins_Medium,
        color:"#494949",
    },
    SalesNumber: {
        fontSize: 19,
        fontFamily: FontFamily.Poppins_Medium,
        color:"#344075",
        width:40
    },
    MechanicalsView: {
        width: "85%",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    Mechanicals: {
        fontSize: 16,
        fontFamily: FontFamily.Poppins_Medium,
        color:"#494949",
    },
    MechanicalsNumber: {
      fontSize: 19,
      fontFamily: FontFamily.Poppins_Medium,
      color:"#344075",
      width:40
    },

})