import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, ScrollView, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ApplicationComponent from '../../Components/Application';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontFamily from '../../Utils/FontFamily';
import MyStatusBar from '../../Utils/MyStatusBar';
import Colors from '../../Utils/Colors';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;


const DATA = [
  {
    id: 1,
    title: "Polard Neo",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 2,
    title: "Polard Neo",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 3,
    title: "Polard Neo",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019"
  },
  {
    id: 4,
    title: "Polard Neo",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 5,
    title: "Polard Neo",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
];


class Application extends Component {
  state = {
    postjob: ''
  }

  hideAll = () => {
    DATA.map(d => {
      if (this.state[d.id]) {
        this.setState({ [d.id]: false })
      }
    })
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="Application"
          type={"textinput"}
          onChangeText={(postjob) => this.setState({ postjob })}
          placeholder="Search"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />
        <ScrollView>
          <View style={styles.maincontainer}>
            <View style={styles.secondcontainer}>
              <View style={styles.view1}>
                <View style={styles.InnerView}>
                  <Text style={styles.title}>HR Manager</Text>
                </View>
                <View style={{ marginLeft: 2 }}>
                  <View style={styles.repeatcontainer}>
                    <View style={styles.postimage}>
                      <SimpleLineIcons
                        style={{ marginBottom: 10 }}
                        name="briefcase"
                        color="gray"
                        size={18}
                      />
                    </View>
                    <Text style={styles.text}>4-6 Years</Text>
                  </View>
                  <View style={styles.repeatcontainer}>
                    <View style={styles.postimage}>
                      <EvilIcons
                        style={{}}
                        name="location"
                        color="gray"
                        size={20}
                      />
                    </View>
                    <Text style={styles.text}>California</Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <FlatList
            scrollEnabled={false}
            data={DATA}
            renderItem={({ item }) => (
              <ApplicationComponent
                id={item.id}
                title={item.title}
                years={item.years}
                locationname={item.locationname}
                designatonsection
                desingnation={item.desingnation}
                date
                posteddate={item.posteddate}
                verticalthreedot
                delete
                show={this.state[item.id]}
                gotodetails={() => this.setState({ [item.id]: !this.state[item.id] })}
                maincontainerPress={() => { this.hideAll; this.props.navigation.navigate('AppliedProfile') }}
                details={() => { this.props.navigation.navigate('ShortList'); this.setState({ [item.id]: false }) }}
                block={() => {  this.setState({ [item.id]: false }) }}
              />
            )}
            keyExtractor={item => item.id}
          />
        </ScrollView>
      </View>
    );
  }
}

export default Application;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  },
  maincontainer: {
    backgroundColor: "#999", marginBottom: 15, elevation: 3
  },
  secondcontainer: {
    backgroundColor: "#e9e9e9", padding: 20
  },
  repeatcontainer: {
    flexDirection: "row"
  },
  postimage: {
    alignItems: "flex-start", marginRight: 10
  },
  text: {
    marginBottom: 8, fontFamily: FontFamily.Poppins_Medium, color: "#494949"
  },
  title: {
    marginBottom: 10, fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium, fontWeight: "600"
  },
})