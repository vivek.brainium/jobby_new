import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, TouchableOpacity, Image, ScrollView, Alert, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
import Colors from '../../Utils/Colors';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const DATA = [
  {
    id: 1,
    title: "Hr Executive",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
  {
    id: 2,
    title: "Sr Software Developer",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
  {
    id: 3,
    title: "Dotnet Developer",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
  {
    id: 4,
    title: "Dotnet Developer",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
  {
    id: 5,
    title: "Sr Software Developer",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today"
  },
];

class JobListing2 extends Component {

  state = {
    postjob: '',
    show: false
  }

  hideAll = () => {
    DATA.map(d => {
      if (this.state[d.id]) {
        this.setState({ [d.id]: false })
      }
    })
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          backicon
          dot
          noNotification
          onClick2={() => this.props.navigation.goBack()}
          headerTitle="Job Listing"
          headingLeft={8}
        />
        <View style={{ flex: 1, position: "relative" }}>
          <FlatList
            data={DATA}
            style={{showsHorizontalScrollIndicator:false}}
            renderItem={({ item }) => (
              <View style={styles.maincontainer}>
                <View style={styles.secondcontainer}>
                  <TouchableOpacity activeOpacity={1} style={styles.view1}
                    onPress={()=> Alert.alert(
                      'Alert',
                      'Please Login to View Details',
                      [
                        {
                          text: 'Cancel',
                          onPress: () => console.log('Cancel Pressed'),
                          style: 'cancel',
                        },
                        {text: 'OK', onPress: () => this.props.navigation.navigate('Landing', {"signin":"signin"})},
                      ],
                      {cancelable: false},
                    )}
                  >
                    <Text style={styles.title}>{item.title}</Text>
                    <View style={{ flex: 1, }}>
                      <View style={styles.repeatcontainer}>
                        <View style={styles.yearimage}>
                          <SimpleLineIcons
                            style={{ marginBottom: 10, }}
                            name="briefcase"
                            color="gray"
                            size={16}
                          />
                        </View>
                        <Text style={styles.text}>{item.years}</Text>
                      </View>
                      <View style={styles.repeatcontainer}>
                        <View style={styles.yearimage}>
                          <EvilIcons
                            style={{ marginBottom: 10, }}
                            name="location"
                            color="gray"
                            size={20}
                          />
                        </View>
                        <Text style={styles.text}>{item.locationname}</Text>
                      </View>
                      <View style={styles.repeatcontainer}>
                        <View style={styles.postimage}>
                          <Image
                            style={{ width: 11, height: 16, marginHorizontal: 4.5 }}
                            source={require('../../Images/job_Details/pen.png')}
                          />
                        </View>
                        <Text style={styles.text}>{item.desingnation}</Text>
                      </View>
                      <View style={{ paddingLeft: "10%" }}>
                        <Text style={styles.text2}>{item.posteddate}</Text>
                      </View>
                    </View>
                  </TouchableOpacity>

                  <View style={styles.view2}>
                    <TouchableOpacity>
                      <Entypo
                        style={{ marginBottom: 15 }}
                        name="dots-three-vertical"
                        color="gray"
                        size={20}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity>
                      <AntDesign
                        style={{ marginBottom: 10 }}
                        name="delete"
                        color="#999"
                        size={20}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            )}
            keyExtractor={item => item.id}
          />

        </View>
      </View>
    );
  }
}

export default JobListing2;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  },
  maincontainer: {
    backgroundColor: "#e9e9e9", marginBottom: 15, elevation: 3
  },
  secondcontainer: {
    backgroundColor: "#ffffff", padding: 15, flexDirection: "row", flex: 1
  },
  view1: {
    flex: 0.9,
  },
  view2: {
    flex: 0.1, alignItems: "center", justifyContent: "flex-start", marginTop: 8
  },
  title: {
    marginBottom: 10, paddingHorizontal: 8, fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Medium, fontWeight: "600"
  },
  repeatcontainer: {
    flexDirection: "row", alignItems: "flex-start", flex: 1,
  },
  yearimage: {
    flex: 0.1, alignItems: "center", marginRight: 10
  },
  postimage: {
    flex: 0.12, alignItems: "center", marginRight: 12
  },
  text: {
    marginBottom: 8, fontFamily: FontFamily.Poppins_Medium, color: "#494949"
  },
  text2: {
    fontFamily: FontFamily.Poppins_Regular, color: "#707070",
  },
})