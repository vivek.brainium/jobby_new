import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, TouchableOpacity, Image, ScrollView, Alert, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
import MyStatusBar from '../../Utils/MyStatusBar';
import Colors from '../../Utils/Colors';



const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const DATA = [
  {
    id: 1,
    title: "Hr Executive",
    companyName: "Berkshire Hathaway Inc",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today",
    status: "Pending"
  },
  {
    id: 2,
    title: "Sr Software Developer",
    companyName: "Berkshire Hathaway Inc",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today",
    status: "Shortlisted"
  },
  {
    id: 3,
    title: "Dotnet Developer",
    companyName: "Berkshire Hathaway Inc",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today",
    status: "Shortlisted"
  },
  {
    id: 4,
    title: "Dotnet Developer",
    companyName: "Berkshire Hathaway Inc",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today",
    status: "Pending"
  },
  {
    id: 5,
    title: "Sr Software Developer",
    companyName: "Berkshire Hathaway Inc",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "Today",
    status: "Pending"
  },
];

class AppliedJob extends Component {

  state = {
    postjob: '',
    show: false
  }

  hideAll = () => {
    DATA.map(d => {
      if (this.state[d.id]) {
        this.setState({ [d.id]: false })
      }
    })
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          noNotification
          onClick={() =>this.props.navigation.openDrawer()}
          headerTitle="Applied Jobs"
          type={"textinput"}
          placeholder="Search Job"
        />
        <View style={{ flex: 1, position: "relative" }}>
          <FlatList
            data={DATA}
            renderItem={({ item }) => (
              <View style={styles.maincontainer}>
                <View style={styles.secondcontainer}>
                  <TouchableOpacity activeOpacity={1} style={styles.view1} onPress={()=> this.props.navigation.navigate('JobApplied')}>
                    <Text style={styles.title}>{item.title}</Text>
                    <Text style={styles.companyName}> {item.companyName}</Text>
                    <View style={{ flex: 1, }}>
                      <View style={styles.repeatcontainer}>
                        <View style={styles.yearimage}>
                          <SimpleLineIcons
                            style={{ marginBottom: 10, }}
                            name="briefcase"
                            color="gray"
                            size={16}
                          />
                        </View>
                        <Text style={styles.text}>{item.years}</Text>
                      </View>
                      <View style={styles.repeatcontainer}>
                        <View style={styles.yearimage}>
                          <EvilIcons
                            style={{ marginBottom: 10, }}
                            name="location"
                            color="gray"
                            size={20}
                          />
                        </View>
                        <Text style={styles.text}>{item.locationname}</Text>
                      </View>
                      <View style={styles.repeatcontainer}>
                        <View style={styles.postimage}>
                          <Image
                            style={{ width: 11, height: 16, marginHorizontal: 4.5 }}
                            source={require('../../Images/job_Details/pen.png')}
                          />
                        </View>
                        <Text style={styles.text}>{item.desingnation}</Text>
                      </View>
                      <View style={{ paddingLeft: "10%" }}>
                        <Text style={styles.text2}>Posted {item.posteddate}</Text>
                      </View>
                    </View>
                  </TouchableOpacity>

                  <View style={styles.view2}>
                    <TouchableOpacity style={{alignSelf: "flex-end", paddingHorizontal:3}} onPress={()=> this.props.navigation.navigate('JobApplied')}>
                      <Entypo
                        style={{ marginBottom: 15 }}
                        name="dots-three-vertical"
                        color="gray"
                        size={20}
                      />
                    </TouchableOpacity>
                    
                    <View style={{ paddingLeft: "10%" }}>
                        <Text style={[styles.status,{color:item.status=="Pending"?"#ff772f":"#009331"}]}> {item.status}</Text>
                      </View>
                  </View>
                </View>
              </View>
            )}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }
}

export default AppliedJob;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  },
  maincontainer: {
    backgroundColor: "#e9e9e9",
    marginBottom: 15,
    elevation: 3
  },
  secondcontainer: {
    backgroundColor: "#ffffff",
    padding: 15,
    flexDirection: "row",
    flex: 1
  },
  view1: {
    flex: 0.77,
  },
  view2: {
    flex: 0.23,
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: 8,
    // borderWidth: 1,
  },
  title: {
    paddingHorizontal: 0,
    fontSize: FontSize.medium_size,
    fontFamily: FontFamily.Poppins_Medium,
    fontWeight: "600"
  },
  repeatcontainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    flex: 1,
  },
  yearimage: {
    flex: 0.1,
    alignItems: "center",
    marginRight: 10
  },
  postimage: {
    flex: 0.15,
    alignItems: "center",
    marginRight: 12
  },
  text: {
    marginBottom: 0, 
    fontFamily: FontFamily.Poppins_Medium, 
    color: "#494949"
  },
  text2: {
    fontFamily: FontFamily.Poppins_Regular,
    color: "#707070",
    paddingLeft:5
  },
  companyName: {
    fontFamily: FontFamily.Poppins_Regular, 
    color: "#707070",
    marginBottom: 10,
    //paddingHorizontal: 8,
  },
  status: {
    fontFamily: FontFamily.Poppins_Regular, 
    color: "#707070",
    
  }
})
