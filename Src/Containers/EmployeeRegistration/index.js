import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, ImageBackground, StatusBar, Image, TouchableOpacity, ScrollView, Platform, SafeAreaView, KeyboardAvoidingView,  } from 'react-native';
import Colors from "../../Utils/Colors";
import FontSize from "../../Utils/fonts";
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import TextInputComponent from "../../Components/Login/TextInput"
import ButtonComponent from '../../Components/Button/loginButton'
// import { RadioButton } from 'react-native-paper';
import FontFamily from '../../Utils/FontFamily';
import FloatingLabelInput from '../../Components/FlotingTextInput';
import Apis from '../../Network/apicall';
import RNPicker from "rn-modal-picker";
import Toast from 'react-native-simple-toast';
import {save_user} from '../../actions/userAction';
import {connect} from 'react-redux';
import { black, gray } from 'ansi-colors';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

var radio_props = [
  {label: 'Fresher', value: 'Fresher'},
  {label: 'Experience', value: 'Experience' },
];

class EmployeeRegistration extends Component {

  state = {
    passIcon: true,
    email: "",
    password: "",
    confirmPassword: "",
    poster: "",
    stap: 1,
    jobrole: "",
    experience: "",
    selectedText: "",
    selectedId: "",
    currentTitleText: "",
    currentTitleId: "",
    dataSource: [],
    first: "",
    last: "",
    value: 'Fresher',
  }


  componentDidMount() {
    this.JobRole()
  }

  _selectedValue(index, item) {
    this.setState({
      selectedText: item.name,
      selectedId: item._id
    });
  }

  currentJobTitle(index, item) {
    this.setState({
      currentTitleText: item.name,
      currentTitleId: item._id
    });
  }

  handleIconChange() {
    if (this.state.passIcon == true) {
      this.setState({ passIcon: false })
    } else {
      this.setState({ passIcon: true })
    }
  }

  renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios'
    return (<View style={[{ left: radio }, styles.radioCircle]} />)
  }

  JobRole = () => {
    Apis.job_role()
      .then((res) => {
        if (res.response_code == 2000) {
          const data = res.response_data.docs;
          this.setState({
            dataSource: data
          })
          console.log(data, "data");

        }
        else {
          Toast.show('Info ', res.response_message);
        }
      })
  }

  validate = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      // Toast.show('Email is not Correct');
      this.setState({ email: "incorrect" })
      return false;
    }
    else {
      this.setState({ email: text })
      // Toast.show('Email is Correct');
    }
  }

  register = () => {
    if (this.state.password != this.state.confirmPassword) {
      Toast.show('Password and Confirm Password not Matched');
    }
    else if (this.state.first == '' || this.state.last == '' || this.state.email == '' ||  this.state.password == '' || this.state.confirmPassword == '' || this.state.value == '' || this.state.selectedId == '') {
      Toast.show('Field Cannot be blanked');
    }
    else if (this.state.password.length < 5) {
      Toast.show('Password Must be 6 Character');
    }
    else if (this.state.email == "incorrect") {
      Toast.show('Email is incorrect');
    }
    else {
      const apidata = {
        "fname": this.state.first,
        "lname": this.state.last,
        "email": this.state.email,
        "password": this.state.password,
        "employeeType": this.state.value,
        "current_job_title": this.state.currentTitleId,
        "preferJob": {
          "job_role": this.state.selectedId
        },
        "apptype": Platform.OS === 'android' ? "ANDROID" : 'IOS'
      }
      Apis.employee_registration(apidata)
        .then((res) => {
          if (res.response_code == 2000) {
            console.log(res.response_data, "response data");
            Toast.show("Success  " + res.response_message);
            // this.props.navigation.navigate('DrawerComponent');
            this.setState({
              stap: 2
            })
          }
          else {
            Toast.show("Info  " + res.response_message);
            //this.props.navigation.navigate('DrawerComponent')
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }


  emailverification = () => {
    if (this.state.otp == "") {
      Toast.show("Otp Field Canot be blanked")
    }
    else {
      const data = {
        "email": this.state.email,
        "verification_code": this.state.otp
      }
      console.log(data, "data")
      Apis.employee_email_verification(data)
        .then((res) => {
          if (res.response_code == 2000) {
            Toast.show("Success  " + res.response_message);
            // this.props.navigation.navigate('DrawerComponent');
            console.log(res.response_data, "response_data");
            this.setdata(res.response_data)
          }
          else {
            Toast.show("Info  " + res.response_message);
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }

  setdata = async (res) => {
    this.props.save_user(res.authtoken)
    await AsyncStorage.setItem("authtocken", res.authtoken);
    await AsyncStorage.setItem("usertype", res.user_type);
  }

  renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }

  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        
        <View style={{ height: Platform.OS === 'android' ? StatusBar.currentHeight + 40 : 60, flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginTop:10 }}>
          <View style={{ flex: 0.2, alignItems: "center" }}>
            <Ionicons
              onPress={() => this.props.navigation.goBack()}
              name="md-arrow-back"
              size={25}
              color="black"
            />
          </View>
        </View>

        <ScrollView showsVerticalScrollIndicator={false}>
        <KeyboardAvoidingView behavior="position" keyboardVerticalOffset={-180} style={{flex:1}}>

        <View style={[styles.repeatContainer, {marginBottom:30}]}>
          <Text style={{ fontSize: 35, fontFamily: FontFamily.Poppins_Medium, color:"#1a2246" }}>Registration</Text>
          <View style={{ alignItems: "center", flexDirection: "row", }}>
              <Text style={styles.newUser_Text}>Already have an account?</Text>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("Login")}>
                <Text style={{ color: "#1a2246", fontSize: FontSize.small_size }}> Sign In</Text>
              </TouchableOpacity>
            </View>
        </View>
        {
          this.state.stap == 1 ?
            <View style={{width:"100%"}}>
              <View style={[styles.repeatContainer]}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={"100%"}
                  label="First Name"
                  value={this.state.first}
                  onChangeText={(val) => this.setState({ first: val })}
                  placeholder={"First Name of the job poster"}
                />
              </View>

              
              <View style={[styles.repeatContainer]}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={"100%"}
                  label="Last Name"
                  value={this.state.last}
                  onChangeText={(val) => this.setState({ last: val })}
                  placeholder={"Last Name of the job poster"}
                />
              </View>

             
              <View style={[styles.repeatContainer]}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={"100%"}
                  label="Email"
                  // value={this.state.email}
                  onChangeText={(val) => this.validate(val)}
                  placeholder={"Enter your Email"}
                  autoCapitalize='none'
                />
              </View>

              <View style={[styles.repeatContainer]}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={"100%"}
                  label="Password"
                  value={this.state.password}
                  onChangeText={(val) => this.setState({ password: val })}
                  placeholder={"Enter your Password"}
                  secureTextEntry={true}
                />
              </View>

              
              <View style={[styles.repeatContainer]}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={"100%"}
                  label="Confirm Password"
                  value={this.state.confirmPassword}
                  onChangeText={(val) => this.setState({ confirmPassword: val })}
                  placeholder={"Enter Confirm Password"}
                  secureTextEntry={true}
                />
              </View>
              

              <View style={styles.repeatContainer}>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <RadioForm
                    radio_props={radio_props}
                    initial={0}
                    formHorizontal={true}
                    labelHorizontal={true}
                    buttonColor={'#45cfd2'}
                    selectedButtonColor={'#45cfd2'}
                    animation={true}
                    onPress={(value) => {this.setState({value:value})}}
                    labelStyle={{marginRight: 10, fontFamily:FontFamily.Poppins_Regular }}
                    buttonSize={10}
                    style={{flexWrap:"wrap"}}
                  />
                  {/* <RadioButton.Group
                    onValueChange={value => this.setState({ experience: value })}
                    value={this.state.experience}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                      <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium }}>Freasher</Text>
                      {this.renderRadioCircleIos('72%')}
                      <RadioButton value="Fresher" />
                    </View>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                      <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium }}>Experience</Text>
                      {this.renderRadioCircleIos('76%')}
                      <RadioButton value="Experience" />
                    </View>
                  </RadioButton.Group> */}
                </View>
              </View>

              {this.state.experience == "Experience" ?
                <View style={[styles.repeatContainer]}>
                  <Text style={styles.title}>Current Job Title</Text>
                  <RNPicker
                    dataSource={this.state.dataSource}
                    dummyDataSource={this.state.dataSource}
                    defaultValue={false}
                    pickerTitle={"Current Job Title"}
                    showSearchBar={true}
                    disablePicker={false}
                    changeAnimation={"none"}
                    searchBarPlaceHolder={"Search....."}
                    showPickerTitle={true}
                    searchBarContainerStyle={this.props.searchBarContainerStyle}
                    pickerStyle={styles.pickerStyle}
                    pickerItemTextStyle={styles.listTextViewStyle}
                    selectedLabel={this.state.currentTitleText}
                    placeHolderLabel="Current Job Title"
                    selectLabelTextStyle={styles.selectLabelTextStyle}
                    placeHolderTextStyle={styles.placeHolderTextStyle}
                    dropDownImageStyle={styles.dropDownImageStyle}
                    dropDownImage={require("../../Images/home/60995.png")}
                    selectedValue={(index, item) => this.currentJobTitle(index, item)}
                  />

                  
                </View>
                
                : null
              }
              <View style={[styles.repeatContainer]}>
                <Text style={styles.title}>Prefferd Job Role</Text>
                <RNPicker
                  dataSource={this.state.dataSource}
                  dummyDataSource={this.state.dataSource}
                  defaultValue={false}
                  pickerTitle={"Prefferd Job Role"}
                  showSearchBar={true}
                  disablePicker={false}
                  changeAnimation={"none"}
                  searchBarPlaceHolder={"Search....."}
                  showPickerTitle={true}
                  searchBarContainerStyle={this.props.searchBarContainerStyle}
                  pickerStyle={styles.pickerStyle}
                  pickerItemTextStyle={styles.listTextViewStyle}
                  selectedLabel={this.state.selectedText}
                  placeHolderLabel="Prefferd Job Role"
                  selectLabelTextStyle={styles.selectLabelTextStyle}
                  placeHolderTextStyle={styles.placeHolderTextStyle}
                  dropDownImageStyle={styles.dropDownImageStyle}
                  dropDownImage={require("../../Images/home/60995.png")}
                  selectedValue={(index, item) => this._selectedValue(index, item)}
                />
              </View>

              

              <View style={{ marginTop: 20, marginBottom: 50, alignItems: "center" }}>
                <ButtonComponent
                  buttonName="Submit"
                  backgroundColor1="#1a2246"
                  height={50}
                  width="85%"
                  onClick={()=> this.register()}
                  // onClick={() => this.setState({
                  //   stap: 2
                  // })}
                />
              </View>

              {/* <View style={{ alignItems: "center", flexDirection: "row", alignSelf: "center" }}>
                <Text style={styles.newUser_Text}>New User?</Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("Login")}>
                  <Text style={{ color: "#1a2246", fontSize: FontSize.small_size }}> Sign In</Text>
                </TouchableOpacity>
              </View> */}
            </View>
            : null}
        {
          this.state.stap == 2 ?
            <View>
              <View style={[styles.repeatContainer]}>
                <FloatingLabelInput
                  textInputHeight={35}
                  textInputWidth={"100%"}
                  label="OTP"
                  value={this.state.otp}
                  onChangeText={(val) => this.setState({ otp: val })}
                  placeholder={"Enter Your OTP"}
                  maxLength={4}
                />
              </View>
              {/* <TextInputComponent
                titleName={"OTP"}
                iconType={"image"}
                width={16}
                height={20}
                image={require("../../Images/employee_Registration/man.png")}
                placeholder={"Enter your OTP"}
                onChangeText={(val) => this.setState({ poster: val })}
              /> */}
              <View style={{ marginBottom: 50, alignItems: "center" }}>
                <ButtonComponent
                  buttonName="Create Account"
                  backgroundColor1="#1a2246"
                  height={50}
                  width="85%"
                  // onClick={() => this.props.navigation.navigate("DrawerComponent")}
                  onClick={()=> this.emailverification()}
                />
              </View>
            </View>
            : null}
            </KeyboardAvoidingView>
            </ScrollView>
      </View>
    );
  }
}

export default connect(null, {save_user})(EmployeeRegistration)

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:"#fff"
  },
  newUser_Text: {
    color: "#696969",
    fontSize: FontSize.small_size
  },
  SignUp_text: {
    color: "#1a2245",
    fontSize: FontSize.small_size
  },
  repeatContainer: {
    width: "85%", alignSelf: "center", marginBottom: 15,
  },
  radioCircle: {
    padding: 9, 
    borderWidth: 2, 
    borderColor: Colors.Nero, 
    borderRadius: 100, 
    position: 'absolute'
  },
  itemSeparatorStyle: {
    height: 1,
    width: "90%",
    alignSelf: "center",
    backgroundColor: "#D3D3D3"
  },
  searchBarContainerStyle: {
    marginBottom: 10,
    flexDirection: "row",
    height: 35,
    shadowOpacity: 1.0,
    shadowRadius: 5,
    shadowOffset: {
      width: 1,
      height: 1
    },
    backgroundColor: "rgba(255,255,255,1)",
    shadowColor: "#d3d3d3",
    borderRadius: 10,
    elevation: 3,
    marginLeft: 10,
    marginRight: 10
  },

  selectLabelTextStyle: {
    color: "#000",
    textAlign: "left",
    width: "99%",
    paddingVertical: 10,
    flexDirection: "row",
    fontSize: 15,
    // backgroundColor:"#000"
  },
  placeHolderTextStyle: {
    color: "#999",
    paddingVertical: 10,
    textAlign: "left",
    width: "99%",
    flexDirection: "row",
    fontSize: 15
  },
  dropDownImageStyle: {
    marginLeft: 10,
    width: 10,
    height: 10,
    alignSelf: "center"
  },
  listTextViewStyle: {
    color: "#000",
    marginVertical: 10,
    flex: 0.9,
    marginLeft: 20,
    marginHorizontal: 10,
    textAlign: "left"
  },
  pickerStyle: {
    marginLeft: 14,
    paddingRight: 25,
    marginRight: 10,
    marginBottom: 2,
    borderBottomWidth: 1,
    borderBottomColor: "#999",
    backgroundColor: "rgba(255,255,255,255)",
    flexDirection: "row"
  },
  title:{
    fontSize:FontSize.small_size,
    fontFamily:FontFamily.Poppins_Medium,
  }

})