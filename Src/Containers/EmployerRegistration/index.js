import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, ImageBackground, StatusBar, Image, TouchableOpacity, Platform, ScrollView, SafeAreaView, KeyboardAvoidingView } from 'react-native';
import Colors from "./../../Utils/Colors";
import FontSize from "./../../Utils/fonts";
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import TextInputComponent from "../../Components/Login/TextInput"
import ButtonComponent from '../../Components/Button/loginButton'
import Apis from '../../Network/apicall';
import Toast from 'react-native-simple-toast';
import FloatingLabelInput from '../../Components/FlotingTextInput';
import RNPickerSelect from 'react-native-picker-select';
import FontFamily from '../../Utils/FontFamily';
import { save_user } from '../../actions/userAction';
import { connect } from 'react-redux';

const screenHeight = Dimensions.get('window').height
const screenwidth = Dimensions.get('window').width


class EmployerRegistration extends Component {


  state = {
    passIcon: true,
    passIcon1: true,
    email: "",
    password: "",
    confirmPassword: "",
    fName: "",
    lName: "",
    cName: "",
    designation: "",
    stap: 1,
    company_name: "",
    otp: "",
    namecompany: []
  }

  searchCompanyName = () => {
    const page = 1;
    const limit = 1000;
    const name = this.state.company_name;
    Apis.SearchCompany(page, limit, name)
      .then((res) => {
        if (res.response_code == 2000) {
          const companyname = res.response_data.docs;
          this.setState({
            namecompany: companyname
          })
          console.log(this.state.namecompany, "companyname");
        }
        else {
          console.log("Info", res.response_message);

        }
      })
  }

  handleIconChange() {
    if (this.state.passIcon == true) {
      this.setState({ passIcon: false })
    } else {
      this.setState({ passIcon: true })
    }
  }

  handleIconChange1() {
    if (this.state.passIcon1 == true) {
      this.setState({ passIcon1: false })
    } else {
      this.setState({ passIcon1: true })
    }
  }

  validate = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      // Toast.show('Email is not Correct');
      this.setState({ email: "incorrect" })
      return false;
    }
    else {
      this.setState({ email: text })
      // Toast.show('Email is Correct');
    }
  }

  register = () => {
    if (this.state.password != this.state.confirmPassword) {
      Toast.show('Password and Confirm Password not Matched');
    }
    else if (this.state.company_name == '' || this.state.designation == '' || this.state.fname == '' || this.state.lname == '' || this.state.email == '' || this.state.password == '' || this.state.confirmPassword == '') {
      Toast.show('Field Cannot be blanked');
    }
    else if (this.state.password.length < 5) {
      Toast.show('Password Must be 6 Character');
    }
    else if (this.state.email == "incorrect") {
      Toast.show('Email is incorrect');
    }
    else {
      const apidata = {
        "company_name": this.state.company_name,
        "designation": this.state.designation,
        "fname": this.state.fName,
        "lname": this.state.lName,
        "email": this.state.email,
        "password": this.state.password,
        "apptype": Platform.OS === 'android' ? "ANDROID" : 'IOS'
      }
      Apis.employer_registration(apidata)
        .then((res) => {
          if (res.response_code == 2000) {
            console.log(res.response_data, "response data");
            Toast.show("Success  " + res.response_message);
            this.setState({
              stap: 2
            })
            // this.props.navigation.navigate('DrawerComponent');
          }
          else if (res.response_code == 2008) {
            Toast.show("Info  " + res.response_message);
            //this.props.navigation.navigate('DrawerComponent')
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }


  emailverification = () => {
    if (this.state.otp == "") {
      Toast.show("Otp Field Canot be blanked")
    }
    else {
      const data = {
        "email": this.state.email,
        "verification_code": this.state.otp
      }
      Apis.employer_email_verification(data)
        .then((res) => {
          if (res.response_code == 2000) {
            Toast.show("Success  " + res.response_message);
            // this.props.navigation.navigate('DrawerComponent');
            console.log(res.response_data, "response_data");
            this.setdata(res.response_data)
          }
          else {
            Toast.show("Info  " + res.response_message);
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }

  setdata = async (res) => {
    this.props.save_user(res.authtoken)
    await AsyncStorage.setItem("authtocken", res.authtoken);
    await AsyncStorage.setItem("usertype", res.user_type);
  }

  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ flex: 0, backgroundColor: Colors.StatusBar }} />

        <View style={{ height: Platform.OS === 'android' ? StatusBar.currentHeight + 40 : 60, flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginTop: 10 }}>
          <View style={{ flex: 0.2, alignItems: "center" }}>
            <Ionicons
              onPress={() => this.props.navigation.goBack()}
              name="md-arrow-back"
              size={25}
              color="black"
            />
          </View>
        </View>

        <KeyboardAvoidingView behavior="padding" style={{flex:1}}>
        <ScrollView showsVerticalScrollIndicator={false}>
              <View style={[styles.repeatContainer, { marginBottom: 30 }]}>
                <Text style={{ fontSize: 35, fontFamily: FontFamily.Poppins_Medium, color: "#1a2246" }}>Registration</Text>
                <View style={{ alignItems: "center", flexDirection: "row", }}>
                  <Text style={styles.newUser_Text}>Already have an account?</Text>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate("Login")}>
                    <Text style={{ color: "#1a2246", fontSize: FontSize.small_size }}> Sign In</Text>
                  </TouchableOpacity>
                </View>
              </View>

              {this.state.stap == 1 ?
                <View>
                  <View style={[styles.repeatContainer]}>
                    <FloatingLabelInput
                      textInputHeight={35}
                      textInputWidth={"100%"}
                      label="First Name"
                      value={this.state.fName}
                      onChangeText={(val) => this.setState({ fName: val })}
                      placeholder={"Enter your First name"}
                    />
                  </View>
                  <View style={[styles.repeatContainer]}>
                    <FloatingLabelInput
                      textInputHeight={35}
                      textInputWidth={"100%"}
                      label="Last Name"
                      value={this.state.lName}
                      onChangeText={(val) => this.setState({ lName: val })}
                      placeholder={"Enter your Last name"}
                    />
                  </View>
             
                  <View style={[styles.repeatContainer]}>
                    <FloatingLabelInput
                      textInputHeight={35}
                      textInputWidth={"100%"}
                      label="Email"
                      // value={this.state.email}
                      onChangeText={(val) => this.validate(val)}
                      placeholder={"Ex. you@emample.com"}
                      autoCapitalize='none'
                    />
                  </View>
             
                  <View style={[styles.repeatContainer, { zIndex: 99999999 }]}>
                    <FloatingLabelInput
                      textInputHeight={35}
                      textInputWidth={"100%"}
                      label="Company Name"
                      value={this.state.company_name}
                      onChangeText={(val) => this.setState({ company_name: val })}
                      placeholder={"Company Name"}
                      onChange={() => {
                        this.searchCompanyName()
                      }}
                    />
                    <View style={{ position: "absolute", top: 50, left: 0, width: "100%", backgroundColor: "#fff", zIndex: 999999999 }}>
                      {
                        this.state.namecompany.map((data, i) => {
                          return (
                            <TouchableOpacity style={{ paddingHorizontal: 10, paddingVertical: 5 }} key={i} onPress={() => this.setState({
                              company_name: data.name,
                              namecompany: []
                            })}>
                              <Text style={{ fontSize: 12 }}>{data.name}</Text>
                            </TouchableOpacity>
                          )
                        })
                      }
                    </View>
                  </View>

                  <View style={[styles.repeatContainer]}>
                    <FloatingLabelInput
                      textInputHeight={35}
                      textInputWidth={"100%"}
                      label="Job Title"
                      value={this.state.designation}
                      onChangeText={(val) => this.setState({ designation: val })}
                      placeholder={"Company Job Title"}
                    />
               
                  </View>
                  <View style={[styles.repeatContainer]}>
                    <FloatingLabelInput
                      textInputHeight={35}
                      textInputWidth={"100%"}
                      label="Password"
                      value={this.state.password}
                      onChangeText={(val) => this.setState({ password: val })}
                      placeholder={"Enter Password"}
                      secureTextEntry={true}
                    />
                  </View>

                  <View style={[styles.repeatContainer]}>
                    <FloatingLabelInput
                      textInputHeight={35}
                      textInputWidth={"100%"}
                      label="Confirm Password"
                      value={this.state.confirmPassword}
                      onChangeText={(val) => this.setState({ confirmPassword: val })}
                      placeholder={"Enter Confirm Password"}
                      secureTextEntry={true}
                    />
                  </View>
                  <View style={{ marginTop: 25, marginBottom: 25, alignItems: "center" }}>
                    <ButtonComponent
                      buttonName="Submit"
                      backgroundColor1="#1a2246"
                      height={50}
                      width="85%"
                      onClick={() => this.register()}
                    // onClick={() => this.setState({
                    //   stap: 2
                    // })}
                    />
                    {/* <ButtonComponent
                buttonName="Submit"
                //onClick={() => this.register()}
                
              /> */}
                  </View>

                  {/* <View style={{ alignItems: "center", flexDirection: "row", alignSelf: "center" }}>
              <Text style={styles.newUser_Text}>New User?</Text>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("Login")}>
                <Text style={{ color: "#1a2246", fontSize: FontSize.small_size }}> Sign In</Text>
              </TouchableOpacity>
            </View> */}
                </View>
                : null
              }
              {
                this.state.stap == 2 ?
                  <View style={[styles.repeatContainer]}>
                    <FloatingLabelInput
                      textInputHeight={35}
                      textInputWidth={"100%"}
                      label="OTP"
                      value={this.state.otp}
                      onChangeText={(val) => this.setState({ otp: val })}
                      placeholder={"Enter your OTP"}
                      maxLength={4}
                    />
                    <View style={{ marginTop: 20, alignItems: "center" }}>
                      <ButtonComponent
                        width={"100%"}
                        buttonName="Create Account"
                        // onClick={() => this.props.navigation.navigate("DrawerComponent")}
                        onClick={() => this.emailverification()}
                      />
                    </View>
                  </View>
                  : null
              }
        </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

export default connect(null, { save_user })(EmployerRegistration)

const styles = StyleSheet.create({
  container: {
    width: screenwidth,
    height: screenHeight
  },
  newUser_Text: {
    color: "#696969",
    fontSize: FontSize.small_size
  },
  SignUp_text: {
    color: "#1a2245",
    fontSize: FontSize.small_size
  },
  repeatContainer: {
    width: "85%", alignSelf: "center", marginBottom: 15, position: "relative"
  },
  title: {
    fontSize: 12
  }
})

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 15,
    paddingVertical: 12,
    paddingHorizontal: 0,
    // borderWidth: 1,
    // borderColor: 'gray',
    // borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 15,
    paddingHorizontal: 0,
    paddingVertical: 0,
    padding: 0,
    margin: 0,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});
