import React, { Component,useState, useEffect } from 'react';
import { TouchableOpacity,View, Text, StyleSheet, Dimensions, FlatList,TextInput,Image, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import ContactList from '../../Components/ContactList';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
import moment from "moment";
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import MyStatusBar from '../../Utils/MyStatusBar';
import Colors from '../../Utils/Colors';


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const CurrentTime=moment(new Date()).format("YYYY-MM-DD");
class CalenderSchedule extends Component {

  state = {
    postjob: '',
    text:"",
    monthsName: "",
    monthlist:["January","February","March","April","May","June","July","August","September","October","November","December"],
    date: 0,
    month: 0,
    year:0,
    today:moment(new Date()).format("YYYY-MM-DD"),
  }

  componentDidMount(){
    let CurrentDate = new Date().getDate();
    let CurrentMonth = new Date().getMonth() + 1;
    let CurrentYear = new Date().getFullYear();
    //let CurrentFullDate= ;
    console.log("Current time ======>  ",CurrentDate,CurrentMonth,CurrentYear,);
    
    this.setState({date:CurrentDate,month:CurrentMonth,year:CurrentYear,})
  }

  render() {
    console.log("Within render === ",this.state.today.toString(),moment(new Date()).format("YYYY-MM-DD"));
    
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          backicon
          dot
          onClick2={() => this.props.navigation.goBack()}
          headerTitle="Shedule"
          noNotification
        />
        
        <CalendarList
  
  onVisibleMonthsChange={(months) => {
    console.log('now gghjkjhthese months are visible', months);
    console.log("jgyuftyftyftyyu========",this.state.monthlist[0]);
    
    this.setState({monthsName:this.state.monthlist[months[0].month]})
  }}
  theme={{
    backgroundColor: '#ffffff',
    calendarBackground: '#ffffff',
    textSectionTitleColor: '#b6c1cd',
    selectedDayBackgroundColor: '#00adf5',
    selectedDayTextColor: '#ffffff',
    todayTextColor: '#00adf5',
    dayTextColor: '#2d4150',
    textDisabledColor: '#d9e1e8',
    dotColor: '#00adf5',
    selectedDotColor: '#ffffff',
    arrowColor: 'orange',
    disabledArrowColor: '#d9e1e8',
    monthTextColor: 'blue',
    indicatorColor: 'blue',
    // textDayFontFamily: 'monospace',
    // textMonthFontFamily: 'monospace',
    // textDayHeaderFontFamily: 'monospace',
    textDayFontWeight: '300',
    textMonthFontWeight: 'bold',
    textDayHeaderFontWeight: '300',
    textDayFontSize: 16,
    textMonthFontSize: 16,
    textDayHeaderFontSize: 16
  }}

  pastScrollRange={50}
  pagingEnabled={true}
  //showScrollIndicator={false}
  futureScrollRange={50}
  //selected={true}
  scrollEnabled={true}
  horizontal={false}
  
  //showScrollIndicator={true}
  markedDates={{
    '2020-03-13': {selected: true, marked: true},
    // '2020-02-17': {marked: true},
    CurrentTime : {selected: true, marked: true,}
    
  }}
  
/>
          
       
      </View>
    );
  }
}

export default CalenderSchedule;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  },
  message:{
    marginTop:15,
   fontFamily: FontFamily.Poppins_Medium,
   fontSize: FontSize.medium_size,
  },
  socialImage: {
    width:40,
    height:40
  },
  socialMediaView: {
    flexDirection:"row",
    justifyContent:"space-between",
    marginTop: 20,
    marginBottom: 30,
  },
  socialIconName:{
    fontFamily: FontFamily.Poppins_Medium,
   fontSize: FontSize.micro_size,
   fontWeight:"600"
  },
  shareText:{
    fontFamily: FontFamily.Poppins_Medium,
   fontSize: FontSize.micro_size,
   fontWeight:"600"
  },
  suggestionText: {
    marginBottom:30,
    fontFamily: FontFamily.Poppins_Medium,
   fontSize: FontSize.small_size,
   fontWeight:"600"
  }
})