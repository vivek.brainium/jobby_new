import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from './../../Components/Button/loginButton'
import FontFamily from '../../Utils/FontFamily';
import { ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import Colors from '../../Utils/Colors';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;


class EmployerProfile extends Component {

  state = {
    postjob: '',
    storage: ''
  }

  async componentDidMount() {
    const storage = await AsyncStorage.getItem('usertype');
    this.setState({
      storage: storage
    })
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex: 0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          backicon
          onClick2={() => this.props.navigation.goBack()}
          headerTitle="Employer Profile"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View>
            <View style={styles.secondbody}>
              <View style={styles.repeatbody}>
                <View style={styles.firstcontainer}>
                  <View style={styles.profileImage}>
                    <Image source={require('../../Images/EmployerProfile/pr.png')} style={{ height: 90, width: 90, borderRadius: 50 }} />
                  </View>
                  <View style={styles.profileDetails}>
                    <Text style={styles.profiletitle}>Company Name:</Text>
                    <Text style={styles.profiledesc}>Treebo Unvelis</Text>
                  </View>
                </View>
                <View>
                  <Text style={styles.addresstitle}>Company Address:</Text>
                  <Text style={styles.fulladdress}>1600 Pennsylan Ave, NW, Washington DC 20050</Text>
                </View>
              </View>
            </View>

            <View style={styles.secondbody}>
              <View style={styles.repeatbody}>
                <Text style={styles.aboutcompanytitle}>About Company</Text>
                <View>
                  <Text style={styles.cardText}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                    enim ad minim veniam, quis nostrud exercitation ullamco laboris
                    nisi ut aliquip ex ea commodo consequat.  Duis aute irure dolor
                    in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                    nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                    sunt in culpa qui officia deserunt mollit anim id est laborum
            </Text>
                </View>
              </View>
            </View>
            {this.state.storage == "employers" ?
              <View style={{ flexDirection: "row", justifyContent: 'center', alignItems: "center", marginBottom: 40 }}>
                <ButtonComponent
                  buttonName="Edit Company Details"
                  backgroundColor1="#73e470"
                  height={50}
                  width="90%"
                  onClick={() => this.props.navigation.navigate('CompanyEdit')}
                />
              </View>
              :
              <View style={{ flexDirection: "row", justifyContent: 'center', alignItems: "center", marginBottom: 40 }}>
                <ButtonComponent
                  buttonName="Current Opening"
                  backgroundColor1="#73e470"
                  height={50}
                  width="90%"
                  onClick={() => this.props.navigation.navigate('Opening')}
                />
              </View>
            }
          </View>
        </ScrollView>

      </View>
    );
  }
}

export default EmployerProfile;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  },
  cardText: {
    color: "#2f2f2f",
    fontFamily: FontFamily.Poppins_Light
  },
  secondbody: {
    backgroundColor: "#e9e9e9", marginBottom: 10, elevation: 3, marginBottom: 15
  },
  repeatbody: { backgroundColor: "#ffffff", padding: 20 },
  firstcontainer: { flexDirection: "row", marginBottom: 10 },
  profileImage: { flex: 0.30, alignItems: "flex-start", },
  profileDetails: { flex: 0.70, justifyContent: "center" },
  profiletitle: { fontSize: 14, fontFamily: FontFamily.Poppins_Regular },
  profiledesc: { fontSize: FontSize.large_size, fontWeight: "600", fontFamily: FontFamily.Poppins_Medium },
  addresstitle: { fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium, marginBottom: 10 },
  fulladdress: { fontSize: 15, fontFamily: FontFamily.Poppins_Medium, marginBottom: 10, color: "#3c3c3c" },
  aboutcompanytitle: { marginBottom: 5, fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Medium }
})