import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import EmployerComponent from "../../Components/EmployerComponent"
import Colors from '../../Utils/Colors';

const screenHeight = Math.round(Dimensions.get('window').height);
const screenwidth = Math.round(Dimensions.get('window').width);

const DATA = [
  {
    id: 1,
    companyName: "Adeeco india",
    location: "San Fransisco",
  },
  {
    id: 2,
    companyName: "TCG Pvt Ltd",
    location: "San Fransisco",
  },
  {
    id: 3,
    companyName: "Adeeco india",
    location: "San Fransisco",
  },
  {
    id: 4,
    companyName: "Adeeco india",
    location: "San Fransisco",
  },
  {
    id: 5,
    companyName: "Adeeco india",
    location: "San Fransisco",
  },
  {
    id: 6,
    companyName: "Adeeco india",
    location: "San Fransisco",
  },
];

class Employer extends Component {

  state = {
    postjob:''
  }
  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <View>
          <HeaderComponent
            drawericon
            type={"textinput"}
            onClick={() => this.props.navigation.openDrawer()}
            headerTitle="Employers"
            onChangeText={(postjob) => this.setState({ postjob })}
            placeholder="Search"
            notificationonpress={()=> this.props.navigation.navigate("Notification")}
          />
        </View>

        <FlatList
          data={DATA}
          renderItem={({ item }) => (
            <EmployerComponent
              id={item.id}
              CompanyName={item.companyName}
              Location={item.location}
            />
          )}
          keyExtractor={item => item.id}
          showsVerticalScrollIndicator={false}
        />

      </View >
    );
  }
}

export default Employer;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  }
})