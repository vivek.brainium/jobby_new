import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from '../../Components/Button/loginButton'
import ApplicationComponent from '../../Components/Application';
import Colors from '../../Utils/Colors';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;


const DATA = [
  {
    id: 1,
    title: "Polard Neo",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 2,
    title: "Polard Neo",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 3,
    title: "Polard Neo",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 4,
    title: "Polard Neo",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
  {
    id: 5,
    title: "Polard Neo",
    years: "4-6 years",
    locationname: "California",
    desingnation: "Software Developer",
    posteddate: "02.02.2019",
    image: require('../../Images/EmployerProfile/image.jpg')
  },
];


class EmployeeList extends Component {
  state = {
    postjob: ''
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex: 0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          headerTitle="Application"
          type={"textinput"}
          onChangeText={(postjob) => this.setState({ postjob })}
          placeholder="Search"
          notificationonpress={()=> this.props.navigation.navigate("Notification")}
        />
        
        <View style={{ flex: 1, }}>
          <FlatList
            data={DATA}
            renderItem={({ item }) => (
              <ApplicationComponent
                id={item.id}
                title={item.title}
                years={item.years}
                profile_image
                image={item.image}
                locationname={item.locationname}
                designatonsection
                desingnation={item.desingnation}
                verticalthreedot
                gotodetails={() => this.props.navigation.navigate("Employee")}
                maincontainerPress={() => { this.props.navigation.navigate("Employee") }}
              />
            )}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    );
  }
}

export default EmployeeList;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
  },
})