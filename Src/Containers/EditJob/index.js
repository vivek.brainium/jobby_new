import React, { Component } from 'react';
import { View, Text, ImageBackground, StyleSheet, StatusBar, Dimensions, Image, TouchableOpacity, SafeAreaView } from 'react-native';
import HeaderComponent from '../../Components/Header'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import FontSize from "./../../Utils/fonts";
import ReadMore from 'react-native-read-more-text';
import ButtonComponent from './../../Components/Button/loginButton'
import Colors from '../../Utils/Colors';
import FontFamily from '../../Utils/FontFamily';
import MyStatusBar from '../../Utils/MyStatusBar';

const screenHeight = Dimensions.get('window').height
const screenwidth = Dimensions.get('window').width

class EditJob extends Component {

  state = {
    searchJob: ""
  }

  _handleTextReady = () => {
    console.log('ready!');
  }

  _renderTruncatedFooter = (handlePress) => {
    return (
      <Text style={{ color: "red", marginTop: 5 }} onPress={handlePress}>
        Read more
      </Text>
    );
  }

  _renderRevealedFooter = (handlePress) => {
    return (
      <Text style={{ color: "red", marginTop: 5 }} onPress={handlePress}>
        Show less
      </Text>
    );
  }

  render() {
    let { text } = this.props;
    console.log("this.props==>01" + JSON.stringify(this.props))
    return (
      <View style={{ width: screenwidth, height: screenHeight }}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => this.props.navigation.openDrawer()}
          placeholder="Search Job"
          headerTitle="View Job"
          notificationonpress={()=> this.props.navigation.navigate("Notification")}
        />
        

        <View style={{ backgroundColor: "#e9e9e9", marginBottom: 10, elevation: 3 }}>
          <View style={{ backgroundColor: "#ffffff", padding: 20 }}>
            <Text style={{ marginBottom: 10, fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Medium }}>HR Executive</Text>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ flex: 0.1, alignItems: 'flex-start' }}>
                <SimpleLineIcons
                  style={{ marginBottom: 10 }}
                  name="briefcase"
                  color="gray"
                  size={20}
                />

                <EvilIcons
                  style={{ marginBottom: 10 }}
                  name="location"
                  color="gray"
                  size={25}
                />

                <Image
                  style={{ width: 16, height: 22 }}
                  source={require('../../Images/job_Details/pen.png')}
                />
              </View>

              <View style={{ flex: 0.9, alignItems: "flex-start", justifyContent: "center" }}>
                <Text style={{ marginBottom: 10, fontFamily: FontFamily.Poppins_Medium }}>4-6 Years</Text>
                <Text style={{ marginBottom: 10, fontFamily: FontFamily.Poppins_Medium }}>California</Text>
                <Text style={{ fontFamily: FontFamily.Poppins_Medium }}>MBA</Text>
              </View>
            </View>
          </View>
        </View>


        <View style={{ backgroundColor: "#e9e9e9", marginBottom: 10, elevation: 3, marginBottom: 30 }}>
          <View style={{ backgroundColor: "#ffffff", padding: 20 }}>
            <Text style={{ marginBottom: 5, fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium }}>Profile Summary</Text>
            <View>
              <ReadMore
                numberOfLines={2}
                onReady={this._handleTextReady}
                renderTruncatedFooter={this._renderTruncatedFooter}
                renderRevealedFooter={this._renderRevealedFooter}
              >
                <Text style={styles.cardText}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                  enim ad minim veniam, quis nostrud exercitation ullamco laboris
                  nisi ut aliquip ex ea commodo consequat.  Duis aute irure dolor
                  in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                  nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                  sunt in culpa qui officia deserunt mollit anim id est laborum
            </Text>
              </ReadMore>
            </View>
          </View>
        </View>

        <View style={{ alignItems: "center" }}>
          <ButtonComponent
            buttonName="View Application"
            backgroundColor1="#78a7f5"
            height={50}
            width="88%"
          />
        </View>

      </View>
    );
  }
}

export default EditJob;

const styles = StyleSheet.create({
  cardText: {
    color: "#2f2f2f",
    fontFamily: "Poppins-Light"
  },
});

