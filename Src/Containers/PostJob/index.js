import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TextInput, ScrollView, KeyboardAvoidingView, Platform, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from '../../Components/Button/loginButton'
import FontFamily from '../../Utils/FontFamily';
import Colors from '../../Utils/Colors';
import TextInputComponent from "./../../Components/Login/TextInput"
// import { RadioButton } from 'react-native-paper';
import TagInput from 'react-native-tag-input';
import FloatingLabelInput from '../../Components/FlotingTextInput';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;
// import { CheckBox } from 'react-native-elements';


const horizontalInputProps = {
  keyboardType: 'default',
  returnKeyType: 'search',
  placeholder: 'Enter Tag',
  style: {
    fontSize: 16,
    marginVertical: Platform.OS == 'ios' ? -5 : -2,
    height: 40,
    padding: 0
  },
};

const horizontalScrollViewProps = {
  horizontal: true,
  showsHorizontalScrollIndicator: false,
};

var radio_props = [
  {label: 'Fulltime', value: 0 },
  {label: 'Part-time', value: 1 },
  {label: 'Temporary', value: 2 },
  {label: 'Volunteer', value: 3 },
  {label: 'Internship', value: 4 },
];

var seniority = [
  {label: 'Internship', value: 0 },
  {label: 'Entry', value: 1 },
  {label: 'Associate', value: 2 },
  {label: 'Senior', value: 3 },
  {label: 'Director', value: 4 },
  {label: 'Executive', value: 5 },
  {label: 'Non-applicable', value: 6 },
];


class PostJob extends Component {

  state = {
    title: '',
    location: '',
    value: 'first',
    horizontalTags: [],
    horizontalText: "",
    description: '',
    companydetails: '',
    tagline: '',
    experience: '',
    city: '',
    country: '',
    company: "",
    remote: 'internship',
    ar_title: '',
    value: 0,
  }

  

  labelExtractor = (tag) => tag;

  onChangeHorizontalTags = (horizontalTags) => {
    this.setState({
      horizontalTags,
    });
  };

  onChangeHorizontalText = (horizontalText) => {
    this.setState({ horizontalText });

    const lastTyped = horizontalText.charAt(horizontalText.length - 1);
    const parseWhen = [',', ' ', ';', '\n'];

    if (parseWhen.indexOf(lastTyped) > -1) {
      this.setState({
        horizontalTags: [...this.state.horizontalTags, this.state.horizontalText],
        horizontalText: "",
      });
      this._horizontalTagInput.scrollToEnd();
    }
  }

  renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios'
    return (<View style={[{ left: radio }, styles.radioCircle]} />)
  }

  render() {
    const { navigation } = this.props;
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex:0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          drawericon
          onClick={() => navigation.openDrawer()}
          headerTitle="Post Job"
          // type={"textinput"}
          // onChangeText={(postjob)=> this.setState({postjob})}
          // placeholder="Search"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
          drawerLeft={15}
        />
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" >
          <ScrollView
            contentContainerStyle={{ width: "100%", }}>
            <View style={{ alignItems: "center", marginBottom: 20, paddingTop: 30 }}>
              <Text style={styles.mainjobtitle}>Enter your job details</Text>
            </View>
            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.18}
                label="Company Name"
                value={this.state.company}
                onChangeText={(val) => this.setState({ company: val })}
                placeholder="Enter your Company Name"
              />
            </View>

            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.18}
                label="Job Title"
                value={this.state.title}
                onChangeText={(val) => this.setState({ title: val })}
                placeholder="Enter your Job Title"
              />
            </View>

            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.18}
                label="عنوان وظيفي"
                value={this.state.ar_title}
                onChangeText={(val) => this.setState({ ar_title: val })}
                placeholder="أدخل عنوان الوظيفة العربية"
              />
            </View>
            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.18}
                label="Country"
                value={this.state.country}
                onChangeText={(val) => this.setState({ country: val })}
                placeholder="Enter your Country Name"
              />
            </View>

            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.18}
                label="City"
                value={this.state.city}
                onChangeText={(val) => this.setState({ city: val })}
                placeholder="Enter your City Name"
              />
            </View>
          
            <View style={styles.repeatContainer}>
              <Text style={styles.title}>Application Job Type</Text>
              <View style={{ flexDirection: "column", flexWrap: "wrap", marginVertical: 10, }}>
                <RadioForm
                  radio_props={radio_props}
                  initial={0}
                  formHorizontal={true}
                  labelHorizontal={true}
                  buttonColor={'#45cfd2'}
                  selectedButtonColor={'#45cfd2'}
                  animation={false}
                  onPress={(value) => {this.setState({value:value})}}
                  labelStyle={{marginRight: 10, fontFamily:FontFamily.Poppins_Regular }}
                  buttonSize={10}
                  style={{flexWrap:"wrap"}}
                />
                
              </View>
            </View>
            <View style={styles.repeatContainer}>
              <Text style={[styles.title]}>Add Skill</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#fff', borderBottomColor: "#999", borderBottomWidth: 1, }}>
                <TagInput
                  ref={(horizontalTagInput) => { this._horizontalTagInput = horizontalTagInput }}
                  value={this.state.horizontalTags}
                  onChange={this.onChangeHorizontalTags}
                  labelExtractor={this.labelExtractor}
                  text={this.state.horizontalText}
                  onChangeText={this.onChangeHorizontalText}
                  tagColor="#05f7ef"
                  tagTextColor="#000"
                  inputProps={horizontalInputProps}
                  scrollViewProps={horizontalScrollViewProps}
                  tagContainerStyle={{ height: 35 }}
                />
              </View>
              <Text style={{ fontSize: FontSize.micro_size, color: "#cacbcc" }}>Note: Add "," for adding multiple tag</Text>
            </View>
        
            <View style={styles.repeatContainer}>
              <Text style={styles.title}>Job Describtion</Text>
              <TextInput
                underlineColorAndroid="transparent"
                placeholder="Job Description"
                placeholderTextColor="#999"
                autoCapitalize="none"
                onChangeText={(val) => this.setState({ description: val })}
                animation={false}
                value={this.state.description}
                style={styles.mytextinput}
              />
            </View>
            
            <View style={styles.repeatContainer}>
              {/* <Text style={styles.title}>Company Industry</Text> */}
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.18}
                label="Company"
                value={this.state.companyIndustrial}
                onChangeText={(val) => this.setState({ companyIndustrial: val })}
                placeholder={"Enter Your Company Industry"}
              />
            </View>
            
            <View style={styles.repeatContainer}>
              <Text style={styles.title}>Seniority Level</Text>
              <View style={{ flexDirection: "row", flexWrap: "wrap", marginVertical: 10, }}>
                <RadioForm
                  radio_props={seniority}
                  initial={0}
                  formHorizontal={true}
                  labelHorizontal={true}
                  buttonColor={'#45cfd2'}
                  selectedButtonColor={'#45cfd2'}
                  animation={false}
                  onPress={(value) => {this.setState({value:value})}}
                  labelStyle={{marginRight: 10, fontFamily:FontFamily.Poppins_Regular }}
                  buttonSize={10}
                  style={{flexWrap:"wrap"}}
                />
              
              </View>
            </View>
            

            <View style={styles.repeatContainer}>
              {/* <Text style={styles.title}>Year of Experience</Text> */}
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.18}
                label="Experience"
                value={this.state.experience}
                onChangeText={(val) => this.setState({ experience: val })}
                placeholder={"Year of Experience (only numbers)"}
                keyboardType="numeric"
              />
            </View>
            

            <View style={{ flexDirection: "row", justifyContent: 'space-around', marginBottom: 40 }}>
              <ButtonComponent
                buttonName="Submit Details"
                backgroundColor1="#73e470"
                height={50}
                width="80%"
                onClick={() => this.props.navigation.navigate("PostJob")}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

export default PostJob;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
    backgroundColor: "#fff"
  },
  repeatContainer: {
    width: "85%", alignSelf: "center", marginBottom: 10
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, height: 80, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },
  title: {
    fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium
  },
  mainjobtitle: {
    fontSize: FontSize.medium_size, fontFamily: "Poppins-Medium", color: Colors.BlackColor
  },
  radioCircle: {
    padding: 9,
    borderWidth: 2,
    borderColor: Colors.Nero,
    borderRadius: 100,
    position: 'absolute'
  }
})