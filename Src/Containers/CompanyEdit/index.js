import React, { Component } from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Dimensions, FlatList, TextInput, Image, ScrollView, SafeAreaView, KeyboardAvoidingView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import TextInputComponent from '../../Components/Login/TextInput';
import companyPic from '../../Images/Company_Profile/company.png'
import phonecallPic from '../../Images/Company_Profile/phonecall.png'
import designationPic from '../../Images/Company_Profile/designation.png'
import ButtonComponent from '../../Components/Button/loginButton';
import FloatingLabelInput from '../../Components/FlotingTextInput';
import FontFamily from '../../Utils/FontFamily';
import FontSize from '../../Utils/fonts';
import Colors from '../../Utils/Colors';



const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;


class CompanyEdit extends Component {

  state = {
    companyname: "Brainium Information Technology",
    address: "Washion DC",
    phoneNo: "9830731745",
    email: "loriem@mail.com",
    designation: "Loriem Ipsum Loriem Ipsum",
    country: "India",
    city: "kolkata",
    fname: "John",
    lname: "Doe",
    description: "Loriem Ipsum Loriem Ipsum Loriem Ipsum Loriem Ipsum"
  }

  render() {
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <SafeAreaView style={{ flex: 0, backgroundColor: Colors.StatusBar }} />
        <HeaderComponent
          backicon
          dot
          noNotification
          onClick2={() => this.props.navigation.goBack()}
          headerTitle="Edit Company Details"
          headingLeft={16}
        />
        <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
          <KeyboardAvoidingView behavior="padding">
            <View>
              <View style={[styles.repeatContainer, { paddingTop: 30 }]}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={screenwidth / 1.25}
                  label="Company name"
                  value={this.state.companyname}
                  onChangeText={(val) => this.setState({ companyname: val })}
                  placeholder="Ex. Brainium Information Technology"
                />
              </View>

              <View style={styles.repeatContainer}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={screenwidth / 1.25}
                  label="Country"
                  value={this.state.country}
                  onChangeText={(val) => this.setState({ country: val })}
                  placeholder="Enter Country Name"
                />
              </View>
              <View style={styles.repeatContainer}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={screenwidth / 1.25}
                  label="City"
                  value={this.state.city}
                  onChangeText={(val) => this.setState({ city: val })}
                  placeholder="Ex. Kolkata"
                />
              </View>

              <View style={styles.repeatContainer}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={screenwidth / 1.25}
                  label="First Name"
                  value={this.state.first}
                  onChangeText={(val) => this.setState({ first: val })}
                  placeholder="First Name of the job poster"
                />
              </View>

              <View style={styles.repeatContainer}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={screenwidth / 1.25}
                  label="Last Name"
                  value={this.state.last}
                  onChangeText={(val) => this.setState({ last: val })}
                  placeholder="Last Name of the job poster"
                />
              </View>

              <View style={styles.repeatContainer}>
                <FloatingLabelInput
                  textInputHeight={30}
                  textInputWidth={screenwidth / 1.25}
                  label="Email ID"
                  value={this.state.email}
                  onChangeText={(val) => this.setState({ email: val })}
                  placeholder="Enter Your Email Id"
                />
              </View>


              <View style={styles.repeatContainer}>
                <Text style={styles.title}>Job Describtion</Text>
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Job Description"
                  placeholderTextColor="#999"
                  autoCapitalize="none"
                  onChangeText={(val) => this.setState({ description: val })}
                  multiline={true}
                  value={this.state.description}
                  style={styles.mytextinput}
                />
              </View>


              <View style={{ marginBottom: 20, alignItems: "center", marginTop: 10 }}>
                <ButtonComponent
                  buttonName="Save"
                />
              </View>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  }
}

export default CompanyEdit;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
    // flex: 1,

  },
  repeatContainer: {
    width: "85%", alignSelf: "center", marginBottom: 10
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderRadius: 10, borderWidth: 1, height: 80, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },
  title: {
    marginBottom: 10,
    fontSize: FontSize.small_size,
    fontFamily: FontFamily.Poppins_Medium,
  }
})