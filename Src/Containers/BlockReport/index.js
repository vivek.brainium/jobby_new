import React, { Component } from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Dimensions, FlatList, TextInput, Image, SafeAreaView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import ContactList from '../../Components/ContactList';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
import BlockReportUsers from '../../Components/BlockReportUsers'
import MyStatusBar from '../../Utils/MyStatusBar';
import Colors from '../../Utils/Colors';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const BlockData = [
    {
        id: 1,
        name: "Ricky Fron",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        image: require('../../Images/EmployerProfile/image1.jpg'),
        unlock: "Unlock this user"
    },
    {
        id: 2,
        name: "Ricky Fron",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        image: require('../../Images/EmployerProfile/image1.jpg'),
        unlock: "Unlock this user"
    },
    {
        id: 3,
        name: "Ricky Fron",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        image: require('../../Images/EmployerProfile/image1.jpg'),
        unlock: "Unlock this user"
    },
    {
        id: 4,
        name: "Ricky Fron",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        image: require('../../Images/EmployerProfile/image1.jpg'),
        unlock: "Unlock this user"
    },
    {
        id: 5,
        name: "Ricky Fron",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        image: require('../../Images/EmployerProfile/image1.jpg'),
        unlock: "Unlock this user"
    },
    {
        id: 6,
        name: "Stuart Alexandra",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        image: require('../../Images/EmployerProfile/image1.jpg'),
        unlock: "Unlock this user"
    },
];

const ReportData = [
    {
        id: 1,
        name: "Ricky Fron",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        image: require('../../Images/EmployerProfile/image.jpg'),
        unlock: "Unlock this user"
    },
    {
        id: 2,
        name: "Ricky Fron",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        image: require('../../Images/EmployerProfile/image.jpg'),
        unlock: "Unlock this user"
    },
    {
        id: 3,
        name: "Ricky Fron",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        image: require('../../Images/EmployerProfile/image.jpg'),
        unlock: "Unlock this user"
    },
    {
        id: 4,
        name: "Ricky FronRicky Fron",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        image: require('../../Images/EmployerProfile/image.jpg'),
        unlock: "Unlock this user"
    },
    {
        id: 5,
        name: "Ricky FronRicky Fron",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        image: require('../../Images/EmployerProfile/image.jpg'),
        unlock: "Unlock this user"
    },
    {
        id: 6,
        name: "Ricky FronRicky Fron",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        image: require('../../Images/EmployerProfile/image.jpg'),
        unlock: "Unlock this user"
    },
];

class ReportBlock extends Component {

    state = {
        postjob: '',
        text: "",
        blockColor: "#000000",
        blockBorder: 1,
        reportColor: "#707070",
        reportborder: 0,
        blockViewColor: "#0c0c0c",
        reportViewColor: "",
        DATA: [],
    }

    componentDidMount() {
        this.setState({ DATA: BlockData })
    }

    blockUsers = () => {
        this.setState({ blockViewColor: "#0c0c0c", reportViewColor: "", blockColor: "#000000", reportColor: "#707070", blockBorder: 1, reportborder: 0, DATA: BlockData })
    }

    reportUsers = () => {
        this.setState({ blockViewColor: "", reportViewColor: "#0c0c0c", blockColor: "#707070", reportColor: "#000000", blockBorder: 0, reportborder: 1, DATA: ReportData })
    }

    render() {
        return (
            //Main Container
            <View style={styles.MainContainer}>
                <SafeAreaView style={{ flex: 0, backgroundColor: Colors.StatusBar }} />
                <HeaderComponent
                    drawericon
                    onClick={() => this.props.navigation.openDrawer()}
                    headerTitle="Block &amp; Report"
                />
                <View style={{ flex: 1, }}>
                    <View style={styles.TabContainer}>
                        <View style={[styles.blockUser_view,{borderBottomColor: this.state.blockViewColor, borderBottomWidth: this.state.blockBorder,}]}>
                            <TouchableOpacity onPress={this.blockUsers}>
                                <Text style={[styles.blockUsers,{ color: this.state.blockColor,}]}>Block Users</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.reportUser_view,{ borderBottomColor: this.state.reportViewColor, borderBottomWidth: this.state.reportborder,}]}>
                            <TouchableOpacity onPress={this.reportUsers}>
                                <Text style={[styles.reportUsers,{ color: this.state.reportColor,}]}>Report Users</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <FlatList
                        data={this.state.DATA}
                        renderItem={({ item }) => (
                            <BlockReportUsers
                                id={item.id}
                                name={item.name}
                                description={item.description}
                                image={item.image}
                                unlock={item.unlock}
                            />
                        )}
                        keyExtractor={item => item.id}
                        showsVerticalScrollIndicator={false}
                    />

                </View>
            </View>
        );
    }
}

export default ReportBlock;

const styles = StyleSheet.create({
    MainContainer: {
        //width: screenwidth,
        //height: screenHeight,
        flex: 1,
        marginBottom: 40
    },
    TabContainer: {
        flexDirection: "row",
        marginTop: 10,
        marginBottom: 35,
    },
    blockUser_view: { 
        justifyContent: "center", 
        height: 50, width: "50%", 
        alignItems: "center",
    },
    blockUsers: {
        fontSize: 17, 
        fontFamily: FontFamily.Poppins_Medium,
    },
    reportUser_view: {
        justifyContent: "center", 
        height: 50, 
        width: "50%", 
        alignItems: "center"
    },
    reportUsers: {
        fontSize: 17, 
        fontFamily: FontFamily.Poppins_Medium,
    }

})