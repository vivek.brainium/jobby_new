import NetInfo from "@react-native-community/netinfo";
import axios from 'axios'
import { apiurl } from '../url'

//THIS CODE IS FOR DEBUGGIN NETWORK CALLES IN CHROME DEVTOOLS
//REMOVE THIS ON PRODUCTION BUILD

//  XMLHttpRequest = GLOBAL.originalXMLHttpRequest ?
//    GLOBAL.originalXMLHttpRequest : GLOBAL.XMLHttpRequest;

//Main method for network calls using axios
export const Network = (method, endpoint, data = {}) => {
  return new Promise((resolve, reject) => {
    //cheking network connection
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        if(method == "GET"){
        axios({
          method,
          url: `${apiurl}${endpoint}`,
          headers:{
           'x-access-token': data.authtoken ? data.authtoken : null
          },
          body: data
        }).then((response) => {
            if(response.status === 200 || response.response_code == 2000) {
              resolve(response.data)
            } else {
              reject('something went wrong')
            }
        });
      } else{
        axios({
          method,
          url: `${apiurl}${endpoint}`,
          headers:{
           'x-access-token': data.authtoken ? data.authtoken : null
          },
          data
        }).then((response) => {
            if(response.status === 200 || response.response_code == 2000) {
              resolve(response.data)
            } else {
              reject('something went wrong')
            }
        });
      }
      }
    });
  })
}
