import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { Platform, NativeModules } from 'react-native';


export default class Language {
  static ln = async () => {
    return promise = new Promise(async (resolve, reject) => {
      const lng = await AsyncStorage.getItem('language')
      const deviceLanguage =
        Platform.OS === 'ios'
          ? NativeModules.SettingsManager.settings.AppleLocale ||
          NativeModules.SettingsManager.settings.AppleLanguages[0] //iOS 13
          : NativeModules.I18nManager.localeIdentifier;
      if (lng != null) {
        resolve(lng)
      }
      else if (deviceLanguage == "en_US" || deviceLanguage == "en_UK" || deviceLanguage == "en_IN") {
        resolve(0)
      }
      else {
        resolve(1)
      }
      // const lng = await AsyncStorage.getItem('language')
      // if(lng != null) {
      //   resolve(lng)
      // } else {
      //   resolve(1)
      // }

    })
  }
}