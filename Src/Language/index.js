export default language = [

  //ENGLISH
  {
    //SIGN IN
    signin: 'Sign In',
    email: 'E-mail',
    password: 'Password',
    forgotpassword: 'Forgot Your Password?',
    signinwithfacebookgmail: 'Sign in with',
    donthaveanaccount: 'Don’t have an account yet?',
    signup: 'Sign Up',
    singin: 'Sign In',

    // SOCIAL SIGN IN MODAL
    enterDetails: 'To Complete Registration Enter Below Details',
    enterPhone: 'To Complete Registration Enter Phone No',
    typeMobile: 'Enter Mobile Number',
    typeEmail: 'Enter Email',


    //FORGOT PASSWORD
    forgotpassword: 'Forgot your password',
    typeyouremail: 'Type your email',
    submit: 'Submit',
    and: 'and',

    //SIGN UP
    name: 'Name',
    email: 'Email',
    dob: 'Dob',
    password: 'Password',
    confirmpassword: 'Confirm password',
    alreadyhaveanaccount: 'Alreadve an account?',

    //MOBILE_POPUP
    mobile: 'Mobile',
    accept: 'I accept to the',
    mustagreee: 'You must have to agree the Terms and Conditions !',

    //OTP_OPTION
    whereshouldwesenttheotp: 'Where should we send the one-time password(OTP)',
    email: 'Email',
    sms: 'SMS',
    submit: 'Submit',

    //OTP_VERIFICATION
    verifyyouraccount: 'Verify your account',
    wehavesentaotp: 'We have sent a one-time password(OTP) on your mobile number',
    wehavesentaotpemail: 'We have sent a one-time password (OTP) on your Email',
    enterotp: 'Enter one-time password(OTP)',
    dontgetthecode: 'Don’t get the code? Resend',
    submit: 'Submit',

    //HOME
    change: 'Change',
    nearbyrestaurant: 'Nearby Restaurants',
    collect: 'Collect+',
    earn: 'Earn',
    clovers: 'Clovers',
    redeem: 'Redeem+',
    getflatdiscount: 'Get__flat discount',
    noreslistinyourlocation: 'No restaurant list in your location',

    //ORGANISATION_POPUP
    selectyourorganisation: 'Select your organization ',
    joinorganization: 'Join organization',
    typeorganizationname: 'Type organization name',
    addorganization: 'Add organization',
    join: 'Join',
    findorganization: 'Find Organization',
    searchyourorganization: 'Search your organization',
    ifyoudontseeyourorganization: 'If you dont see your organization, Add a new organization',
    therearenoorganizationnearby: 'There are no organization nearby',
    joined: 'Joined',

    //LOCATION_WHERE_SERVICE_NOT_AVILABLE
    sorry: 'Sorry',
    wedonthaveservice: 'We don’t have service here yet',
    vote: 'Vote',

    //CART_SCREEN
    preorder: 'Pre-Order',
    pickuptime: 'Pickup Time',
    preorderallow24hourlater: 'Pre-Order only allows you to place an order for up to 24 hours later',
    earn: 'You have earn',
    clovers: 'clovers in this order',
    yourorderwouldreadyin30min: 'Your order would ready in next ',
    subtotal: 'Sub total',
    cloverredeemed: '____Clovers Redeemed',
    total: 'Total (Tax included)',
    gui: 'GUI',
    promocode: 'Promo code',
    specialinstruction: 'Enter special instructions for your order',
    choosepromocode: 'Choose Promo code',
    youhaveclovers: 'You have',
    redeemnow: 'Redeem Now',
    payby: 'Pay by',
    // change: 'CHANGE',
    preorderpickuptime: 'Select pre order pickup time ',
    offerpickup: 'Offer pickup',
    soloorder: 'Solo order',
    teamorder: 'Team order',
    pickupwithteammatesoralone: 'Offer to pick up for up to 4 teammates or order alone',
    ordernow: 'Order now',
    termandconditions: 'I confirm that I have double-checked my order and that I won’t be able to modify or cancel after placing the order',

    //SEARCH_SCREEN
    searchbyrestaurantandcategories: 'Search by restaurant keywords & categories',

    //MY_FAVOURITE_RESTAURANT
    myfavouriterestaurant: 'My favorite restaurants',

    //SCAN_SCREEN
    startscanning: 'start scanning',
    orderid: 'Order id',
    restaurantname: 'Restaurant name',
    qrcode: 'Qr code',

    //PICKUP_SCREEN
    pickingupyourorderandanother: '____ is picking up your order and another order',
    pickuplocation: 'Pickup Location',
    order: 'Order',

    //MESSAGE_SCREEN
    typeyourmessage: 'type your message',

    //FOOD_ARRIVED_SCREEN
    foodhasarrived: 'Food has arrived',
    notifyfoodieriders: 'Notify foodieridders food has arrived',

    //ORDER_TRACKING_SCREEN
    orderinprogress: 'order in progress',
    orderinaccepted: 'order in accepted',
    orderinCancel: 'order in cancel',
    orderinDelivered: 'order in delivered',

    receipt: 'receipt',
    subtotal: 'Subtotal',
    tax: 'Tax',
    total: 'Total',
    foodierideorderreceipt: 'FoodieRide Order Receipt',
    email: 'Email',
    call: 'Call',
    help: 'Help',

    //VALIDATE_ORDER_SCREEN
    map: 'Map',
    validatetheorder: 'Validate the order',

    //Customize_Dish_Screen
    customizeyourdish: 'Customize your dish',
    addtodish: 'Add to dish',

    //My_Redemption 
    currentclovers: 'Current Clovers',
    unlock: 'Unlock',
    gold: 'Gold',
    currentcredits: 'Current Credits',
    redeemcredits: 'Redeem Credit',
    clovers: 'Clovers',
    per: 'per',
    spent: 'spent',
    searchredeemandrestaurant: 'Search Redeem + Restaurant',
    redemptionhistory: 'Redemption history',
    cloversbalance: 'Clovers Balance',

    //MY_EARNING
    orderid: 'Order id',
    superfoodie: 'Super Foodie',
    referrals: 'Referrals',
    youhaveearnedclovers: 'You  have earned ____ clovers',
    ezfoodiehashelpedyou: 'Ezfoodie has helped you save $__',

    //EARN
    learn: 'Learn',
    trysomethingnew: 'Try something new',
    nevervisitedbeforeandearn: 'Order from a spot you’ve never visited before and earn.',
    teammatevalidation: 'Teammate validation',
    earncloverswhenapproveandrejectamember: 'Earn additional clovers when you validate(approve/reject)a new member who requested to join your EZ team',
    foodierideforateammate: 'Foodieride for a teammate',
    upto4teammate1000cloversperorders: 'Up to 4 teammates(1,000 clovers)per order',
    helpusimprove: 'Help us improve',
    reportincorrectmenu: 'Report incorrect menu info and earn additional clovers',
    birthdaybonous: 'Birthday bonus',
    yourbirthdaydate: 'Tell us your birthday date and you will get a birthday bonus on your birthday every year. ',
    yourfavouritespot: 'Save your favourite spot ',
    saveatleast5favspot: 'Save at least 5 of your favourite spots(One time only) ',
    whenyouinvite10newusers: 'When you invite 10 new users who placed their first order(One time only)',
    whenyouplace30orderpermonth: 'When you place 30 order per month(once a month only) ',

    //Rewards_Redemption_Reach_Gold
    justunlockgoldstatus: 'You have just unlock gold status',

    //Redemption_Towards_Gold_Screen
    progress: 'Progress',
    clovers: 'Clovers',
    ezfoodierewards: 'Ezfoodie rewards',
    progresstooctobergold: 'Progress to October Gold',
    need6purchasestounlockezfoodies: 'You need 6 more purchases to unlock ezfoodie until the end of month',
    done: 'Done',

    //Reward_When_Refer_a_friend_screen
    referafriendandearn: 'Refer a friend and earn',
    clovers: 'Clovers',

    //Gold_status_pop_up
    youhaveunlockthe: 'You have unlocked the',
    goldstatus: 'Gold status',

    // My_Team_Screen
    viewactivity: 'View activity',
    viewrequest: 'View request',
    fourmembers: '4 members',

    //Team_activity_for_months_screen
    teamactivityfor: 'Team activity for _ ',
    numberofpeople: 'Number of People',
    totalorders: 'Total Orders',
    cloversearned: 'Clovers Earned',
    top3restaurants: 'Top 3 Restaurants ',
    changeezteamname: 'change Ez Team Name ',
    exitteam: 'Exit team',

    //Request_screen
    approvalpending: 'Approval pending ',
    approve: 'Approve',
    reject: 'Reject',

    //Settings
    foodieridesetting: 'FoodieRide Settings',
    mutefoodieride: 'Mute foodieRide',
    mutenotification: 'Mute notification ',
    options: 'Choose the frequency of being notified,options are below ',
    for1hourafter1order: 'For 1 hour after 1 order ',
    fortherestoftheday: 'For the rest of the day ',
    alwaysnotifyme: 'Always notify me',
    nevernotifyme: 'Never notify me',
    orderstatusnotification: 'Order status notification ',
    receiveimportantnotification: 'You would receive important notifications like whether your order is accepted and when to pick up your food',
    organizationandlocation: 'Organization and location',
    changeorganization: 'Change organization',
    joinneworganization: 'Join new organization ',
    leaveanyorganization: 'Leave organization',
    automaticallyofferingfoodieride: 'Automatically offering foodieRide',
    optionofswitchtosoloorder: 'every order will automatically become offer 	pickup but you are still free to switch it to solo order',
    disablefoodieride: 'Disable foodieRide',
    disconnectyoufromalltheteam: 'This will disconnect you from all the teams.',
    nolongerreceivefoodirrideorder: 'You will no longer receive foodieRide order',
    ezteammatenotabletofindyou: 'Your EZ teammates will not be able to find you in the team and join your orders',

    //Ezfoodies_Menu
    switchezteam: 'switch EZ Team',
    home: 'Home',
    pastorders: 'Past orders',
    myezteam: 'My EZ Team ',
    myfavouriterestaurant: 'My favourite restaurants',
    myrewards: 'My Rewards',
    mypaymentdetails: 'My payment Details',
    mypromocode: 'My promo code',
    referafriend: 'Refer a friend',
    accountsettings: 'Accounts settings',
    editprofile: 'Edit profile',
    ratetheapp: 'Rate the app',
    learn: 'Learn',
    help: 'Help',
    logout: 'Logout',
    deleterequest: 'Delete User Request',

    //Popup_notification
    xxxhasplacedanorder: 'xxx has placed an order from 000. Join Order ',

    //PAST_ORDER
    starters: 'Starters',
    maincourse: 'Main course',
    quickbite: 'Quick bite',
    deserts: 'Deserts',
    customize: 'Customize',

    //ACCOUNT_SETTINGS
    editprofile: 'Edit profile',
    changeapplanguage: 'Change app language ',
    notificationpreference: 'Notification preference',
    termsandconditions: 'Terms and Condition ',
    privacypolicy: 'Privacy policy',
    about: 'About',
    specficLanguage: 'Are want to sure change language',

    //EDIT_PROFILE
    changepassword: 'change password',
    submit: 'Submit',

    //CHANGE_PASSWORD
    currentpassword: 'current password ',
    newpassword: 'New password',
    reenterpassword: 'Re-enter password',
    submit: 'Submit',

    //RESET_YOUR_PASSWORD
    enterotp: 'Enter OTP',
    typeyournewpassword: 'Type your new password',
    retypeyournewpassword: 'Retype your new password',
    submit: 'Submit',

    //CHANGE_EMAIL_ADDRESS
    sentaverificationemail: 'we would sent you a verification email',
    verification: 'please verify it within 48 hours ',
    newemail: 'New email',
    confirmnewemail: 'Confirm new email',
    submit: 'Submit',

    //CHANGE_MOBILE_NUMBER
    phoneno: 'Phone no',
    sendverificationcode: 'Send verification code',

    //Mobile_number_verification_pop_up
    verificationcode: 'verification code',
    wehavesentaotp: 'We have sent a one time password(OTP) on your mobile number',
    dontgetthecode: 'Don’t get the code? Resend ',
    submit: 'Submit',

    //Bottom_Tab_Navigation
    home: 'Home',
    favourite: 'Favourite',
    rewards: 'Rewards',
    settings: 'Settings'
  },


  //TRADITIONAL CHINES
  {
    //SIGN IN
    signin: '登入',
    email: '電子信箱',
    password: '密碼',
    forgotpassword: '忘記密碼',
    signinwithfacebookgmail: '登陸使用',
    donthaveanaccount: '還沒有帳號？',
    signup: '註冊',
    singin: '登入',

    // SOCIAL SIGN IN MODAL
    enterDetails: '要完成註冊，請輸入以下詳細信息',
    enterPhone: '要完成註冊，請輸入電話號碼',
    typeMobile: '輸入手機號碼',
    typeEmail: '輸入電子郵件',

    //FORGOT PASSWORD
    forgotpassword: ' 忘記密碼',
    typeyouremail: ' 輸入電子信箱',
    submit: '確認',
    and: '和',

    //SIGN UP
    name: '姓名',
    email: ' 電子信箱',
    dob: '生日',
    password: '密碼',
    confirmpassword: '確認密碼',
    alreadyhaveanaccount: '已經有帳',

    //MOBILE_POPUP
    mobile: '手機號碼',
    accept: '我接受',
    mustagreee: '您必須同意條款和條件！',

    //OTP_OPTION
    whereshouldwesenttheotp: '將一次性確認碼送至',
    email: '電子信箱',
    sms: '手機簡訊',
    submit: '確認',

    //OTP_VERIFICATION
    verifyyouraccount: '確認帳戶',
    wehavesentaotp: '我們已將一次性密碼寄送到你的手機號碼',
    wehavesentaotpemail: '我們已經在您的電子郵件中發送了一次性密碼（OTP）',
    enterotp: '輸入一次性確認碼',
    dontgetthecode: '尚未收到確認碼？重送',
    submit: ' 確認',

    //HOME
    change: '更換吃貨幫',
    nearbyrestaurant: '附近餐廳',
    collect: ' 速集計畫 (速＋)',
    earn: '額外獎勵',
    clovers: '吃貨草',
    redeem: '快省計畫 （省＋）',
    getflatdiscount: '獲取 _折扣',
    noreslistinyourlocation: '您所在位置没有餐厅清单',

    //ORGANISATION_POPUP
    selectyourorganisation: '選擇你的團隊',
    joinorganization: '加入组织',
    typeorganizationname: '输入组织名称',
    addorganization: '添加组织',
    join: '加入',
    findorganization: '查找组织',
    searchyourorganization: '搜索您的组织',
    ifyoudontseeyourorganization: '如果看不到您的组织，请添加新组织',
    therearenoorganizationnearby: '附近没有组织',
    joined: '已加入',

    //LOCATION_WHERE_SERVICE_NOT_AVILABLE
    sorry: '抱歉',
    wedonthaveservice: '我們尚未服務您所在的地區',
    vote: '提出要求',

    //CART_SCREEN
    preorder: '預購',
    pickuptime: '預購領取時段',
    preorderallow24hourlater: '餐點預購僅限預購未來24小時之內',
    earn: '您在這筆訂單中獲取',
    clovers: '個吃貨草',
    yourorderwouldreadyin30min: ' 您的訂單將在30分鐘內準備完成',
    subtotal: '小計',
    cloverredeemed: '已兌換_個吃貨草',
    total: '總計（含稅）',
    gui: '統一編號',
    promocode: '優惠碼',
    specialinstruction: '備註(非必填)',
    choosepromocode: '選擇優惠碼',
    youhaveclovers: '你有 _吃貨草',
    redeemnow: '兌換',
    payby: '付款方式',
    // change: '更換付款方式',
    preorderpickuptime: '選擇預計領取時段',
    offerpickup: '提供順風單外帶',
    soloorder: ' 此筆為獨享訂單 ',
    teamorder: '團隊秩序',
    pickupwithteammatesoralone: '最多可幫忙4位隊友取餐, 或獨享訂單',
    ordernow: '確認下單',
    termandconditions: '我確認我已審核我的訂單並同意在下單之後我沒有修改或取消餐點的權力',

    //SEARCH_SCREEN
    searchbyrestaurantandcategories: '關鍵字與餐廳類別搜尋',

    //MY_FAVOURITE_RESTAURANT
    myfavouriterestaurant: ' 喜愛餐廳',

    //SCAN_SCREEN
    startscanning: '掃描',
    orderid: '訂單碼',
    restaurantname: '餐廳名稱',
    qrcode: '二維碼',

    //PICKUP_SCREEN
    pickingupyourorderandanother: '_正在幫你與其他隊友領取餐點',
    pickuplocation: '領餐地點',
    order: '訂單',

    //MESSAGE_SCREEN
    typeyourmessage: '輸入訊息',

    //FOOD_ARRIVED_SCREEN
    foodhasarrived: '餐點已抵達領餐地點',
    notifyfoodieriders: '通知吃貨隊友領取餐點',

    //ORDER_TRACKING_SCREEN
    orderinprogress: '餐點準備中',
    orderinaccepted: 'order in accepted',
    orderinCancel: 'order in cancel',
    orderinDelivered: 'order in delivered',
    receipt: '收據',
    subtotal: '小計',
    tax: '稅',
    total: ' 總計',
    foodierideorderreceipt: '順風單成員點餐細項',
    email: '電子信箱',
    call: '通話',
    help: '客服中心',

    //VALIDATE_ORDER_SCREEN
    map: '瀏覽地圖',
    validatetheorder: '確認訂單',

    //Customize_Dish_Screen
    customizeyourdish: '客製您的餐點',
    addtodish: ' 加入餐點 ',

    //My_Redemption 
    currentclovers: '當前吃貨草總數 ',
    unlock: '开锁',
    gold: '金',
    currentcredits: '目前回饋金',
    redeemcredits: '兌換回饋金',
    searchredeemandrestaurant: '搜尋快省計畫餐廳 ',
    redemptionhistory: '兌換歷史紀錄',
    cloversbalance: '三叶草平衡',
    clovers: '吃貨草',
    per: '每',
    spent: '花费',

    //MY_EARNING
    orderid: '訂單碼 ',
    superfoodie: '神吃貨 ',
    referrals: '邀請朋友',
    youhaveearnedclovers: '您已獲取_吃貨草',
    ezfoodiehashelpedyou: ' 簡單吃貨已為您省下_元',

    //EARN
    learn: '說明',
    trysomethingnew: '嘗試新餐廳',
    nevervisitedbeforeandearn: ' 嘗試新餐廳並獲取額外吃貨草 ',
    teammatevalidation: ' 驗證隊友 ',
    earncloverswhenapproveandrejectamember: '驗證新吃貨幫隊友並賺取額外吃貨草',
    foodierideforateammate: ' 提供隊友順風單',
    upto4teammate1000cloversperorders: '每筆可幫最多4名隊友取餐（1000吃貨草）',
    helpusimprove: '協助我們改善',
    reportincorrectmenu: '回報菜單資訊與餐廳內不同並賺取額外吃貨草 ',
    birthdaybonous: '生日福利',
    yourbirthdaydate: ' 如果您提供您的生日, 我們將於每年您的生日時贈與吃貨草',
    yourfavouritespot: '紀錄您的最愛餐廳',
    saveatleast5favspot: '紀錄至少5家最愛餐廳 （一次性回饋）',
    whenyouinvite10newusers: '邀請滿10名新會員並且完成他們的第一筆交易（一次性回饋）',
    whenyouplace30orderpermonth: '每個月下單至少30筆 (每月一次回饋)',

    //Rewards_Redemption_Reach_Gold
    justunlockgoldstatus: ' 恭喜！您已成為金草會員！',

    //Redemption_Towards_Gold_Screen
    progress: '目前進度',
    clovers: '吃貨草',
    ezfoodierewards: '目前回饋',
    progresstooctobergold: '成為10月金草會員進度',
    need6purchasestounlockezfoodies: '在這個月結束前, 您需要再下6筆訂單以成為金草會員 ',
    done: '關閉',

    //Reward_When_Refer_a_friend_screen
    referafriendandearn: '推薦好友並獲取回饋',
    clovers: '吃貨草',

    //Gold_status_pop_up
    youhaveunlockthe: '您已解锁',
    goldstatus: '黄金状态',

    // My_Team_Screen
    viewactivity: '瀏覽紀錄 ',
    viewrequest: '瀏覽加入要求',
    fourmembers: '4名成員',

    //Team_activity_for_months_screen
    teamactivityfor: ' _月吃貨幫總覽 ',
    numberofpeople: ' 隊友數量 ',
    totalorders: '總訂單數',
    cloversearned: '總吃貨草數',
    top3restaurants: '熱門餐廳 ',
    changeezteamname: '更改隊名 更改队名',
    exitteam: '離開隊伍',

    //Request_screen
    approvalpending: '等候審核',
    approve: ' 同意',
    reject: ' 拒絕 ',

    //Settings
    foodieridesetting: '順風單設定 ',
    mutefoodieride: '暫停順風單功能 ',
    mutenotification: '暫停系統通知',
    options: ' 請選擇系統通知頻率 ',
    for1hourafter1order: ' 在下過一筆訂單後, 暫停1個小時 ',
    fortherestoftheday: '整日暫停通知',
    alwaysnotifyme: '永遠通知我',
    nevernotifyme: '一律不要通知',
    orderstatusnotification: '訂單狀態通知',
    receiveimportantnotification: ' 系統將會發送重要訊息通知,例如餐廳已接收你的訂單以及通知您前往領取餐點',
    organizationandlocation: '團隊名稱與約定取餐地點',
    changeorganization: '更改團隊',
    joinneworganization: '加入新團隊 ',
    leaveanyorganization: '離開團隊',
    automaticallyofferingfoodieride: '自動提供順風單',
    optionofswitchtosoloorder: '系統會自動將預先設定為提供順風單,但你仍然可以手動改成獨享訂單',
    disablefoodieride: '關閉順風單功能',
    disconnectyoufromalltheteam: '此舉將會讓您失去享有在吃貨幫中的權力',
    nolongerreceivefoodirrideorder: ' 您將不再收取來自隊友的順風單訂單 ',
    ezteammatenotabletofindyou: '您的吃貨幫隊友將無法在隊伍中找到您或加入您的順風單',

    //Ezfoodies_Menu
    switchezteam: '更換吃貨幫 ',
    home: ' 主頁',
    pastorders: '歷史訂單',
    myezteam: '我的吃貨幫',
    myfavouriterestaurant: '最愛餐廳',
    myrewards: '我的吃貨回饋 ',
    mypaymentdetails: '付款資料',
    mypromocode: '優惠碼',
    referafriend: '推薦朋友',
    accountsettings: '帳戶設定',
    editprofile: '編輯個人資料 ',
    ratetheapp: '評論簡單吃貨 ',
    learn: '吃貨專屬回饋說明',
    help: '客服中心',
    logout: '登出',
    deleterequest: '刪除用戶請求',

    //Popup_notification
    xxxhasplacedanorder: 'xxx已在000餐廳下單.  加入點餐',

    //PAST_ORDER
    starters: '開胃菜',
    maincourse: '主菜',
    quickbite: '點心',
    deserts: '甜點',
    customize: '特製',

    //ACCOUNT_SETTINGS
    editprofile: '編輯檔案',
    changeapplanguage: ' 更改語言',
    notificationpreference: '系統通知喜好',
    termsandconditions: ' 使用者條款',
    privacypolicy: ' 隱私政策',
    about: '關於我們',
    specficLanguage: '是否想確定更改語言',

    //EDIT_PROFILE
    changepassword: '更改密碼',
    submit: '送出',

    //CHANGE_PASSWORD
    currentpassword: '目前密碼',
    newpassword: '新密碼 ',
    reenterpassword: ' 再次輸入新密碼',
    submit: '送出',

    //RESET_YOUR_PASSWORD
    enterotp: '輸入一次性確認碼',
    typeyournewpassword: '輸入新密碼',
    retypeyournewpassword: ' 再次輸入新密碼',
    submit: '送出',

    //CHANGE_EMAIL_ADDRESS
    sentaverificationemail: '我们将向您发送验证电子邮件',
    verification: '请在48小时内验证',
    verification: '',
    newemail: ' 新電子信箱 ',
    confirmnewemail: '確認新電子信箱',
    submit: '送出',

    //CHANGE_MOBILE_NUMBER
    phoneno: '手機號碼',
    sendverificationcode: '寄送確認碼 ',

    //Mobile_number_verification_pop_up
    verificationcode: '確認碼 ',
    wehavesentaotp: ' 我們已將一次性確認碼簡訊至您的手機號碼 ',
    dontgetthecode: '尚未收到確認碼？重送  ',
    submit: '確認送出',

    //Bottom_Tab_Navigation
    home: '主頁',
    favourite: '最愛',
    rewards: '獎勵',
    settings: '設定'

  },


  //SIMPLIFIED CHINES
  {
    //SIGN IN
    signin: '登入',
    email: '电子信箱',
    password: '密码',
    forgotpassword: '忘记密码',
    signinwithfacebookgmail: '登陆使用',
    donthaveanaccount: '还没有帐号？',
    signup: '注册',
    singin: '登入',

    // SOCIAL SIGN IN MODAL
    enterDetails: '要完成注册，请输入以下详细信息',
    enterPhone: '要完成注册，请输入电话号码',
    typeMobile: '输入手机号码',
    typeEmail: '输入电子邮件',


    //FORGOT PASSWORD
    forgotpassword: '忘记密码',
    typeyouremail: '输入电子信箱',
    submit: '确认',
    and: '和',

    //SIGN UP
    name: '姓名',
    email: '电子信箱',
    dob: '生日',
    password: '密码',
    confirmpassword: '确认密码',
    alreadyhaveanaccount: '已经有帐户？',

    //MOBILE_POPUP
    mobile: '手机号码',
    accept: '我接受',
    mustagreee: '您必须同意条款和条件！',

    //OTP_OPTION
    whereshouldwesenttheotp: '将一次性确认码送至',
    email: '电子信箱',
    sms: '手机简讯',
    submit: '確認',

    //OTP_VERIFICATION
    verifyyouraccount: ' 確認帳戶',
    wehavesentaotp: '我們已將一次性密碼寄送到你的手機號碼',
    wehavesentaotpemail: '我们已经在您的电子邮件中发送了一次性密码（OTP）',
    enterotp: '輸入一次性確認碼',
    dontgetthecode: '尚未收到確認碼？重送',
    submit: '确认',

    //HOME
    change: '更换吃货帮',
    nearbyrestaurant: '附近餐厅',
    collect: '速集计画 (速＋)',
    earn: '额外奖励',
    clovers: '吃货草',
    redeem: '快省計畫 （省＋）',
    getflatdiscount: '获取 _折扣',
    noreslistinyourlocation: '您所在位置没有餐厅清单',

    //ORGANISATION_POPUP
    selectyourorganisation: '选择你的团队',
    joinorganization: '加入组织',
    typeorganizationname: '输入组织名称',
    addorganization: '添加组织',
    join: '加入',
    findorganization: '查找组织',
    searchyourorganization: '搜索您的组织',
    ifyoudontseeyourorganization: '如果看不到您的组织，请添加新组织',
    therearenoorganizationnearby: '附近没有组织',
    joined: '已加入',

    //LOCATION_WHERE_SERVICE_NOT_AVILABLE
    sorry: '抱歉',
    wedonthaveservice: '我们尚未服务您所在的地区',
    vote: '提出要求',

    //CART_SCREEN
    preorder: '预购',
    pickuptime: '预购领取时段',
    preorderallow24hourlater: '餐点预购仅限预购未来24小时之内',
    earn: '您在这笔订单中获取',
    clovers: '个吃货草',
    yourorderwouldreadyin30min: '您的订单将在30分钟内准备完成',
    subtotal: '小计',
    cloverredeemed: '已兑换_个吃货草',
    total: '总计（含税）',
    gui: '统一编号',
    promocode: '优惠码',
    specialinstruction: '备注(非必填)',
    choosepromocode: '选择优惠码',
    youhaveclovers: '你有 _吃货草',
    redeemnow: '兑换',
    payby: '付款方式',
    // change: ' 更换付款方式',
    preorderpickuptime: '选择预计领取时段',
    offerpickup: '提供顺风单外带',
    soloorder: '此笔为独享订单',
    teamorder: ' 团队秩序',
    pickupwithteammatesoralone: '最多可帮忙4位队友取餐, 或独享订单',
    ordernow: '確認下單',
    termandconditions: '我确认我已审核我的订单并同意在下单之后我没有修改或取消餐点的权力',

    //SEARCH_SCREEN
    searchbyrestaurantandcategories: '关键字与餐厅类别搜寻',

    //MY_FAVOURITE_RESTAURANT
    myfavouriterestaurant: '喜爱餐厅',

    //SCAN_SCREEN
    startscanning: '扫描',
    orderid: '订单码',
    restaurantname: '餐厅名称',
    qrcode: '二维码',

    //PICKUP_SCREEN
    pickingupyourorderandanother: '_正在帮你与其他队友领取餐点',
    pickuplocation: '领餐地点',
    order: '订单',

    //MESSAGE_SCREEN
    typeyourmessage: '输入讯息',

    //FOOD_ARRIVED_SCREEN
    foodhasarrived: '餐点已抵达领餐地点',
    notifyfoodieriders: '通知吃货队友领取餐点',

    //ORDER_TRACKING_SCREEN
    orderinprogress: '餐点准备中',
    orderinaccepted: 'order in accepted',
    orderinCancel: 'order in cancel',
    orderinDelivered: 'order in delivered',
    receipt: ' 收据',
    subtotal: ' 小计',
    tax: '税',
    total: '总计',
    foodierideorderreceipt: '顺丰单成员点餐细项',
    email: '电子信箱',
    call: ' 通话',
    help: '客服中心',

    //VALIDATE_ORDER_SCREEN
    map: '浏览地图',
    validatetheorder: '确认订单',

    //Customize_Dish_Screen
    customizeyourdish: '客制您的餐点',
    addtodish: ' 加入餐点',

    //My_Redemption 
    currentclovers: '当前吃货草总数',
    unlock: '开锁',
    gold: '金',
    currentcredits: '目前回馈金',
    redeemcredits: '兑换回馈金',
    clovers: '吃貨草',
    per: '每',
    spent: '花费',
    searchredeemandrestaurant: '搜寻快省计画餐厅',
    redemptionhistory: '兑换历史纪录',
    cloversbalance: '三叶草平衡',

    //MY_EARNING
    orderid: '订单码',
    superfoodie: '神吃货',
    referrals: '邀请朋友',
    youhaveearnedclovers: '您已获取_吃货草',
    ezfoodiehashelpedyou: '简单吃货已为您省下_元',

    //EARN
    learn: '说明',
    trysomethingnew: '尝试新餐厅',
    nevervisitedbeforeandearn: '尝试新餐厅并获取额外吃货草',
    teammatevalidation: ' 验证队友',
    earncloverswhenapproveandrejectamember: ' 验证新吃货帮队友并赚取额外吃货草',
    foodierideforateammate: '提供队友顺风单',
    upto4teammate1000cloversperorders: '每笔可帮最多4名队友取餐（1000吃货草）',
    helpusimprove: ' 协助我们改善',
    reportincorrectmenu: ' 回报菜单资讯与餐厅内不同并赚取额外吃货草',
    birthdaybonous: '生日福利',
    yourbirthdaydate: '如果您提供您的生日, 我们将于每年您的生日时赠与吃货草',
    yourfavouritespot: '纪录您的最爱餐厅',
    saveatleast5favspot: '纪录至少5家最爱餐厅 （一次性回馈）',
    whenyouinvite10newusers: '邀请满10名新会员并且完成他们的第一笔交易（一次性回馈）',
    whenyouplace30orderpermonth: '每个月下单至少30笔 (每月一次回馈)',

    //Rewards_Redemption_Reach_Gold
    justunlockgoldstatus: ' 恭喜！您已成为金草会员！',

    //Redemption_Towards_Gold_Screen
    progress: '目前进度',
    clovers: '吃货草',
    ezfoodierewards: '目前回馈',
    progresstooctobergold: '成为10月金草会员进度',
    need6purchasestounlockezfoodies: '在这个月结束前, 您需要再下6笔订单以成为金草会员',
    done: '关闭',

    //Reward_When_Refer_a_friend_screen
    referafriendandearn: '推荐好友并获取回馈',
    clovers: '吃货草',

    //Gold_status_pop_up
    youhaveunlockthe: '您已解锁',
    goldstatus: '黄金状态',

    // My_Team_Screen
    viewactivity: ' 浏览纪录',
    viewrequest: ' 浏览加入要求',
    fourmembers: '4名成员',

    //Team_activity_for_months_screen
    teamactivityfor: ' _月吃货帮总览 ',
    numberofpeople: ' 队友数量',
    totalorders: '总订单数',
    cloversearned: '总吃货草数',
    top3restaurants: '热门餐厅',
    changeezteamname: '更改隊名 更改队名',
    exitteam: '离开队伍',

    //Request_screen
    approvalpending: '等候审核',
    approve: '同意',
    reject: ' 拒绝',

    //Settings
    foodieridesetting: '顺风单设定',
    mutefoodieride: ' 暂停顺风单功能',
    mutenotification: ' 暂停系统通知',
    options: '请选择系统通知频率',
    for1hourafter1order: '在下过一笔订单后, 暂停1个小时',
    fortherestoftheday: ' 整日暂停通知',
    alwaysnotifyme: '永远通知我',
    nevernotifyme: '一律不要通知',
    orderstatusnotification: ' 订单状态通知',
    receiveimportantnotification: '系统将会发送重要讯息通知,例如餐厅已接收你的订单以及通知您前往领取餐点',
    organizationandlocation: '团队名称与约定取餐地点',
    changeorganization: '更改团队',
    joinneworganization: '加入新团队',
    leaveanyorganization: '离开团队',
    automaticallyofferingfoodieride: '自动提供顺风单',
    optionofswitchtosoloorder: '系统会自动将预先设定为提供顺风单,但你仍然可以手动改成独享订单',
    disablefoodieride: '关闭顺风单功能',
    disconnectyoufromalltheteam: '此举将会让您失去享有在吃货帮中的权力',
    nolongerreceivefoodirrideorder: ' 您将不再收取来自队友的顺风单订单 ',
    ezteammatenotabletofindyou: '您的吃货帮队友将无法在队伍中找到您或加入您的顺风单',

    //Ezfoodies_Menu
    switchezteam: '更换吃货帮',
    home: ' 主页',
    pastorders: '历史订单',
    myezteam: '我的吃货帮',
    myfavouriterestaurant: '最爱餐厅',
    myrewards: '我的吃货回馈',
    mypaymentdetails: ' 付款资料',
    mypromocode: '优惠码',
    referafriend: '推荐朋友',
    accountsettings: '帐户设定',
    editprofile: ' 编辑个人资料',
    ratetheapp: ' 评论简单吃货',
    learn: '吃货专属回馈说明',
    help: '客服中心',
    logout: '登出',
    deleterequest: '删除用户请求',

    //Popup_notification
    xxxhasplacedanorder: 'xxx已在000餐厅下单. 加入点餐',

    //PAST_ORDER
    starters: ' 开胃菜',
    maincourse: '主菜',
    quickbite: ' 点心',
    deserts: '甜点',
    customize: ' 特制',

    //ACCOUNT_SETTINGS
    editprofile: '编辑档案',
    changeapplanguage: '更改语言',
    notificationpreference: '系统通知喜好',
    termsandconditions: '使用者条款',
    privacypolicy: '隐私政策',
    about: '关于我们',
    specficLanguage: '是否要确定更改语言',

    //EDIT_PROFILE
    changepassword: '更改密码',
    submit: ' 送出',

    //CHANGE_PASSWORD
    currentpassword: '目前密码',
    newpassword: ' 新密码',
    reenterpassword: ' 再次输入新密码',
    submit: '送出',

    //RESET_YOUR_PASSWORD
    enterotp: '输入一次性确认码',
    typeyournewpassword: ' 输入新密码',
    retypeyournewpassword: '再次输入新密码',
    submit: '送出',

    //CHANGE_EMAIL_ADDRESS
    sentaverificationemail: '我们将向您发送验证电子邮件',
    verification: '请在48小时内验证',
    newemail: '  新电子信箱 ',
    confirmnewemail: ' 确认新电子信箱',
    submit: ' 送出',

    //CHANGE_MOBILE_NUMBER
    phoneno: '手机号码',
    sendverificationcode: '寄送确认码',

    ////Mobile_number_verification_pop_up
    verificationcode: '确认码',
    wehavesentaotp: '我们已将一次性确认码简讯至您的手机号码 ',
    dontgetthecode: ' 尚未收到确认码？重送 ',
    submit: '确认送出',

    //Bottom_Tab_Navigation
    home: '主页',
    favourite: '最爱',
    rewards: '奖励',
    settings: '设定'
  },
]