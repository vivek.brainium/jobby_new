import { SAVE_USER } from './../actions/types'

export default function(state = {}, action) {
    console.log(state, action);
    switch (action.type) {
      case SAVE_USER:
        return { user: action.payload };
      default:
        return state;
    }
  }