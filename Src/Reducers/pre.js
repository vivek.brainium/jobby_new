import * as union_all_get_api from '../actions/union.action';

const unionReducer = (
    state = {
        union_update_resp: null,
        union_update_status: "",
    },
    action
) => {

    switch (action.type) {

        /*************for create union****************/
        case union_all_get_api.UNION_REQUEST:
            return {
                status: action.status
            }
        case union_all_get_api.UNION_SUCCESS:
            return {
                status: action.status,
            }

        case union_all_get_api.UNION_FAILURE:
            return {
                status: action.status,
            }
        /**********************************************/

        default:
            return state;

    }
}

export default unionReducer;
