import React, { useEffect } from 'react';
import {
    StyleSheet,
    View,
    StatusBar,
    Platform,
} from 'react-native';

const MyStatusBar = ({ backgroundColor, ...props }) => {
    return (
        <View style={[styles.statusBar, { backgroundColor }]}>
            <StatusBar translucent backgroundColor={backgroundColor} {...props} />
        </View>
    )
}
export default MyStatusBar;

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 25 : StatusBar.currentHeight;

const styles = StyleSheet.create({    
    statusBar: {
        height: STATUSBAR_HEIGHT,
    }
});