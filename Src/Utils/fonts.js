const Font = {
  micro_size: 12,
  small_size: 16,
  medium_size: 18,
  large_size: 20,
  extra_large:22
};

export default Font;