import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import FontFamily from '../../Utils/FontFamily';


const ContactList = (props) => {
  return (
    <View style={{flex:1}}>
    <View style={styles.container}>
      <View style={styles.profileContainer}>
        <View style={styles.ImageContainer}>
          <Image source={props.image} style={styles.profileImage} />
        </View>
        <View style={styles.profileDescriptionContainer}>
          <Text style={styles.name}>{props.name}</Text>
          <Text style={styles.email}>{props.email}</Text>
        </View>
      </View>
      <TouchableOpacity>
      <Image source={require('../../Images/Share/message.png')} style={{height:30,width:30,marginTop:7}} resizeMode="contain"/>
      </TouchableOpacity>
    </View>
    <View style={{borderBottomWidth:.55 , width:"75%",alignSelf:"flex-end", borderBottomColor:"#9a9a9a",marginBottom:25}}></View>
    </View>
  );
}


export default ContactList;

const styles = StyleSheet.create({
  container: {
    //flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    width: "100%",
    paddingLeft: 30,
    paddingBottom: 10,
    paddingRight: 30
  },
  profileContainer: {
    flexDirection: "row",

  },
  ImageContainer: {
    height: 50,
    width: 50,
    borderRadius: 50,
    marginRight: 10
  },
  profileImage: {
    height: "100%",
    width: "100%",
    borderRadius: 50,
  },
  profileDescriptionContainer: {
   
  },
  name: {
    fontFamily: FontFamily.Poppins_Medium,
   fontSize: 14,
   color:"#1c1c1c"
  },
  email: {
    fontFamily: FontFamily.Poppins_Medium,
   fontSize: 13,
   color:"#4b4b4b"
  }
})