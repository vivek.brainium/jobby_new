import React, { useEffect, useState, useReducer } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ScrollView, Platform, Dimensions } from 'react-native';
import FontFamily from '../../Utils/FontFamily';
import { RadioButton } from 'react-native-paper';
import ButtonComponent from '../Button/loginButton';
import FontSize from '../../Utils/fonts';
import HeaderComponent from '../Header';
import Colors from '../../Utils/Colors';
import FloatingLabelInput from '../FlotingTextInput';


  const screenHeight = Dimensions.get('window').height;
  const screenwidth = Dimensions.get('window').width;

const Salary = (props) => {
  const [current, setcurrent] = useState('');
  const [expected, setexpected] = useState('');


  const saveAndContinue = (e) => {
    e.preventDefault()
    props.nextStep()
  }
  const back = (e) => {
    e.preventDefault();
    props.prevStep();
  }

  const renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }


  return (
    <View style={{ flex: 1 }}>
     
      <ScrollView style={{ flex: 1 }}>
        <Text style={{ textAlign: "center", color: "#182e4d", paddingVertical: "5%", fontSize: FontSize.large_size, fontFamily: FontFamily.Poppins_Medium }}>Enter Salary Details</Text>
        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Current Salary"
            value={current}
            onChangeText={(val) => setcurrent(val)}
            keyboardType={"number-pad"}
            placeholder="Entry Current Salary"
          />
          {/* <Text style={styles.title}>Current Salary</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="Current Salary"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setcurrent(val)}
            value={current}
            style={styles.mytextinput}
            keyboardType={"number-pad"}
          /> */}
        </View>

        <View style={styles.repeatContainer}>
          {/* <Text style={styles.title}>Expected Salary</Text> */}
          {/* <TextInput
            underlineColorAndroid="transparent"
            placeholder="Entry Expected Salary"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setexpected(val)}
            value={expected}
            style={styles.mytextinput}
            keyboardType={"number-pad"}
          /> */}
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Expected Salary"
            value={expected}
            onChangeText={(val) => setexpected(val)}
            placeholder="Entry Expected Salary"
          />
        </View>

        {/* <View style={{ flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "space-between", marginBottom: 20 }}>
          <ButtonComponent
            buttonName="Back"
            backgroundColor1="#1a2246"
            height={40}
            width="45%"
            onClick={back}
          />
          <ButtonComponent
            buttonName="Save"
            backgroundColor1="#1a2246"
            height={40}
            width="45%"
            onClick={saveAndContinue}
          />
        </View> */}
      </ScrollView>
    </View>
  );
}


export default Salary;

const styles = StyleSheet.create({
  repeatContainer: {
    width: "85%", alignItems: "flex-start", alignSelf: "center", marginBottom: 15
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, borderRadius: 10, height: 40, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },
  title:{
    fontSize:FontSize.small_size,
    fontFamily:FontFamily.Poppins_Medium,
    marginBottom:5,
    paddingStart:5
  }
})