import React, { useEffect, useState, useReducer } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ScrollView, Platform, Dimensions, KeyboardAvoidingView } from 'react-native';
import FontFamily from '../../Utils/FontFamily';
import ButtonComponent from '../Button/loginButton';
// import { RadioButton } from 'react-native-paper';
import FontSize from '../../Utils/fonts';
import HeaderComponent from '../Header';
import Colors from '../../Utils/Colors';
import FloatingLabelInput from '../../Components/FlotingTextInput';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const PersonalInfo = (props) => {
  const [company, setcompany] = useState('');
  const [country, setcountry] = useState('');
  const [city, setcity] = useState('');
  const [dob, setdob] = useState('');
  const [nationality, setnationality] = useState('');
  const [rcountry, setrcountry] = useState('');
  const [visa, setvisa] = useState('');
  const [material, setmaterial] = useState('');
  const [driving, setdriving] = useState('');
  const [value, setvalue] = useState(0);

  const saveAndContinue = (e) => {
    e.preventDefault()
    props.nextStep()
  }

  const renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }

  var radio_props = [
    {label: 'Yes', value: 'yes' },
    {label: 'No', value: 'no' },
  ];

  return (
    <View style={{flex:1}}>
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="position" keyboardVerticalOffset={-150} >
      <ScrollView>
        <Text style={{ textAlign: "center", paddingVertical: "5%", fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Medium }}>Enter Your Profile Information</Text>
        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.18}
            label="Company Name"
            value={company}
            onChangeText={(val) => setcompany(val)}
            placeholder="Enter Company Name"
          />
          
        </View>
        <View style={styles.repeatContainer}>
          {/* <Text style={styles.title}>Country</Text> */}
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.18}
            label="Country Name"
            value={country}
            onChangeText={(val) => setcountry(val)}
            placeholder="Enter Country Name"
          />
      
        </View>
        <View style={styles.repeatContainer}>
          {/* <Text style={styles.title}>City</Text> */}
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.18}
            label="City Name"
            value={city}
            onChangeText={(val) => setcity(val)}
            placeholder="Enter City Name"
          />
        </View>
        <View style={styles.repeatContainer}>
          {/* <Text style={styles.title}>Date Of Birth</Text> */}
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.18}
            label="Date of Birth"
            value={dob}
            onChangeText={(val) => setdob(val)}
            placeholder="Your Date of birth"
          />
        </View>

        <View style={styles.repeatContainer}>
          {/* <Text style={styles.title}>Nationality</Text> */}
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.18}
            label="Nationality"
            value={nationality}
            onChangeText={(val) => setnationality(val)}
            placeholder="Enter Your Nationality"
          />
          
        </View>

        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.18}
            label="Residence Country"
            value={rcountry}
            onChangeText={(val) => setrcountry(val)}
            placeholder="Enter Residence Country"
          />
        </View>
        
        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Upload Image</Text>
          <ButtonComponent
            buttonName="Upload image"
            backgroundColor1="#1a2246"
            height={40}
            width="50%"
          // onClick={() => this.props.navigation.navigate("PostJob")}
          />
        </View>
        <View style={[styles.repeatContainer, {marginBottom:0}]}>
          <Text style={styles.title}>Visa Status</Text>
          <View style={{ flexDirection: "row", flexWrap: "wrap", paddingStart:5 }}>
            <RadioForm
              radio_props={radio_props}
              initial={0}
              formHorizontal={true}
              labelHorizontal={true}
              buttonColor={'#45cfd2'}
              selectedButtonColor={'#45cfd2'}
              animation={false}
              onPress={(value) => setvalue(value)}
              labelStyle={{marginRight: 10, fontFamily:FontFamily.Poppins_Regular }}
              buttonSize={10}
            />
          </View>
        </View>
        <View style={[styles.repeatContainer, {marginBottom:0}]}>
          <Text style={styles.title}>Material Status</Text>
          <View style={{ flexDirection: "row", flexWrap: "wrap", paddingStart:5 }}>
            <RadioForm
              radio_props={radio_props}
              initial={0}
              formHorizontal={true}
              labelHorizontal={true}
              buttonColor={'#45cfd2'}
              selectedButtonColor={'#45cfd2'}
              animation={false}
              onPress={(value) => {setvalue(value)}}
              labelStyle={{marginRight: 10, fontFamily:FontFamily.Poppins_Regular }}
              buttonSize={10}
            />
            
          </View>
        </View>

        <View style={[styles.repeatContainer, {marginBottom:0}]}>
          <Text style={styles.title}>Driving Licence</Text>
          <View style={{ flexDirection: "row", flexWrap: "wrap", paddingStart:5 }}>
            <RadioForm
              radio_props={radio_props}
              initial={0}
              formHorizontal={true}
              labelHorizontal={true}
              buttonColor={'#45cfd2'}
              selectedButtonColor={'#45cfd2'}
              animation={false}
              onPress={(value) => {setvalue(value)}}
              labelStyle={{marginRight: 10, fontFamily:FontFamily.Poppins_Regular }}
              buttonSize={10}
            />
          </View>
        </View>
        
      </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
}


export default PersonalInfo;

const styles = StyleSheet.create({
  repeatContainer: {
    width: "85%", alignItems: "flex-start", alignSelf: "center", marginBottom: 20
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, borderRadius: 10, height: 40, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },

  radioCircle: {
    padding: 9, 
    borderWidth: 2, 
    borderColor: Colors.Nero, 
    borderRadius: 100, 
    position: 'absolute'
  },
  title:{
    fontSize:FontSize.small_size,
    fontFamily:FontFamily.Poppins_Medium,
    marginBottom:10,
    paddingStart:5
  }
})