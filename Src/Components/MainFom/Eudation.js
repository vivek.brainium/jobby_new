import React, { useEffect, useState, useReducer } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ScrollView, Picker, Dimensions, KeyboardAvoidingView } from 'react-native';
import ButtonComponent from '../Button/loginButton';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
import HeaderComponent from '../Header';
import RNPickerSelect from 'react-native-picker-select';
import FloatingLabelInput from '../FlotingTextInput';


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const Eudation = (props) => {
  const [country, setcountry] = useState('');
  const [degree, setdegree] = useState('');
  const [university, setuniversity] = useState('');
  const [study, setstudy] = useState('');
  const [gradution, setgradution] = useState('');
  const [grade, setgrade] = useState('');
  const [description, setdescription] = useState('');

  const saveAndContinue = (e) => {
    e.preventDefault()
    props.nextStep()
  }
  const back = (e) => {
    e.preventDefault();
    props.prevStep();
  }

  return (
    <View style={{flex:1}}>
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" >
      <ScrollView style={{ flex: 1 }}>
        <Text style={{ textAlign: "center", color: "#182e4d", paddingVertical: "5%", fontSize: FontSize.large_size, fontFamily: FontFamily.Poppins_Medium }}>Enter Edutation Details</Text>
        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Degree</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%",  height: 32, justifyContent: "center", backgroundColor: "#fff" }}>
          <RNPickerSelect
             placeholder={{
              label: 'Select Degree...',
              value: null,
            }}
            placeholderTextColor="#999"
            onValueChange={(value) => setdegree(value)}
            style={pickerSelectStyles}
            items={[
                { label: 'BSC', value: 'bsc' },
                { label: 'B.Com', value: 'bcom' },
                { label: 'MSC', value: 'msc' },
                { label: 'M.Com', value: 'mcom' },
                { label: 'B.Tech', value: 'b.tech' },
                { label: 'M.Tech', value: 'm.tech' },
            ]}
        />
            {/* <Picker
              selectedValue={degree}
              onValueChange={(itemValue, itemPosition) =>
                setdegree(itemValue)}>
              <Picker.Item label="Select Degree" value="" />
              <Picker.Item label="BSC" value="rc_dev" />
              <Picker.Item label="B.com" value="wb_dev" />
              <Picker.Item label="MSC" value="dotnet" />
              <Picker.Item label="M.Com" value="wb_designer" />
              <Picker.Item label="B.Tech" value="b_tech" />
              <Picker.Item label="M.Tech" value="m_tech" />
            </Picker> */}
          </View>
        </View>

        <View style={styles.repeatContainer}>
            <FloatingLabelInput
              textInputHeight={35}
              textInputWidth={screenwidth / 1.18}
              label="University Name"
              value={university}
              onChangeText={(val) => setuniversity(val)}
              placeholder="Entry University Name"
            />
          {/* <Text style={styles.title}>University</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="Entry University Name"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setuniversity(val)}
            value={university}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={styles.repeatContainer}>
          <FloatingLabelInput
              textInputHeight={35}
              textInputWidth={screenwidth / 1.18}
              label="Country Name"
              value={country}
              onChangeText={(val) => setcountry(val)}
              placeholder="Entry Company Name"
            />
          {/* <Text style={styles.title}>Country</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="Entry Country Name"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setcountry(val)}
            value={country}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Field of Study (Major)"
            value={study}
            onChangeText={(val) => setstudy(val)}
            placeholder="Ex. Buisness Administrator"
          />
          {/* <Text style={styles.title}>Field of Study (Major)</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="Field of Study"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setstudy(val)}
            value={study}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Enter Graduation Date"
            value={gradution}
            onChangeText={(val) => setgradution(val)}
            placeholder="Choose Date"
          />
          {/* <Text style={styles.title}>Graduation Date</Text> */}
          {/* <TextInput
            underlineColorAndroid="transparent"
            placeholder="Enter Graduation Date"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setgradution(val)}
            value={gradution}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Grade</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 32, justifyContent: "center", backgroundColor: "#fff" }}>
          <RNPickerSelect
            placeholder={{
              label: 'Grade System...',
              value: null,
            }}
            placeholderTextColor="#999"
            onValueChange={(value) => setgrade(value)}
            style={pickerSelectStyles}
            items={[
                { label: 'A+', value: 'bsc' },
                { label: 'AA', value: 'bcom' },
                { label: 'B+', value: 'msc' },
                { label: 'C', value: 'mcom' },
            ]}
        />
            {/* <Picker
              selectedValue={grade}
              onValueChange={(itemValue, itemPosition) =>
                setgrade(itemValue)}>
              <Picker.Item label="Select Grade" value="" />
              <Picker.Item label="A+" value="a" />
              <Picker.Item label="AA" value="aa" />
              <Picker.Item label="B+" value="b" />
              <Picker.Item label="C" value="c" />
            </Picker> */}
          </View>
        </View>

        <View style={[styles.repeatContainer]}>
          <Text style={[styles.title, {marginBottom:10}]}>Description</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="List of Projects, achievements of actvities you completed during your studies."
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setdescription(val)}
            multiline={true}
            value={description}
            style={[styles.mytextinput, { height: 100 }]}
          />
        </View>
      </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
}


export default Eudation;

const styles = StyleSheet.create({
  repeatContainer: {
    width: "85%", alignItems: "flex-start", alignSelf: "center", marginBottom: 15
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, borderRadius: 0, height: 40, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },
  title:{
    fontSize:FontSize.small_size,
    fontFamily:FontFamily.Poppins_Medium
    // marginBottom:5,
    // paddingStart:5
  }
})

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 15,
    paddingVertical: 0,
    paddingHorizontal: 0,
    // borderWidth: 1,
    // borderColor: 'gray',
    // borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 0,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});