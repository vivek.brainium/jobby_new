import React, { useEffect, useState, useReducer } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ScrollView, Picker, Platform, Dimensions, Switch, KeyboardAvoidingView } from 'react-native';
import FontFamily from '../../Utils/FontFamily';
import { RadioButton } from 'react-native-paper';
import ButtonComponent from '../Button/loginButton';
import FontSize from '../../Utils/fonts';
import HeaderComponent from '../Header';
import Colors from '../../Utils/Colors';
import RNPickerSelect from 'react-native-picker-select';
import FloatingLabelInput from '../FlotingTextInput';



const Experience = (props) => {
  const [title, settitle] = useState('');
  const [companyname, setcompanyname] = useState('');
  const [jobrole, setjobrole] = useState('');
  const [description, setdescription] = useState('');
  const [country, setcountry] = useState('');
  const [city, setcity] = useState('');
  const [startdate, setStartdate] = useState('');
  const [enddate, setEnddate] = useState('');
  const [presentwork, setpresentwork] = useState('');
  const [switch1Value, setswitch] = useState(false);
  

  
  const screenHeight = Dimensions.get('window').height;
  const screenwidth = Dimensions.get('window').width;

  const saveAndContinue = (e) => {
    e.preventDefault()
    props.nextStep()
  }
  const back = (e) => {
    e.preventDefault();
    props.prevStep();
  }

  const renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }

  const toggleSwitch1 = (value) => {
    setswitch(value)
    // this.setState({switch1Value: value})
    console.log('Switch 1 is: ' + value)
  }

  return (
    <View style={{flex:1}}>
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={{ textAlign: "center", paddingVertical: "5%", fontSize: FontSize.large_size, fontFamily: FontFamily.Poppins_Medium,color: "#182e4d" }}>Enter Job Experience</Text>
        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Job Title"
            value={title}
            onChangeText={(val) => settitle(val)}
            placeholder="Ex. Project Manager"
          />
          
        </View>
        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Field</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 32, justifyContent: "center", backgroundColor: "#fff" }}>
          <RNPickerSelect
             placeholder={{
              label: 'Select Job Role...',
              value: null,
            }}
            placeholderTextColor="#999"
            onValueChange={(value) => setjobrole(value)}
            style={pickerSelectStyles}
            items={[
                { label: 'React Native Developer', value: 'rc_dev' },
                { label: 'Web Developer', value: 'wb_dev' },
                { label: '.Net Developer', value: 'railway' },
                { label: 'Web Designer', value: 'wb_designer' },
            ]}
            />
            
          </View>
        </View>


        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Company Name"
            value={companyname}
            onChangeText={(val) => setcompanyname(val)}
            placeholder="Ex. Brainium Information Technology"
          />
          
        </View>

        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Job Location (Country)"
            value={country}
            onChangeText={(val) => setcountry(val)}
            placeholder="Enter Job Location"
          />
          
        </View>

        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Job Location (City)"
            value={country}
            onChangeText={(val) => setcity(val)}
            placeholder="Enter City Name"
          />
        </View>

        <View style={[styles.repeatContainer]}>
          {/* <Text style={styles.title}>Enter Start Date &amp; End Date</Text> */}
          <View style={{ flexDirection: "row", justifyContent: "space-between", width: "100%" }}>
            
            <FloatingLabelInput
              textInputHeight={35}
              textInputWidth={screenwidth / 2.60}
              label="Start Date"
              value={startdate}
              onChangeText={(val) => setStartdate(val)}
              placeholder="Enter Start Date"
            />
              
              { switch1Value == false ?
                <FloatingLabelInput
                textInputHeight={35}
                textInputWidth={screenwidth / 2.60}
                label="End Date"
                value={enddate}
                onChangeText={(val) => setEnddate(val)}
                placeholder="Enter End Date"
              />
              :null
              }
                        
          </View>
        </View>

        <View style={styles.repeatContainer}>
          <View style={{ flexDirection: "row", flexWrap: "wrap", alignItems:"center" }}>
          <Text style={{ fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Regular, marginRight:10 }}>I still work here</Text>
            <Switch
              onValueChange = {toggleSwitch1}
              value = {switch1Value}/>
          </View>
        </View>

        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Description</Text>
         
           <TextInput
            underlineColorAndroid="transparent"
            placeholder="Enter your job project name, work environment, office cultute and other things."
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setdescription(val)}
            multiline={true}
            value={description}
            style={[styles.mytextinput, { height: 80 }]}
          /> 
        </View>
      </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
}


export default Experience;

const styles = StyleSheet.create({
  repeatContainer: {
    width: "85%", alignItems: "flex-start", alignSelf: "center", marginBottom: 15
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, height: 40, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },
  radioCircle: {
    padding: 9, 
    borderWidth: 2, 
    borderColor: Colors.Nero, 
    borderRadius: 100, 
    position: 'absolute'
  },
  title:{
    fontSize:FontSize.medium_size,
    fontFamily:FontFamily.Poppins_Medium
  }
})

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 15,
    paddingVertical: 0,
    paddingHorizontal: 0,
    // borderWidth: 1,
    // borderColor: 'gray',
    // borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 15,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});