import React, { useEffect, useState, useReducer } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ScrollView, Dimensions, KeyboardAvoidingView } from 'react-native';
import FontFamily from '../../Utils/FontFamily';
import { RadioButton } from 'react-native-paper';
import ButtonComponent from '../Button/loginButton';
import FontSize from '../../Utils/fonts';
import CountryPicker, { getAllCountries, getCallingCode } from 'react-native-country-picker-modal';
import HeaderComponent from '../Header';
import FloatingLabelInput from '../FlotingTextInput';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const ContactInfo = (props) => {
  console.log(props, "props");

  const [email, setemail] = useState('');
  const [phone, setphone] = useState('');
  const [country, setCountry] = useState(null)
  const [withFlag, setWithFlag] = useState(true)
  const [withCallingCode, setWithCallingCode] = useState(false)
  const [countryCode, setCountryCode] = useState('FR');



  const onSelect = (country) => {
    setCountryCode(country.cca2)
    setCountry(country)
  }

  const saveAndContinue = (e) => {
    e.preventDefault()
    props.nextStep()
  }

  const back = (e) => {
    e.preventDefault();
    props.navigation.goBack();
  }

  console.log(withCallingCode, "withCallingCode");

  return (
    <View style={{ flex: 1 }}>

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ alignItems: "center" }}>
          <Text style={{ textAlign: "center", paddingVertical: "5%", fontSize: FontSize.large_size, fontFamily: FontFamily.Poppins_Medium, color: "#182e4d" }}>Enter Your Contact Information</Text>
        </View>
        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Enter Email"
            value={email}
            onChangeText={(val) => setemail(val)}
            placeholder="Entry Email Id"
          />

        </View>

        <View style={styles.repeatContainer}>
          <View style={{ flexDirection: "row" }}>
            <View style={[styles.mytextinput, { width: 50, justifyContent: "center", marginRight: 10 }]}>
              <CountryPicker
                {...{
                  countryCode,
                  withFlag,
                  withCallingCode,
                  onSelect
                }}
              />

            </View>

            <FloatingLabelInput
              textInputHeight={35}
              textInputWidth={screenwidth / 1.42}
              label="Entry Phone No"
              value={email}
              onChangeText={(val) => setphone(val)}
              placeholder="Entry Phone No"
            />

          </View>
        </View>
      </ScrollView>
    </View>
  );
}


export default ContactInfo;

const styles = StyleSheet.create({
  repeatContainer: {
    width: "85%", alignItems: "flex-start", alignSelf: "center", marginBottom: 15
  },
  mytextinput: {
    borderBottomColor: "#999", borderBottomWidth: 1, height: 52, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },
  title: {
    marginBottom: 5,
    paddingStart: 5
  }
})