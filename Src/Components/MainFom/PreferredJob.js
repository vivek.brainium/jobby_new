import React, { useEffect, useState, useReducer } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ScrollView, Picker, Platform, Dimensions, KeyboardAvoidingView } from 'react-native';
import FontFamily from '../../Utils/FontFamily';
// import { RadioButton } from 'react-native-paper';
import ButtonComponent from '../Button/loginButton';
import FontSize from '../../Utils/fonts';
import HeaderComponent from '../Header';
import Colors from '../../Utils/Colors';
import RNPickerSelect from 'react-native-picker-select';
import FloatingLabelInput from '../FlotingTextInput';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const PreferredJob = (props) => {
  const [company, setcompany] = useState('');
  const [jobrole, setjobrole] = useState('');
  const [joblabel, setjoblabel] = useState('');
  const [joblocation, setlocation] = useState('');
  const [description, setdescription] = useState('');
  const [industry, setindustry] = useState('');
  const [employment, setemployment] = useState('');
  const [mounthsalary, setmonthalysalary] = useState('');
  const [notice, setnotice] = useState('');
  const [value, setvalue] = useState(0);


  const saveAndContinue = (e) => {
    e.preventDefault()
    props.nextStep()
  }
  const back = (e) => {
    e.preventDefault();
    props.prevStep();
  }

  const renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }

  var radio_props = [
    {label: 'Fulltime', value: 0 },
    {label: 'Part-time', value: 1 },
    {label: 'Temporary', value: 2 },
  ];

  return (
    <View style={{flex:1}}>
       <KeyboardAvoidingView style={{ flex: 1 }} behavior="position" >
        <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={{ textAlign: "center", paddingVertical: "5%", fontSize: FontSize.large_size, fontFamily: FontFamily.Poppins_Medium,color: "#182e4d" }}>Enter Preferred Job</Text>
        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Job Title"
            value={company}
            onChangeText={(val) => setcompany(val)}
            placeholder="Ex. Digital Marketing Manager"
          />
         
        </View>
        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Preferred Job Role</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 32, justifyContent: "center", }}>
          <RNPickerSelect
            onValueChange={(value) => setjobrole(value)}
            placeholder={{
              label: 'Ex. React Native Developer',
              value: null,
            }}
            placeholderTextColor="#999"
            style={pickerSelectStyles}
            items={[
                { label: 'React Native Developer', value: 'rc_dev' },
                { label: 'Web Developer', value: 'wb_dev' },
                { label: '.Net Developer', value: 'railway' },
            ]}
            />
          </View>
        </View>
          

        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Job Level</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 32, justifyContent: "center", }}>
          <RNPickerSelect
            onValueChange={(value) => setjoblabel(value)}
            placeholder={{
              label: 'Select Job Level...',
              value: null,
            }}
            placeholderTextColor="#999"
            style={pickerSelectStyles}
            items={[
                { label: 'Executive', value: 'executive' },
                { label: 'Manager', value: 'manager' },
                { label: 'Supervisor', value: 'supervisor' },
                { label: 'officer', value: 'officer' },
                { label: 'etc', value: 'etc' },
            ]}
            />
          </View>
        </View>
        

        <View style={styles.repeatContainer}>
          {/* <Text style={styles.title}>Job Location</Text> */}
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Location"
            value={joblocation}
            onChangeText={(val) => setlocation(val)}
            placeholder="Location"
          />
          
        </View>

        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Personal Summary"
            value={description}
            onChangeText={(val) => setdescription(val)}
            placeholder="Enter Personal Summary"
          />
          
        </View>

        <View style={styles.repeatContainer}>
          <Text style={styles.title}>Preferred Industry</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 32, justifyContent: "center", backgroundColor: "#fff" }}>
             <RNPickerSelect
              placeholder={{
                label: 'Ex. Information Technology',
                value: null,
              }}
              placeholderTextColor="#999"
              onValueChange={(value) => setindustry(value)}
              style={pickerSelectStyles}
              items={[
                  { label: 'Information Technology', value: 'it_job' },
                  { label: 'Goverment Job', value: 'gov_job' },
                  { label: 'Railway Job', value: 'railway' },
                  { label: 'BPO', value: 'bpo' },
              ]}
            />
            
          </View>
        </View>

        <View style={[styles.repeatContainer, {marginBottom: 0,}]}>
          <Text style={styles.title}>Employment Type</Text>
          <View style={{ flexDirection: "row", flexWrap: "wrap", marginVertical: 10, }}>
            <RadioForm
              radio_props={radio_props}
              initial={0}
              formHorizontal={true}
              labelHorizontal={true}
              buttonColor={'#45cfd2'}
              selectedButtonColor={'#45cfd2'}
              animation={false}
              onPress={(value) => {setvalue(value)}}
              labelStyle={{marginRight: 10, fontFamily:FontFamily.Poppins_Regular }}
              buttonSize={10}
              style={{flexWrap: "wrap",}}
            />
          </View>
        </View>

        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Terget Monthly"
            value={mounthsalary}
            onChangeText={(val) => setmonthalysalary(val)}
            placeholder="Enter Terget Monthly"
          />
        
        </View>

        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputHeight={35}
            textInputWidth={screenwidth / 1.18}
            label="Noice Period"
            value={notice}
            onChangeText={(val) => setnotice(val)}
            placeholder="Enter Notice Period(Only Numbers)"
          />
          
        </View>

        
      </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
}

export default PreferredJob;

const styles = StyleSheet.create({
  repeatContainer: {
    width: "85%", alignItems: "flex-start", alignSelf: "center", marginBottom: 15
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, borderRadius: 10, height: 35, width: "100%", paddingHorizontal: 10, fontSize:FontSize.small_size, justifyContent:"center"
  },
  radioCircle: {
    padding: 9, 
    borderWidth: 2, 
    borderColor: Colors.Nero, 
    borderRadius: 100, 
    position: 'absolute'
  },
  title:{
    fontSize:FontSize.small_size,
    fontFamily:FontFamily.Poppins_Medium,
    //paddingStart:5
  }
})

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 15,
    paddingVertical: 10,
    paddingHorizontal: 0,
    // borderWidth: 1,
    // borderColor: 'gray',
    // borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 15,
    paddingHorizontal: 0,
    paddingVertical: 10,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});