import React, { useEffect, useState, useReducer } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ScrollView, Picker, Platform } from 'react-native';
import FontFamily from '../../Utils/FontFamily';
import { RadioButton } from 'react-native-paper';
import ButtonComponent from '../Button/loginButton';
import FontSize from '../../Utils/fonts';
import HeaderComponent from '../Header';
import Colors from '../../Utils/Colors';
import RNPickerSelect from 'react-native-picker-select';


const Language = (props) => {
  const [lavel, setlavel] = useState('');
  const [language, setlanguage] = useState('');

  const saveAndContinue = (e) => {
    e.preventDefault()
    props.nextStep()
  }

  const back = (e) => {
    e.preventDefault();
    props.prevStep();
  }

  const renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }

  return (
    <View style={{flex:1}}>
      <ScrollView style={{ flex: 1 }}>
        <Text style={{ textAlign: "center", color: "#182e4d", paddingVertical: "5%", fontSize: FontSize.large_size, fontFamily: FontFamily.Poppins_Medium }}>Enter Language</Text>
        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Language</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 32, justifyContent: "center", backgroundColor: "#fff" }}>
          <RNPickerSelect
            placeholder={{
              label: 'Choose language...',
              value: null,
            }}
            placeholderTextColor="#999"
            onValueChange={(value) => setlanguage(value)}
            style={pickerSelectStyles}
            items={[
                { label: 'English', value: 'bsc' },
                { label: 'Arabic', value: 'bcom' },
                { label: 'Urdu', value: 'msc' },
            ]}
            />
          </View>
        </View>
          
        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Level</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 32, justifyContent: "center", backgroundColor: "#fff" }}>
          <RNPickerSelect
            placeholder={{
              label: 'Choose Lavel...',
              value: null,
            }}
            placeholderTextColor="#999"
            onValueChange={(value) => setlavel(value)}
            style={pickerSelectStyles}
            items={[
                { label: 'Beginner', value: 'beginner' },
                { label: 'Intermediate', value: 'intermediate' },
                { label: 'Expert', value: 'expert' },
                { label: 'Native', value: 'native' },
            ]}
            />
          </View>
        </View> 
        

      </ScrollView>
    </View>
  );
}


export default Language;

const styles = StyleSheet.create({
  repeatContainer: {
    width: "85%", alignItems: "flex-start", alignSelf: "center", marginBottom: 10
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, borderRadius: 10, height: 40, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },
  radioCircle: {
    padding: 9, 
    borderWidth: 2, 
    borderColor: Colors.Nero, 
    borderRadius: 100, 
    position: 'absolute'
  },
  title:{
    fontSize:FontSize.small_size,
    fontFamily:FontFamily.Poppins_Medium
    //marginBottom:5,
    // paddingStart:5
  }
})

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 15,
    paddingVertical: 0,
    paddingHorizontal: 0,
    // borderWidth: 1,
    // borderColor: 'gray',
    // borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 15,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  title:{
    fontSize:FontSize.small_size,
    fontFamily:FontFamily.Poppins_Medium
    // marginBottom:5,
    // paddingStart:5
  }
});