import React from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image, StatusBar, Dimensions, SafeAreaView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontSize from "./../../Utils/fonts";
import Colors from "./../../Utils/Colors";
import FontFamily from '../../Utils/FontFamily';
import AntDesign from 'react-native-vector-icons/AntDesign';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const HeaderComponent = (props) => {

  return (
    <LinearGradient
      start={{ x: 0, y: 1 }}
      end={{ x: 1, y: 1 }}
      colors={['#45cfd2', '#58d19b',]} style={styles.linearGradient}
    >
      <SafeAreaView>
        <View style={{ flexDirection: "row", justifyContent: "space-between", }}>
          {props.drawericon ?
            <TouchableOpacity
              onPress={() => props.onClick()}
              style={{ flex: 0.15, alignItems: "center", paddingTop: 5, paddingLeft: props.drawerLeft }}>
              <Image
                style={{ width: 20, height: 12 }}
                source={require("../../Images/header/menu.png")}
              />
            </TouchableOpacity>
            : null
          }
          {props.backicon ?
            <TouchableOpacity
              onPress={() => props.onClick2()}
              style={{ flex: 0.15, alignItems: "center", paddingTop: 2, paddingLeft: props.headingLeft }}>
              <AntDesign
                name="arrowleft"
                size={25}
                color="black"
              />
            </TouchableOpacity>
            : null
          }

          <View style={{ flex: 0.65, alignItems: "flex-start" }}>
            <Text style={{ fontSize: FontSize.medium_size, fontFamily: "Poppins-Regular", color: "#1a2245" }} numberOfLines={1}>{props.headerTitle}</Text>
          </View>
          <View style={{ flex: 0.4, flexDirection: "row", alignItems: "center", justifyContent: "space-around" }}>
            {props.message ?
              <>
                <TouchableOpacity style={{ flex: 0.6, alignItems: "flex-end", }}>

                  <View>
                    <Fontisto
                      name="email"
                      size={25}
                      color="black"
                      onPress={() => props.messageiconpress()}
                    />
                    <View style={{ position: "absolute", left: 18, width: 16, height: 16, borderRadius: 8, alignItems: "center", backgroundColor: "red", justifyContent: "center", top: -2 }}>
                      <Text style={{ fontSize: 10, textAlign: "center", color: "#fff" }}>5</Text>
                    </View>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity style={{ flex: 0.7, alignItems: "center" }}>
                  {props.send ?
                    <Image source={require('../../Images/Share/plane_icon.png')} style={{ height: 30, width: 30 }} resizeMode="contain" />
                    : !props.noNotification ?
                      <Ionicons
                        name="ios-notifications-outline"
                        size={30}
                        color="black"
                        onPress={() => props.notificationonpress()}
                      />
                      : null
                  }
                  {!props.send & !props.noNotification ?
                    <View style={{ position: "absolute", right: 18, width: 16, height: 16, borderRadius: 8, alignItems: "center", backgroundColor: "#fff", justifyContent: "center" }}>
                      <Text style={{ fontSize: 10 }}>5</Text>
                    </View>
                    : null}
                </TouchableOpacity>
              </>
              : <TouchableOpacity style={{ flex: 0.6, alignItems: "flex-end" }}>
              {props.send ?
                <Image source={require('../../Images/Share/plane_icon.png')} style={{ height: 30, width: 30 }} resizeMode="contain" />
                : !props.noNotification ?
                  <Ionicons
                    name="ios-notifications-outline"
                    size={30}
                    color="black"
                    onPress={() => props.notificationonpress()}
                  />
                  : null
              }
              {!props.send & !props.noNotification ?
                <View style={{ position: "absolute", right: -7, width: 16, height: 16, borderRadius: 8, alignItems: "center", backgroundColor: "#fff", justifyContent: "center" }}>
                  <Text style={{ fontSize: 10 }}>5</Text>
                </View>
                : null}
            </TouchableOpacity>
            }

            

            {!props.dot ?
              <TouchableOpacity style={{ flex: 0.35, alignItems: "flex-start", marginLeft: 5 }}>
                <MaterialCommunityIcons
                  name="dots-vertical"
                  size={25}
                  color="black"
                />
              </TouchableOpacity>
              : null}
          </View>
        </View>
        {
          props.type == "textinput" ?
            <View style={{ flexDirection: "row", alignItems: "center", elevation: 2, backgroundColor: "#fff", marginHorizontal: 20, height: 50, borderRadius: 5, marginTop: 10 }}>
              <View style={{ flex: 0.85 }}>
                <TextInput
                  style={{ paddingHorizontal: 10, fontSize: FontSize.small_size, fontWeight: "600" }}
                  value={props.InputValue}
                  placeholder={props.placeholder}

                  onChangeText={(text) => props.onChangeText(text)}
                  autoCapitalize='none'
                  autoCorrect={false}
                  textColor={props.textColor}
                  textContentType={props.textContentType}
                  secureTextEntry={props.secureTextEntry}
                  returnKeyType='done'
                  enablesReturnKeyAutomatically={true}
                  selectTextOnFocus={true}
                  spellCheck={false}
                />
              </View>
              <TouchableOpacity
                style={{ flex: 0.15, alignItems: "center" }}
                activeOpacity={0.7}
              >
                <Ionicons
                  name="ios-search"
                  size={20}
                  color="gray"
                />
              </TouchableOpacity>

            </View>
            : null
        }

        {props.headerProfile == "Header Profile" ?
          <View style={{ width: "85%", alignSelf: "center" }}>
            <View style={styles.profileImageView}>
              <Image source={props.data[0].image} style={styles.profileImage} />
              <TouchableOpacity style={{ position: "absolute", bottom: 0, right: 0, backgroundColor: "#fff", borderRadius: 50, paddingVertical: 5, paddingHorizontal: 3 }} activeOpacity={0.8}>
                <EvilIcons
                  name="pencil"
                  size={20}
                  color="gray"
                />
              </TouchableOpacity>
            </View>
            <View style={{ alignSelf: "center", }}>
              <Text style={styles.profileName}>{props.data[0].name}</Text>
              <Text style={styles.profileDesignation}>{props.data[0].desingnation} - {props.data[0].companyname}</Text>
            </View>
            <View style={styles.percentageContainer}>
              <Text style={styles.profilePercentage}>{props.data[0].profilePercentage}% Completed</Text>
              <Text style={styles.profleUpdate}>{props.data[0].updated}</Text>
            </View>
            <View style={{ height: 1.8, width: "100%", backgroundColor: "#fff", alignSelf: "center", marginTop: 2, borderRadius: 10 }}>
              <View style={{ height: "100%", width: `${props.data[0].profilePercentage}%`, backgroundColor: "#faac4e", borderRadius: 10 }}>

              </View>
            </View>
            <TouchableOpacity style={{ position: "absolute", top: 10, right: 0, paddingVertical: 5, paddingHorizontal: 3 }} onPress={() => props.editprofile()}>
              <EvilIcons
                name="pencil"
                size={30}
                color="#000"
              />
            </TouchableOpacity>
          </View>
          : null}
      </SafeAreaView>
    </LinearGradient>
  );
}

export default HeaderComponent;

const styles = StyleSheet.create({
  linearGradient: {
    paddingVertical: 15,
    zIndex: 99999
  },
  profileImageView: {
    height: 85,
    width: 85,
    borderRadius: 80,
    alignSelf: "center",
    marginTop: 13,
    shadowColor: '#000',
    shadowRadius: 60,
    shadowOpacity: 0.7,
    elevation: 18,
    shadowOffset: {
      width: 0,
      height: 0
    }
  },
  profileImage: {
    height: "100%",
    width: "100%",
    borderRadius: 80,
    borderWidth: 2,
    borderColor: "#fff",
  },
  profileName: {
    alignSelf: "center",
    fontSize: 25,
    color: "#fff",
    //fontWeight: "700",
    marginTop: 5,
    fontFamily: FontFamily.Poppins_Medium
  },
  profileDesignation: {
    alignSelf: "center",
    color: "#fff",
    marginBottom: 20,
    marginTop: 0,
    fontFamily: FontFamily.Poppins_Semibold,
    fontSize: 15
  },
  profilePercentage: {
    alignSelf: "center",
    color: "#fff",
    fontFamily: FontFamily.Poppins_Medium,
    fontSize: FontSize.micro_size
  },
  percentageContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignSelf: "center",
    marginTop: 3
  },
  profleUpdate: {
    alignSelf: "center",
    fontWeight: "600",
    fontFamily: FontFamily.Poppins_Medium,
    fontSize: FontSize.micro_size,
    color: "#1e2844"
  }

})