import React, {useState,useEffect} from 'react';
import { View, Text, StyleSheet, TouchableOpacity, PermissionsAndroid} from 'react-native';
import FontSize from "./../../Utils/fonts";
import Colors from "./../../Utils/Colors";
// import ImagePicker from 'react-native-image-picker'

const ButtonComponent = (props) => {

  // const [photo, setPhoto] = useState(0);
  // const [options, setOption] = useState({
  //   quality: 1.0,
  //   maxWidth: 500,
  //   maxHeight: 500,
  //   storageOptions: {
  //     skipBackup: true
  //   }
  //   });
  //   useEffect(() =>{
  //     console.log("Enter useEffect");
      
  //     requestCameraPermission();
  //   })

  //   async function requestCameraPermission(){
  //     try {
  //       const granted = await PermissionsAndroid.request(
  //         PermissionsAndroid.PERMISSIONS.CAMERA,
  //         {
  //           title: 'Cool Photo App Camera Permission',
  //           message:
  //             'Cool Photo App needs access to your camera ' +
  //             'so you can take awesome pictures.',
  //           buttonNeutral: 'Ask Me Later',
  //           buttonNegative: 'Cancel',
  //           buttonPositive: 'OK',
  //         },
  //       );
  //       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //         console.log('You can use the camera');
  //       } else {
  //         console.log('Camera permission denied');
  //       }
  //     } catch (err) {
  //       console.warn(err);
  //     }
  //   }

  // async function onClick () {
  //   console.log('Enter into image upload   ');
    
    
  //   await ImagePicker.showImagePicker(options, (response) => {
  //     if (response.uri) {
  //       setPhoto({photo:response});
  //       console.log('response   =====  ',JSON.stringify(response.uri));
        
  //     }
  //   })
  //   setPhoto({photo:"response"})
  //    console.log("photo  ===",photo);
  // }

  return (
    <TouchableOpacity
      style={[styles.container, { backgroundColor: props.backgroundColor1,width:props.width,height:props.height,marginRight:props.marginRightSpace? props.marginRightSpace:0}]}
      onPress={props.onClick}
      activeOpacity={0.7}
    >
      {console.log("color",props)
      }
      <Text style={[styles.button_Text,{color: props.textColor? props.textColor: "#fff", alignSelf: props.align, paddingRight: props.right}]}>{props.buttonName}</Text>
    </TouchableOpacity>
  );
}

export default ButtonComponent;

ButtonComponent.defaultProps = {
  backgroundColor1: Colors.Nero,
  width:"70%",
  height:50

}

const styles = StyleSheet.create({
  container: {
    height:50,
    width: "70%",
    backgroundColor: Colors.Nero,
    borderRadius: 25,
    // alignItems: "center",
    justifyContent: "center",
    paddingHorizontal:10,
  },
  button_Text: {
    fontSize: FontSize.small_size,
    fontFamily: "Poppins-Medium",
    fontWeight:"800",
    alignSelf: "center",
    textAlign:"center",
    
  }
})