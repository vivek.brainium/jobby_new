import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';



const MessageList = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.profileContainer}>
        <View style={styles.ImageContainer}>
          <Image source={props.image} style={styles.profileImage} />
        </View>
        <View style={styles.profileDescriptionContainer}>
          <Text style={styles.name}>{props.name}</Text>
          <Text style={styles.companyName}>{props.companyName}</Text>
          <Text style={styles.jobDescription}>{props.jobDescription}</Text>
        </View>
      </View>
      <TouchableOpacity style={{}}>
        <Fontisto
          name="email"
          size={20}
          color="gray"
        />
      </TouchableOpacity>
    </View>
  );
}


export default MessageList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    width: "90%",
    paddingLeft: 15,
    paddingBottom: 10,
    //paddingRight: 15,
    borderWidth:0,
    alignSelf: "center",
    borderColor:"#000",
    shadowOpacity: 0.3,
    shadowOffset: 5,
    // elevation: 5,
    shadowOffset: {
    width: 0,
    height: 2
    },
    marginBottom: 15,
    padding: 20,
    backgroundColor:"#fff",borderRadius:5
  },
  profileContainer: {
    flexDirection: "row",

  },
  ImageContainer: {
    height: 60,
    width: 60,
    borderRadius: 60,
    marginRight: 10
  },
  profileImage: {
    height: "100%",
    width: "100%",
    borderRadius: 60,
  },
  profileDescriptionContainer: {
    // justifyContent: "center"
    width: "73%",
    borderWidth:0
  },
  name: {
    fontFamily: FontFamily.Poppins_Medium,
    fontSize: FontSize.small_size,
    fontWeight: "700",
    marginBottom: 2,
    color:"#515A5A"
  },
  companyName: {
    fontFamily: FontFamily.Poppins_Semibold,
    fontSize: FontSize.micro_size,
    fontWeight: "400",
    marginBottom: 10,
    color:"#515A5A"
  },
  jobDescription: {
    fontFamily: FontFamily.Poppins_Semibold,
    fontSize: FontSize.small_size,
    fontWeight: "400",
    marginBottom: 10,
    color: "#515A5A"
  },
})