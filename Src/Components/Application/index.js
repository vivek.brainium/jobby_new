import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Dimensions } from 'react-native';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;


const ApplicationComponent = (props) => {
  const { show } = props;

  const absoluteview = () => {
    return (
      <View style={{ position: "absolute", backgroundColor: "#fff", paddingHorizontal: 20, paddingVertical: 10, zIndex: 99999999, top: 0, right: -10, elevation: 5, minWidth: 150,       shadowOpacity: 0.6,
      shadowOffset: {
        width: 0,
        height: 2
      } }}>
        <TouchableOpacity style={{ paddingVertical: 5, borderBottomColor: "#e0e0e0", borderBottomWidth: 1 }} activeOpacity={0.5} onPress={() => props.details()}>
          <Text style={{ fontFamily: FontFamily.Poppins_Medium, color: "#999" }}>Accept</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ paddingVertical: 5, borderBottomColor: "#e0e0e0", borderBottomWidth: 1 }} onPress={() => props.block()}>
          <Text style={{ fontFamily: FontFamily.Poppins_Medium, color: "#999" }}>Reject</Text>
        </TouchableOpacity>
      </View>
    )
  }

  const shortlist = () => {
    return (
      <View style={{ position: "absolute", backgroundColor: "#fff", paddingHorizontal: 20, paddingVertical: 10, zIndex: 99999999, top: 0, right: -10, elevation: 5, minWidth: 150, shadowOpacity: 0.6, shadowOffset: { width: 0,height: 2}}}>
        <TouchableOpacity style={{ paddingVertical: 5, borderBottomColor: "#e0e0e0", borderBottomWidth: 1 }} activeOpacity={0.5}>
          <Text style={{ fontFamily: FontFamily.Poppins_Medium, color: "#999" }}>Cancle Shortlist</Text>
        </TouchableOpacity>
      </View>
    )
  }

  return (
    <View style={styles.maincontainer}>
      <View style={styles.secondcontainer}>
        <TouchableOpacity activeOpacity={1} style={styles.view1} onPress={() => props.maincontainerPress()} >
          <View style={styles.InnerView}>
            {props.profile_image ?
              <View style={styles.profileImage}>
                <Image source={props.image} style={{ height: 40, width: 40, borderRadius: 50 }} />
              </View>
              : null
            }
            <Text style={styles.title}>{props.title}</Text>
          </View>
          <View style={{ marginLeft: 2 }}>
            { props.designatonsection ?
              <View style={styles.repeatcontainer}>
                <View style={styles.postimage}>
                  <Image
                    style={{ width: 11, height: 16, marginHorizontal: 4.5 }}
                    source={require('../../Images/job_Details/pen.png')}
                  />
                </View>
                <Text style={styles.text}>{props.desingnation}</Text>
              </View>
              : null
            }
            <View style={styles.repeatcontainer}>
              <View style={styles.postimage}>
                <EvilIcons
                  style={{}}
                  name="location"
                  color="gray"
                  size={20}
                />
              </View>
              <Text style={styles.text}>{props.locationname}</Text>
            </View>
            {props.date ?
              <View style={{ paddingLeft: "10%", marginTop: 5 }}>
                <Text style={styles.text2}>{props.posteddate}</Text>
              </View>
              : null
            }
          </View>
          {show ?
            absoluteview()
            : null
          }
          {
            props.shortlistdotpress ?
              shortlist()
              : null
          }
        </TouchableOpacity>

        <View style={styles.view2}>
          {props.verticalthreedot ?
            <TouchableOpacity onPress={() => props.gotodetails()}>
              <Entypo
                style={{ marginBottom: 15 }}
                name="dots-three-vertical"
                color="gray"
                size={20}
              />
            </TouchableOpacity>
            : null
          }
          {props.delete ?
            <TouchableOpacity>
              <AntDesign
                style={{ marginBottom: 10 }}
                name="delete"
                color="#707070"
                size={20}
              />
            </TouchableOpacity>
            : null
          }
        </View>
      </View>
    </View>
  );
}


export default ApplicationComponent;

const styles = StyleSheet.create({
  maincontainer: {
    backgroundColor: "#e9e9e9", marginBottom: 15, elevation: 3
  },
  secondcontainer: {
    backgroundColor: "#ffffff", padding: 15, flexDirection: "row", flex: 1
  },
  view1: {
    flex: 0.9,
  },
  view2: {
    flex: 0.1, alignItems: "center", justifyContent: "flex-start"
  },
  title: {
    marginBottom: 10, paddingHorizontal: 8, fontSize: FontSize.small_size, fontFamily: FontFamily.Poppins_Medium, fontWeight: "600"
  },
  repeatcontainer: {
    flexDirection: "row"
  },
  yearimage: {
    flex: 0.12, alignItems: "center"
  },
  postimage: {
    alignItems: "flex-start", marginRight: 10
  },
  text: {
    marginBottom: 8, fontFamily: FontFamily.Poppins_Medium, color: "#494949"
  },
  text2: {
    fontFamily: FontFamily.Poppins_Regular, color: "#707070",
  },
  profileImage: { alignItems: "center", marginLeft: 10 },
  InnerView: { flexDirection: "row", alignItems: "center", marginBottom: 10 }
})