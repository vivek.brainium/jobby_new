import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';



const Employer_JobListing = (props) => {
  const {show} = props;

  const absoluteview = () => {
    return (
      <View style={{ position: "absolute", backgroundColor: "#fff", paddingHorizontal: 20, paddingVertical: 10, zIndex: 99999999, top: 10, right: -5, elevation: 5, minWidth:150, shadowColor: '#000',
      shadowOpacity: 0.6,
      shadowOffset: {
        width: 0,
        height: 2
      }}}>
        <TouchableOpacity style={{ paddingVertical: 5, borderBottomColor: "#e0e0e0", borderBottomWidth: 1 }} activeOpacity={0.5} onPress={()=> props.details()}>
          <Text style={{fontFamily:FontFamily.Poppins_Medium, color:"#807e7e"}}>View Applicants</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ paddingVertical: 5, borderBottomColor: "#e0e0e0", borderBottomWidth: 1 }}>
          <Text style={{fontFamily:FontFamily.Poppins_Medium, color:"#807e7e"}}>Hide Job</Text>
        </TouchableOpacity>
      </View>
    )
  }


  return (
    <TouchableOpacity onPress={()=> props.onclick()} style={styles.maincontainer}>
      <View style={styles.secondcontainer}>
        <TouchableOpacity activeOpacity={1} onPress={() => props.maincontainerPress()} style={styles.view1}>
          <Text style={styles.title}>{props.title}</Text>
          <View style={{ flex: 1, }}>
            <View style={styles.repeatcontainer}>
              <View style={styles.yearimage}>
                <SimpleLineIcons
                  style={{ marginBottom: 10, }}
                  name="briefcase"
                  color="gray"
                  size={16}
                />
              </View>
              <Text style={styles.text}>{props.years}</Text>
            </View>
            <View style={styles.repeatcontainer}>
              <View style={styles.yearimage}>
                <EvilIcons
                  style={{ marginBottom: 10, }}
                  name="location"
                  color="gray"
                  size={20}
                />
              </View>
              <Text style={styles.text}>{props.locationname}</Text>
            </View>
            <View style={styles.repeatcontainer}>
              <View style={styles.postimage}>
                <Image
                  style={{ width: 11, height: 16, marginHorizontal: 4.5 }}
                  source={require('../../Images/job_Details/pen.png')}
                />
              </View>
              <Text style={styles.text}>{props.desingnation}</Text>
            </View>
            <View style={{ paddingLeft: "10%" }}>
              <Text style={styles.text2}>{props.posteddate}</Text>
            </View>
          </View>

          {show ?
            absoluteview()
            : null
          }

        </TouchableOpacity>

        <View style={styles.view2}>
          <TouchableOpacity onPress={() => props.onclick()}>
            <Entypo
              style={{ marginBottom: 15 }}
              name="dots-three-vertical"
              color="gray"
              size={20}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <AntDesign
              style={{ marginBottom: 10 }}
              name="delete"
              color="#999"
              size={20}
            />
          </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>
  );
}


export default Employer_JobListing;

const styles = StyleSheet.create({
  maincontainer: {
    backgroundColor: "#e9e9e9", 
    marginBottom: 15, 
    elevation: 3
  },
  secondcontainer: {
    backgroundColor: "#ffffff", 
    padding: 15, 
    flexDirection: "row", 
    flex: 1
  },
  view1: {
    flex: 0.9,
  },
  view2: {
    flex: 0.1, 
    alignItems: "center", 
    justifyContent: "flex-start", 
    marginTop: 8
  },
  title: {
    marginBottom: 10, 
    paddingHorizontal: 8, 
    fontSize: FontSize.medium_size, 
    fontFamily: FontFamily.Poppins_Medium, 
    fontWeight: "600"
  },
  repeatcontainer: {
    flexDirection: "row", 
    alignItems: "flex-start", 
    flex: 1,
  },
  yearimage: {
    flex: 0.1, 
    alignItems: "center", 
    marginRight: 10
  },
  postimage: {
    flex: 0.12, 
    alignItems: "center", 
    marginRight: 12
  },
  text: {
    marginBottom: 8, 
    fontFamily: FontFamily.Poppins_Medium, 
    color: "#494949"
  },
  text2: {
    fontFamily: FontFamily.Poppins_Regular, 
    color: "#707070",
  },
})