import React, {useEffect} from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import FontSize from "./../../Utils/fonts";
import Colors from "./../../Utils/Colors";


const TextInputComponent = (props) => {

  useEffect(() => {
    console.log("Hlelo");
    
  }, [])

  return (
    <View style={styles.main_Container}>
      <View style={styles.sub_Container}>
        <View style={{ flex: 0.15, alignItems: "center" }}>

          {props.iconType == "SimpleLineIcons" ?
            <SimpleLineIcons
              style={{ paddingBottom: 6 }}
              name={props.iconName}
              size={props.iconSize}
              color={props.iconColor}
            /> :
            props.iconType == "MaterialIcons" ?
              <MaterialIcons
                style={{ paddingBottom: 6 }}
                name={props.iconName}
                size={props.iconSize}
                color={props.iconColor}
              /> :
              props.iconType == "Octicons" ?
              <Octicons
                style={{ paddingBottom: 6 }}
                name={props.iconName}
                size={props.iconSize}
                color={props.iconColor}
              /> :
              props.iconType == "image" ?
                <Image
                  style={{ paddingBottom: 6, width: props.width, height: props.height }}
                  source={props.image}
                />

                : null}
        </View>

        <View style={styles.textinput_View}>
          <View style={{ flex: 0.85 }}>
            <Text style={styles.title_Text}>{props.titleName}</Text>
            <TextInput
              style={[styles.textInput, {height: props.textheight}]}
              value={props.InputValue}
              placeholder={props.placeholder}
              onChangeText={(text) => props.onChangeText(text)}
              autoCapitalize='none'
              autoCorrect={false}
              textColor={props.textColor}
              textContentType={props.textContentType}
              secureTextEntry={props.secureTextEntry}
              returnKeyType='done'
              enablesReturnKeyAutomatically={true}
              selectTextOnFocus={true}
              spellCheck={false}
              maxLength={props.maxLength}
            />
          </View>
          {props.rightIconType == "Ionicons" ?
            <TouchableOpacity
              style={{ flex: 0.15, alignSelf: "center" }}
              activeOpacity={0.8}
              onPress={props.handleIconChange}
            >
              <Ionicons
                name={props.passwordIconName ? "md-eye" :
                  "md-eye-off"}
                color='#a0a0a0'
                size={24}
              />
            </TouchableOpacity>
            :
            props.rightIconType == "MaterialIcons" ?
              <View style={{ marginRight: 20, width: 18, height: 18, backgroundColor: "#1a2246", borderRadius: 8, alignItems: "center", justifyContent: "center" }}>
                <MaterialIcons
                  style={{}}
                  name='done'
                  color='#fff'
                  size={13}
                />
              </View>
              : null
          }
        </View>
      </View>
    </View>

  );
}

export default TextInputComponent;

const styles = StyleSheet.create({
  main_Container: {
    marginBottom: 15,
    marginHorizontal: "10%"
  },
  sub_Container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 40,
    backgroundColor: '#fff',
    shadowColor: Colors.BlackColor,
    shadowOpacity: 0.4,
    shadowRadius: 2,
    shadowOffset: { height: 0, width: 0 }
  },
  title_Text: {
    paddingTop: 2,
    paddingLeft: 1,
    color: Colors.DimGray,
    fontFamily: "Poppins-Medium"
  },
  textInput: {
    paddingVertical: 3,
    fontFamily: "Poppins-SemiBold",
    color: Colors.BlackColor,
  },
  textinput_View: {
    flex: 0.9,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
})