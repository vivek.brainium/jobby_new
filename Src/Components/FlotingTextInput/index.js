import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  Button,
  StatusBar,  
  KeyboardAvoidingView,
  TextInput, Animated
} from 'react-native';
import FontFamily from '../../Utils/FontFamily';
// import { TextInput } from 'react-native-paper';
var { height, width} = Dimensions.get('window');



export default class FloatingLabelInput extends Component {
  state = {
    isFocused: false,
  };

  componentWillMount() {
    this._animatedIsFocused = new Animated.Value(this.props.value === '' ? 1 : 0);
  }

  handleFocus = () => this.setState({ isFocused: true });
  handleBlur = () => this.setState({ isFocused: false });
  jewelStyle = () => {
    var borderColor = this.state.isFocused ? '#45cfd2' : "#999"
    return { height: this.props.textInputHeight, fontSize: 15, color: '#000', borderBottomWidth: this.state.isFocused ? 2 : 1, width: this.props.textInputWidth, borderBottomColor:borderColor,   }
  }

  componentDidUpdate() {
    Animated.timing(this._animatedIsFocused, {
      toValue: (this.state.isFocused || this.props.value !== '') ? 1 : 0,
      duration: 200,
    }).start();
  }

  render() {
    const { label, ...props } = this.props;
    const labelStyle = {
      position: 'absolute',
      left: 0,
      top:  0,
      fontSize: 16,
      color: '#000',
      fontFamily: FontFamily.Poppins_Medium,
    };
    return (
      <View style={{paddingTop:18,}}>
        <Animated.Text style={labelStyle}>
          {label}
        </Animated.Text>
        <TextInput
          {...props}
          style={this.jewelStyle() }
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          blurOnSubmit
        />
      </View>
    );
  }
}