import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Dimensions, Animated, Linking } from 'react-native';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Colors from "./../../Utils/Colors";
import ButtonComponent from './../../Components/Button/loginButton'
import * as Progress from 'react-native-progress';


const ProfileComponent = (props) => {

  const [progressStatus, setprogressStatus] = useState(100);
  const anim = new Animated.Value(0);

  useEffect(() => {
    onAnimate
  }, []);

  const onAnimate = () => {
    anim.addListener(({ value }) => {
      setprogressStatus({ progressStatus: parseInt(value, 10) })
    });
    Animated.timing(anim, {
      toValue: 100,
      duration: 5000,
    }).start();
  }


  return (
    <View style={styles.maincontainer}>
      <View style={styles.MainImageSection}>
        <View style={styles.ImageContainer}>
          <Image source={require("../../Images/AppliedProfile/image.jpg")}
            resizeMode={'contain'} style={{ height: "100%", width: "80%", }}
          />
        </View>

        <View style={styles.Details}>
          <Text style={styles.Name}>Jhon Marshal</Text>
          <Text style={styles.Profetion}>HR Executive</Text>
          <Text style={styles.Company}>Paladivine Inc.</Text>
          

          {props.prodileprogressbar ?
            <View>
              <View style={{ flexDirection: "row", justifyContent: 'space-between',marginBottom:4 }}>
                <View>
                  <Text style={{
                    fontSize
                      : 12, color: '#747474', textAlign: 'left'
                  }}>Profile Completeness</Text>
                </View>
                <View>
                  <Text style={{
                    fontSize
                      : 12, marginLeft:90, color: '#747474'
                  }}>95%</Text>
                </View>
              </View>
              <Progress.Bar
                  progress={0.95}
                  width={screenwidth/1.6}
                  color="#1a2245"
                  unfilledColor="#d3d3d3"
                  borderWidth={0}
                  height={3}
                />
              {/*             
                <View style={{
                  width: "90%",
                  height: 2,
                  padding: 0,
                  justifyContent: "center", backgroundColor: "gray", borderRadius: 25,
                  marginVertical: 5
                }}>

                  <Animated.View
                    style={[
                      styles.inner, { width: progressStatus + "%" },
                    ]}
                  />
                </View> */}

              <Text style={{
                fontSize
                  : 12, color: '#747474', textAlign: 'left',marginTop:4
              }}>Last update Feb 06.2019</Text>
              
            </View>
            : null
          }
          
        </View>
      </View>

      {
        props.InformationContainer ?

          <View style={styles.InformationContainer}>
            {props.profileyear ?
              <View style={styles.SingleContainer}>
                <SimpleLineIcons
                  name='briefcase'
                  size={20}
                  color='#323232'
                  style={{ marginRight: 20 }} />
                <Text style={{
                  color: '#323232', textAlign: 'center'
                }}>4 - 6 Years</Text>
              </View>
              : null
            }
            {props.profilelocation ?
              <View style={styles.SingleContainer}>
                <SimpleLineIcons
                  name='location-pin'
                  size={20}
                  color='#323232'
                  style={{ marginRight: 20 }} />
                <Text style={{
                  color: '#323232', textAlign: 'center'
                }}>California</Text>
              </View>
              : null
            }
            {props.profiledegree ?
              <View style={styles.MbaContainer}>
                <Feather
                  name='pen-tool'
                  size={20}
                  color='#323232'
                  style={{ marginRight: 20 }} />
                <Text style={{
                  color: '#323232', textAlign: 'center'
                }}>MBA</Text>
              </View>
              : null
            }
            {props.profilemail ?
              <View style={styles.SingleContainer}>
                <Fontisto
                  name='email'
                  size={20}
                  color='#323232'
                  style={{ marginRight: 20 }} />
                <Text style={{
                  color: '#323232', textAlign: 'center'
                }}>johnmarshal@gmail.com</Text>
              </View>
              : null
            }
            {props.profilephone ?
              <View style={styles.SingleContainer}>
                <Feather
                  name='phone-call'
                  size={20}
                  color='#323232'
                  style={{ marginRight: 20 }} />
                <Text style={{
                  color: '#323232', textAlign: 'center'
                }}>9830731745</Text>
              </View>
              : null
            }
          </View>
          : null
      }

      {
        props.InformationContainer2 ?
          <View style={styles.InformationContainer2}>
            {props.profileyear ?
              <View style={styles.SingleContainer}>
                <SimpleLineIcons
                  name='briefcase'
                  size={20}
                  color='#323232'
                  style={{ marginRight: 20 }} />
                <Text style={{
                  color: '#323232', textAlign: 'center', fontFamily: FontFamily.Poppins_Regular,
                }}>4 - 6 Years</Text>
              </View>
              : null
            }
            {props.profilelocation ?
              <View style={styles.SingleContainer}>
                <SimpleLineIcons
                  name='location-pin'
                  size={20}
                  color='#323232'
                  style={{ marginRight: 20 }} />
                <Text style={{
                  color: '#323232', textAlign: 'center', fontFamily: FontFamily.Poppins_Regular,
                }}>California</Text>
              </View>
              : null
            }
            {props.profiledegree ?
              <View style={styles.MbaContainer}>
                <Feather
                  name='pen-tool'
                  size={20}
                  color='#323232'
                  style={{ marginRight: 20 }} />
                <Text style={{
                  color: '#323232', textAlign: 'center', fontFamily: FontFamily.Poppins_Regular,
                }}>MBA</Text>
              </View>
              : null
            }
          </View>
          : null
      }

      {
        props.ProfileSummery ?
          <View style={styles.ProfileSommeryContainer}>
            <View style={styles.FirstContainer}>
              <View style={{ flex: 0.40, justifyContent: 'center' }}>
                <Text style={styles.ProfileSummery}>Profile Summary</Text>
              </View>
              <View style={{ flex: 0.60, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity style={styles.ResumeButton}>
                  <Text style={styles.DownloadResume}>Download Resume</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.DescriptionContainer}>
              <Text style={styles.Description}>it is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. <Text style={styles.ReadMore}>Read More</Text></Text>
            </View>
          </View>
          : null
      }

      {props.shortlist ?
        <View style={styles.LastContainer}>
          <Text style={styles.Shortlist}>Shortlist</Text>
        </View>
        : null
      }

      {props.shedule ?
        <View style={{ marginTop: "8%", flexDirection: "row", justifyContent: "space-around" }}>
          <TouchableOpacity style={{ width: "41%", flexDirection: "row", flexWrap: "wrap", alignItems: "center", justifyContent: "center" }} onPress={() => props.shedulePress()}>
            <Text style={{ textAlign: "center", color: "red", fontFamily: FontFamily.Poppins_Regular, }}>Schedule | </Text>
            <Text style={{ textAlign: "center", color: "red", fontFamily: FontFamily.Poppins_Regular, }}>Re-Schedule</Text>
            <Text style={{ textAlign: "center", color: "red", fontFamily: FontFamily.Poppins_Regular, }}>Cancel</Text>
          </TouchableOpacity>
          {/* <TouchableOpacity style={{ width: "41%", flexDirection: "row" }}>
            <Text style={{ textAlign: "center", color: "red", fontFamily: FontFamily.Poppins_Regular, }}>Video | Audio Cancel</Text>
          </TouchableOpacity  > */}
        </View>
        : null
      }

      <View style={{ justifyContent: 'space-around', marginTop: 15 }}>
        <View style={{ flexDirection: "row", justifyContent: 'space-around', marginBottom: 15 }}>
          <ButtonComponent
            buttonName="Contact"
            backgroundColor1="#73e470"
            height={50}
            width="41%"
            //onClick={() => Linking.openURL('tel:9830731745')}
          />
          <ButtonComponent
            buttonName="Cancle"
            backgroundColor1="#1a2246"
            height={50}
            width="41%"
          // onClick={() => this.props.navigation.navigate("FAQ")}
          />
        </View>
        {
          props.cancleShortlist ?
            <View style={{ marginTop: 0, alignItems: "center", justifyContent: "center" }}>
              <ButtonComponent
                buttonName="Cancel Shortlisted"
                backgroundColor1="#4b849e"
                height={50}
                width="90%"
              // onClick={() => this.props.navigation.navigate("FAQ")}
              />
            </View>
            : null
        }
        {
          props.block ?
            <View style={{ marginBottom: 20, alignItems: "center", justifyContent: "center" }}>
              <ButtonComponent
                buttonName="Block"
                backgroundColor1="#4b849e"
                height={50}
                width="90%"
                onClick={() => props.blockPress()}
              />
            </View>
            : null
        }
      </View>


    </View>
  );
}


export default ProfileComponent;

const styles = StyleSheet.create({
  maincontainer: {
    width: screenwidth,
    backgroundColor: '#f5f5f5',
  },
  MainImageSection: {

    flex: 0.25,
    flexDirection: 'row',
    marginTop: 10
  },
  ImageContainer: {
    flex: 0.35, alignItems: 'center',

  },
  Details: {

    flex: 0.65,
    alignItems: 'flex-start',
    paddingTop: 5,
    justifyContent: "center"
  },
  Name: {
    fontSize: FontSize.large_size,
    color: '#000',
    textAlign: 'left',
    fontFamily: FontFamily.Poppins_Semibold,
    fontWeight: 'bold'
  },
  Profetion: {
    fontSize: FontSize.small_size,
    color: '#333333',
    textAlign: 'left',
    fontFamily: FontFamily.Poppins_Medium,
    fontWeight: '600'
  },
  Company: {
    color: '#464646',
    textAlign: 'left',
    fontFamily: 'Poppins-Light',
    fontWeight: '500'
  },
  InformationContainer: {

    flex: 0.3,
    marginLeft: 25,
    marginTop: 25
  },
  InformationContainer2: {

    flex: 0.20,
    marginLeft: 25,
    marginTop: 25
  },
  SingleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10
  },
  MbaContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15
  },
  ProfileSommeryContainer: {

    flex: 0.34,
    marginTop: 20,
    paddingTop: 10,
    backgroundColor: '#fff',
    // shadowOpacity: 0.5,
    // shadowRadius: 5,
    elevation: 2
  },
  FirstContainer: {
    flexDirection: 'row',
    marginLeft: 25
  },
  ProfileSummery: {
    color: '#000',
    fontSize: FontSize.medium_size,
    fontWeight: '500',
    fontFamily: FontFamily.Poppins_Medium,
  },
  ResumeButton: {
    borderRadius: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 5, height: screenHeight / 18,
    width: screenwidth / 2.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  DownloadResume: {
    fontWeight: '800',
    color: '#4b849e',
    fontFamily: FontFamily.Poppins_Medium
  },
  DescriptionContainer: {
    marginLeft: 25,
    marginVertical: 15,
    paddingRight: 2
  },
  Description: {
    color: '#747474',
    textAlign: 'left',
    fontFamily: 'Poppins-Light'
  },
  ReadMore: {
    color: '#2f2f2f',
    fontSize: FontSize.small_size,
    textAlign: 'left',
    fontFamily: 'Poppins-Light'
  },
  LastContainer: {
    paddingVertical: 10,
    justifyContent: 'center',
  },
  Shortlist: {
    color: '#2f2f2f',
    fontSize: FontSize.small_size,
    textAlign: 'center'
  },
  inner: {
    width: "100%",
    height: 2,
    borderRadius: 15,
    backgroundColor: "green",
  },

})