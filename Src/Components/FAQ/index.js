import React, { Component } from 'react';
import { View, Text, StyleSheet, } from 'react-native';


const FAQComponent = (props) => {
  return (
    <View style={{ backgroundColor: "#e9e9e9", marginBottom: 2, }}>
      <View style={{ backgroundColor: "#ffffff", padding: 20 }}>
        <Text style={styles.title}>{props.title}</Text>
        <Text style={styles.description}>{props.description}</Text>
      </View >
    </View>
  );
}


export default FAQComponent;

const styles = StyleSheet.create({
  title: {
    fontWeight: '600',
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: "#000"
  },
  description: {
    fontWeight: '400',
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    color: "#a1a1a1"
  }
})