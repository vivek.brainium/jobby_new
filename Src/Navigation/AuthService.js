import React, { Component } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import { View, ActivityIndicator, Dimensions, StyleSheet, Image, BackHandler } from 'react-native';
import {save_user} from '../actions/userAction';
import {connect} from 'react-redux';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

class AuthService extends Component{
    
    state ={
        auth: '',
        loading: true
    }
    async componentDidMount(){
        this.checkToken()
    }

    checkToken = async () => {
        const token = await AsyncStorage.getItem('authtocken');
        console.log("TOKEN", token);
        
        const user = token;
        
        if(!user || user == '' || user == null) {
          this.props.navigation.navigate('FindJob')
        } else {
          this.props.save_user(user)
        }
    }

    componentWillMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
  
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    handleBackButtonClick = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    render(){     
         return(
                <View style={styles.container}>
                  <Image source={require('../../Src/Images/splash/splash.jpg')} style={styles.image} />
                </View>
            )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    image: {
      height: screenHeight,
      width: screenwidth
    }
  })

   const mapStateToProps = (state) => {
    return {
      user: state.userdata
    }
  }


export default connect(mapStateToProps, {save_user})(AuthService)