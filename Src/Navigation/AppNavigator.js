
import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Landing from '../Containers/Landing'
import Login from '../Containers/Login';
import DrawerComponent from './DrawerNavigator';
import PostJob from '../Containers/PostJob';
import EmployeeHomescreen from '../Containers/EmployeeHomescreen'
import ForgetPassword from '../Containers/ForgetPassword';
import EmployerRegistration from '../Containers/EmployerRegistration';
import Share from '../Containers/ShareApp'
import CalenderSchedule from '../Containers/CalenderSchedule'
import Splash from '../Containers/SplashScreen'
import FindJob from '../Containers/FindJob'
import EmployerCompanyDetails from '../Containers/EmployerCompanyDetails'
import EmployerJobListing from '../Containers/EmployerJobListing'
import EmployeeJobList from '../Containers/EmployeeJobList'
import Employer from "../Containers/Employer"
import SubmitResume from '../Containers/SubmitResume'
import CompanyDetails from '../Containers/CompanyDetails';
//import Profile from '../Containers/Profile';
import ReportBlock from '../Containers/BlockReport'
import ViewJob from '../Containers/ViewJob'
import SaveJob from '../Containers/SaveJob'
import JobApplied from '../Containers/JobApplied'
import ApplyJob from '../Containers/JobApply'
import ProfileEmployer from '../Containers/ProfileEmployer'
import JobShortlisted from '../Containers/JobShortListed'
import AppliedJob from '../Containers/AppliedJob'
import Profile from '../Containers/Profile';
import JobListing2 from '../Containers/JobListing2';
import EmployeeRegistration from '../Containers/EmployeeRegistration';
import { connect } from 'react-redux'
import AuthService from './AuthService';



const Stack = createStackNavigator();


class AppNavigator extends Component {

  state = {
    auth: '',
    logged: false
  }


  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.userdata.user) {
      console.log("this.props", nextProps.userdata);
      this.setState({
        logged: true
      })
    } else {
      this.setState({
        logged: false
      })
    }
  }

  render() {
    return (
      <Stack.Navigator screenOptions={{
        headerShown: false,
        // gesturesEnabled: false,
      }}

      >
        {
          !this.state.logged ?
            <>
              <Stack.Screen name="AuthService" component={AuthService} initialRouteName="AuthService" />
              <Stack.Screen name="Login" component={Login} />
              <Stack.Screen name="FindJob" component={FindJob} options={{
                gestureEnabled: false,
              }} />
              <Stack.Screen name="Landing" component={Landing} />
              <Stack.Screen name="EmployeeRegistration" component={EmployeeRegistration} />
              <Stack.Screen name="CompanyDetails" component={CompanyDetails} />
              <Stack.Screen name="EmployerCompanyDetails" component={EmployerCompanyDetails} />
              <Stack.Screen name="Share" component={Share} />
              <Stack.Screen name="JobListing2" component={JobListing2} />
              <Stack.Screen name="CalenderSchedule" component={CalenderSchedule} />
              <Stack.Screen name="ForgetPassword" component={ForgetPassword} />
              <Stack.Screen name="EmployerRegistration" component={EmployerRegistration} />
            </>
            :
            <>
              <Stack.Screen name="DrawerComponent" component={DrawerComponent} />
              <Stack.Screen name="EmployeeHomescreen" component={EmployeeHomescreen} />
            </>
        }
      </Stack.Navigator>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userdata: state.userdata
  }
}

export default connect(mapStateToProps, null)(AppNavigator)