import React from 'react';
import { Text, View, TouchableOpacity, Image, Dimensions, StyleSheet, ScrollView, SafeAreaView, StatusBar } from 'react-native';
import Colors from "../Utils/Colors";
import { DrawerContentScrollView } from '@react-navigation/drawer';
import FontFamily from '../Utils/FontFamily';
import FontSize from "../Utils/fonts";
import AntDesign from 'react-native-vector-icons/AntDesign';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Feather from 'react-native-vector-icons/Feather';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';
const screenwidth = Dimensions.get('window').width;
const screenheight = Dimensions.get('window').height;

const DrawerContent = (props, useData) => {

    // console.log("Drawer content   ===== ", useData);

   const logout = async () => {
        // props.navigation.navigate("Login");
        await AsyncStorage.removeItem('usertype');
        await AsyncStorage.removeItem('authtocken');
        // this.props.save_user()
    }

    return (
        <View style={styles.container}>
            <SafeAreaView style={{ flex:1, backgroundColor: Colors.StatusBar }} />
            <SafeAreaView>
            <TouchableOpacity
                onPress={() => props.navigation.closeDrawer()}
                style={{ padding: 20, }}>
                <Image
                    style={{ width: 20, height: 12 }}
                    source={require("../Images/header/menu.png")}
                />
            </TouchableOpacity>

            <View style={styles.boderLine} />

            <ScrollView showsVerticalScrollIndicator={false} style={{ marginBottom: 40 }}>
                     <View style={{ paddingBottom: 20, marginHorizontal: 20 }}>
                <View style={[styles.profile, { marginTop: 20 }]} onPress={() => props.navigation.navigate('EmployeeHomescreen')}>
                    <Image
                        style={styles.profileImage}
                        source={{ uri: "https://www.stickpng.com/assets/images/585e4bf3cb11b227491c339a.png" }}
                    // source={require("../Images/Drawer/employers_icon.png")}
                    />
                    <View style={{ paddingLeft: 15 }}>
                        <Text style={styles.userName}>John Marshal</Text>
                        <Text style={styles.useremail}>brainium@gmail.com</Text>
                    </View>
                </View>

                <View style={styles.menuList}>
                    <AntDesign
                        name="user"
                        size={20}
                        color={Colors.DrawerUserColor}
                    />
                    <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('EmployeeHomescreen')}>
                        <Text style={styles.menuName}>Profile</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.menuList}>
                    <Fontisto
                        name="email"
                        size={20}
                        color={Colors.DrawerUserColor}
                    />
                    <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('MessageScreen')}>
                        <Text style={styles.menuName}>Inbox</Text>
                    </TouchableOpacity>
                </View>

                {useData == "employers" ?
                    <View>
                        <View style={styles.menuList}>
                            <FontAwesome5
                                name="building"
                                size={20}
                                color={Colors.DrawerUserColor}
                            />
                            <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('EmployerCompanyDetails')}>
                                <Text style={styles.menuName}>Compnay Details</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.menuList}>
                            <SimpleLineIcons
                                name="briefcase"
                                size={20}
                                color={Colors.DrawerUserColor}
                            />
                            <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('PostJob')}>
                                <Text style={styles.menuName}>Post a Job</Text>
                            </TouchableOpacity>
                        </View>


                        <View style={styles.menuList}>
                            <Ionicons
                                name="ios-list-box"
                                size={20}
                                color={Colors.DrawerUserColor}
                            />
                            <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('EmployerJobListing')}>
                                <Text style={styles.menuName}>My Job List</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.menuList}>
                            <AntDesign
                                name="solution1"
                                size={20}
                                color={Colors.DrawerUserColor}
                            />
                            <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('ShortList')}>
                                <Text style={styles.menuName}>Shortlisted Employees</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.menuList}>
                            <Image
                                style={{ width: 16, height: 20 }}
                                source={require("./../Images/Drawer/shortlist_icon.png")}
                            />
                            <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('EmployeeList')}>
                                <Text style={styles.menuName}>Employees List</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    : null}

                {useData == "employees" ?
                    <View>

                        <View style={styles.menuList}>
                            <SimpleLineIcons
                                name="briefcase"
                                size={20}
                                color={Colors.DrawerUserColor}
                            />
                            <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('EmployeeJobList')}>
                                <Text style={styles.menuName}>Posted Jobs</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.menuList}>
                            <Feather
                                name="briefcase"
                                size={20}
                                color={Colors.DrawerUserColor}
                            />
                            <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('SubmitResume')}>
                                <Text style={styles.menuName}>Submit Resume</Text>
                            </TouchableOpacity>
                        </View>


                        <View style={styles.menuList}>
                            <Ionicons
                                name="ios-list-box"
                                size={20}
                                color={Colors.DrawerUserColor}
                            />
                            <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('SaveJobListing')}>
                                <Text style={styles.menuName}>Saved Jobs</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.menuList}>
                            <AntDesign
                                name="solution1"
                                size={20}
                                color={Colors.DrawerUserColor}
                            />
                            <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('AppliedJob')}>
                                <Text style={styles.menuName}>Applied Jobs</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.menuList}>
                            <Image
                                style={{ width: 16, height: 20 }}
                                source={require("./../Images/Drawer/shortlist_icon.png")}
                            />
                            <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('ShortList')}>
                                <Text style={styles.menuName}>My Shortlists</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.menuList}>
                            <EvilIcons
                                name="user"
                                size={20}
                                color={Colors.DrawerUserColor}
                            />
                            <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('Employers')}>
                                <Text style={styles.menuName}>Employers</Text>
                            </TouchableOpacity>
                        </View>


                        {/* <View style={styles.menuList}>
                            <FontAwesome5
                                name="user-plus"
                                size={15}
                                color={Colors.DrawerUserColor}
                            />
                            <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('EmployeeList')}>
                                <Text style={styles.menuName}>Refer</Text>
                            </TouchableOpacity>
                        </View> */}
                    </View>

                    : null}

                <View style={styles.menuList}>
                    <SimpleLineIcons
                        name="settings"
                        size={20}
                        color={Colors.DrawerUserColor}
                    />
                    <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('Setting')}>
                        <Text style={styles.menuName}>Settings</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.menuList}>
                    <Image
                        style={{ width: 22, height: 19.5 }}
                        source={require("./../Images/Drawer/help_icon.png")}
                    />
                    <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('Help')}>
                        <Text style={styles.menuName}>Help</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.menuList}>
                    <EvilIcons
                        name="share-google"
                        size={20}
                        color={Colors.DrawerUserColor}
                    />
                    <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('Share')}>
                        <Text style={styles.menuName}>Share App</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.menuList}>
                    <FontAwesome5
                        name="file-invoice-dollar"
                        size={20}
                        color={Colors.DrawerUserColor}
                    />
                    <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('ManagePlan')}>
                        <Text style={styles.menuName}>Manage Plan</Text>
                    </TouchableOpacity>
                </View>

                {useData == "employers" ?
                    <TouchableOpacity style={styles.menuList}>
                        <AntDesign
                            name="user"
                            size={20}
                            color={Colors.DrawerUserColor}
                        />
                        <TouchableOpacity style={{ paddingLeft: 15 }}>
                            <Text style={styles.menuName}>Delete Account</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    : null}
                    </View>
            </ScrollView>

            <TouchableOpacity style={{
                shadowOpacity: 0,
                elevation: 1,
                borderTopWidth: 0,
                borderTopColor: Colors.BlackColor,
                padding: 20,
                flexDirection: "row"
            }} onPress={logout}>
                <AntDesign
                    name="logout"
                    size={25}
                    color={Colors.logOut}
                />
                <Text style={{ fontSize: FontSize.medium_size, color: Colors.logOut, fontWeight: "bold", paddingLeft: 15 }}>LOG OUT</Text>
            </TouchableOpacity>
            </SafeAreaView>
      </View>
  );
}

export default DrawerContent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.DrawerBackground
  },
  boderLine: {
    backgroundColor: Colors.WhiteColor,
    height: 0.4
  },
  profile: {
    flexDirection: "row",
    alignItems: "center",
    paddingBottom: 20
  },
  profileImage: {
    width: screenheight / 14,
    height: screenheight / 14,
    borderRadius: 50
  },
  userName: {
    fontFamily: FontFamily.Poppins_Semibold,
    fontSize: FontSize.medium_size,
    color: Colors.DrawerUserColor,
    fontWeight: "600"
  },
  userEmail: {
    fontFamily: FontFamily.Poppins_Regular,
    color: Colors.DrawerUserColor
  },
  menuList: {
    paddingVertical: 10,
    flexDirection: "row",
    alignItems: "center"
  },
  menuName: {
    fontFamily: FontFamily.Poppins_Regular,
    fontSize: FontSize.small_size,
    color: Colors.DrawerUserColor
  }
});













// import React from 'react';
// import { Text, View, TouchableOpacity, Image, Dimensions, StyleSheet, ScrollView, SafeAreaView, StatusBar } from 'react-native';
// import Colors from "../Utils/Colors";
// import { DrawerContentScrollView } from '@react-navigation/drawer';
// import FontFamily from '../Utils/FontFamily';
// import FontSize from "../Utils/fonts";
// import AntDesign from 'react-native-vector-icons/AntDesign';
// import Fontisto from 'react-native-vector-icons/Fontisto';
// import Feather from 'react-native-vector-icons/Feather';
// import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
// import Ionicons from 'react-native-vector-icons/Ionicons';
// import EvilIcons from 'react-native-vector-icons/EvilIcons';
// import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// import AsyncStorage from '@react-native-community/async-storage';
// const screenwidth = Dimensions.get('window').width;
// const screenheight = Dimensions.get('window').height;




// const DrawerContent = (props, useData) => {

//     // console.log("Drawer content   ===== ", useData);

//     const logout = async () => {
//         // props.navigation.navigate("Login");
//         await AsyncStorage.removeItem('usertype');
//         await AsyncStorage.removeItem('authtocken');
//         // this.props.save_user()
//     }

//     return (
//         <View style={styles.container}>
//             <StatusBar backgroundColor={Colors.StatusBar} barStyle="light-content" />
//             <SafeAreaView>
//                 <TouchableOpacity
//                     onPress={() => props.navigation.closeDrawer()}
//                     style={{ padding: 20, }}>
//                     <Image
//                         style={{ width: 20, height: 12 }}
//                         source={require("../Images/header/menu.png")}
//                     />
//                 </TouchableOpacity>

//                 <View style={styles.boderLine} />

//                 <ScrollView showsVerticalScrollIndicator={false} style={{ marginBottom: 40 }}>
//                     <View style={{ paddingBottom: 20, marginHorizontal: 20 }}>
//                         <View style={[styles.profile, { marginTop: 20 }]} onPress={() => props.navigation.navigate('EmployeeHomescreen')}>
//                             <Image
//                                 style={styles.profileImage}
//                                 source={{ uri: "https://www.stickpng.com/assets/images/585e4bf3cb11b227491c339a.png" }}
//                             // source={require("../Images/Drawer/employers_icon.png")}
//                             />
//                             <View style={{ paddingLeft: 15 }}>
//                                 <Text style={styles.userName}>John Marshal</Text>
//                                 <Text style={styles.userEmail}>brainium@gmail.com</Text>
//                             </View>
//                         </View>

//                         <View style={styles.menuList}>
//                             <AntDesign
//                                 name="user"
//                                 size={20}
//                                 color={Colors.DrawerUserColor}
//                             />
//                             <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('EmployeeHomescreen')}>
//                                 <Text style={styles.menuName}>Profile</Text>
//                             </TouchableOpacity>
//                         </View>

//                         <View style={styles.menuList}>
//                             <Fontisto
//                                 name="email"
//                                 size={20}
//                                 color={Colors.DrawerUserColor}
//                             />
//                             <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('MessageScreen')}>
//                                 <Text style={styles.menuName}>Inbox</Text>
//                             </TouchableOpacity>
//                         </View>

//                         {useData == "employers" ?
//                             <View>
//                                 <View style={styles.menuList}>
//                                     <FontAwesome5
//                                         name="building"
//                                         size={20}
//                                         color={Colors.DrawerUserColor}
//                                     />
//                                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('EmployerCompanyDetails')}>
//                                         <Text style={styles.menuName}>Compnay Details</Text>
//                                     </TouchableOpacity>
//                                 </View>

//                                 <View style={styles.menuList}>
//                                     <SimpleLineIcons
//                                         name="briefcase"
//                                         size={20}
//                                         color={Colors.DrawerUserColor}
//                                     />
//                                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('PostJob')}>
//                                         <Text style={styles.menuName}>Post a Job</Text>
//                                     </TouchableOpacity>
//                                 </View>


//                                 <View style={styles.menuList}>
//                                     <Ionicons
//                                         name="ios-list-box"
//                                         size={20}
//                                         color={Colors.DrawerUserColor}
//                                     />
//                                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('EmployerJobListing')}>
//                                         <Text style={styles.menuName}>My Job List</Text>
//                                     </TouchableOpacity>
//                                 </View>

//                                 <View style={styles.menuList}>
//                                     <AntDesign
//                                         name="solution1"
//                                         size={20}
//                                         color={Colors.DrawerUserColor}
//                                     />
//                                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('ShortList')}>
//                                         <Text style={styles.menuName}>Shortlisted Employees</Text>
//                                     </TouchableOpacity>
//                                 </View>

//                                 <View style={styles.menuList}>
//                                     <Image
//                                         style={{ width: 16, height: 20 }}
//                                         source={require("./../Images/Drawer/shortlist_icon.png")}
//                                     />
//                                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('EmployeeList')}>
//                                         <Text style={styles.menuName}>Employees List</Text>
//                                     </TouchableOpacity>
//                                 </View>
//                             </View>
//                             : null}

//                         {useData == "employees" ?
//                             <View>

//                                 <View style={styles.menuList}>
//                                     <SimpleLineIcons
//                                         name="briefcase"
//                                         size={20}
//                                         color={Colors.DrawerUserColor}
//                                     />
//                                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('EmployeeJobList')}>
//                                         <Text style={styles.menuName}>Posted Jobs</Text>
//                                     </TouchableOpacity>
//                                 </View>

//                                 <View style={styles.menuList}>
//                                     <Feather
//                                         name="briefcase"
//                                         size={20}
//                                         color={Colors.DrawerUserColor}
//                                     />
//                                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('SubmitResume')}>
//                                         <Text style={styles.menuName}>Submit Resume</Text>
//                                     </TouchableOpacity>
//                                 </View>


//                                 <View style={styles.menuList}>
//                                     <Ionicons
//                                         name="ios-list-box"
//                                         size={20}
//                                         color={Colors.DrawerUserColor}
//                                     />
//                                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('SaveJobListing')}>
//                                         <Text style={styles.menuName}>Saved Jobs</Text>
//                                     </TouchableOpacity>
//                                 </View>

//                                 <View style={styles.menuList}>
//                                     <AntDesign
//                                         name="solution1"
//                                         size={20}
//                                         color={Colors.DrawerUserColor}
//                                     />
//                                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('AppliedJob')}>
//                                         <Text style={styles.menuName}>Applied Jobs</Text>
//                                     </TouchableOpacity>
//                                 </View>

//                                 <View style={styles.menuList}>
//                                     <Image
//                                         style={{ width: 16, height: 20 }}
//                                         source={require("./../Images/Drawer/shortlist_icon.png")}
//                                     />
//                                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('ShortList')}>
//                                         <Text style={styles.menuName}>My Shortlists</Text>
//                                     </TouchableOpacity>
//                                 </View>

//                                 <View style={styles.menuList}>
//                                     <EvilIcons
//                                         name="user"
//                                         size={20}
//                                         color={Colors.DrawerUserColor}
//                                     />
//                                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('Employers')}>
//                                         <Text style={styles.menuName}>Employers</Text>
//                                     </TouchableOpacity>
//                                 </View>


//                                 {/* <View style={styles.menuList}>
//                             <FontAwesome5
//                                 name="user-plus"
//                                 size={15}
//                                 color={Colors.DrawerUserColor}
//                             />
//                             <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('EmployeeList')}>
//                                 <Text style={styles.menuName}>Refer</Text>
//                             </TouchableOpacity>
//                         </View> */}
//                             </View>

//                             : null}

//                         <View style={styles.menuList}>
//                             <SimpleLineIcons
//                                 name="settings"
//                                 size={20}
//                                 color={Colors.DrawerUserColor}
//                             />
//                             <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('Setting')}>
//                                 <Text style={styles.menuName}>Settings</Text>
//                             </TouchableOpacity>
//                         </View>

//                         <View style={styles.menuList}>
//                             <Image
//                                 style={{ width: 22, height: 19.5 }}
//                                 source={require("./../Images/Drawer/help_icon.png")}
//                             />
//                             <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('Help')}>
//                                 <Text style={styles.menuName}>Help</Text>
//                             </TouchableOpacity>
//                         </View>

//                         <View style={styles.menuList}>
//                             <EvilIcons
//                                 name="share-google"
//                                 size={20}
//                                 color={Colors.DrawerUserColor}
//                             />
//                             <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('Share')}>
//                                 <Text style={styles.menuName}>Share App</Text>
//                             </TouchableOpacity>
//                         </View>

//                         <View style={styles.menuList}>
//                             <FontAwesome5
//                                 name="file-invoice-dollar"
//                                 size={20}
//                                 color={Colors.DrawerUserColor}
//                             />
//                             <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('ManagePlan')}>
//                                 <Text style={styles.menuName}>Manage Plan</Text>
//                             </TouchableOpacity>
//                         </View>

//                         {useData == "employers" ?
//                             <TouchableOpacity style={styles.menuList}>
//                                 <AntDesign
//                                     name="user"
//                                     size={20}
//                                     color={Colors.DrawerUserColor}
//                                 />
//                                 <TouchableOpacity style={{ paddingLeft: 15 }}>
//                                     <Text style={styles.menuName}>Delete Account</Text>
//                                 </TouchableOpacity>
//                             </TouchableOpacity>
//                             : null}
//                     </View>
//                 </ScrollView>

// <<<<<<< HEAD
//                 {/* <TouchableOpacity style={{
// =======
//                     : null}

//                 <View style={styles.menuList}>
//                     <SimpleLineIcons
//                         name="settings"
//                         size={20}
//                         color={Colors.DrawerUserColor}
//                     />
//                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('Setting')}>
//                         <Text style={styles.menuName}>Settings</Text>
//                     </TouchableOpacity>
//                 </View>

//                 <View style={styles.menuList}>
//                     <Image
//                         style={{ width: 22, height: 19.5 }}
//                         source={require("./../Images/Drawer/help_icon.png")}
//                     />
//                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('Help')}>
//                         <Text style={styles.menuName}>Help</Text>
//                     </TouchableOpacity>
//                 </View>

//                 <View style={styles.menuList}>
//                     <EvilIcons
//                         name="share-google"
//                         size={20}
//                         color={Colors.DrawerUserColor}
//                     />
//                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('Share')}>
//                         <Text style={styles.menuName}>Share App</Text>
//                     </TouchableOpacity>
//                 </View>

//                 <View style={styles.menuList}>
//                     <FontAwesome5
//                         name="file-invoice-dollar"
//                         size={20}
//                         color={Colors.DrawerUserColor}
//                     />
//                     <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => props.navigation.navigate('ManagePlan')}>
//                         <Text style={styles.menuName}>Manage Plan</Text>
//                     </TouchableOpacity>
//                 </View>

//                 {useData == "employers" ?
//                     <TouchableOpacity style={styles.menuList}>
//                         <AntDesign
//                             name="user"
//                             size={20}
//                             color={Colors.DrawerUserColor}
//                         />
//                         <TouchableOpacity style={{ paddingLeft: 15 }}>
//                             <Text style={styles.menuName}>Delete Account</Text>
//                         </TouchableOpacity>
//                     </TouchableOpacity>
//                     : null}
//             </ScrollView>

//             <TouchableOpacity style={{
// >>>>>>> 2d82d676fb56e1fa066adfeef1013a5eb189f19e
//                 shadowOpacity: 0,
//                 elevation: 1,
//                 borderTopWidth: 0,
//                 borderTopColor: Colors.BlackColor,
//                 padding: 20,
//                 flexDirection: "row"
//             }} onPress={logout}>
//                 <AntDesign
//                     name="logout"
//                     size={25}
//                     color={Colors.logOut}
//                 />
//                 <Text style={{ fontSize: FontSize.medium_size, color: Colors.logOut, fontWeight: "bold", paddingLeft: 15 }}>LOG OUT</Text>
//             </TouchableOpacity>
//             </SafeAreaView>

//         </View>
//     );
// }

// export default DrawerContent;

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: Colors.DrawerBackground
//     },
//     boderLine: {
//         backgroundColor: Colors.WhiteColor,
//         height: 0.4
//     },
//     profile: {
//         flexDirection: "row",
//         alignItems: "center",
//         paddingBottom: 20
//     },
//     profileImage: {
//         width: screenheight / 14,
//         height: screenheight / 14,
//         borderRadius: 50
//     },
//     userName: {
//         fontFamily: FontFamily.Poppins_Semibold,
//         fontSize: FontSize.medium_size,
//         color: Colors.DrawerUserColor,
//         fontWeight: "600"
//     },
//     userEmail: {
//         fontFamily: FontFamily.Poppins_Regular,
//         color: Colors.DrawerUserColor
//     },
//     menuList: {
//         paddingVertical: 10,
//         flexDirection: "row",
//         alignItems: "center"
//     },
//     menuName: {
//         fontFamily: FontFamily.Poppins_Regular,
//         fontSize: FontSize.small_size,
//         color: Colors.DrawerUserColor
//     }
// });